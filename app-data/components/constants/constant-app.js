﻿define(["angularAMD"], function (angularAMD) {
    'use strict';

    var ConsApp = {};

    ConsApp.HEAD_OFFICE_APP_ID = "48ed5b71-66dc-4725-9604-4c042e45fa3f";

    //#region Workflow
    ConsApp.WORKFLOW_CODE = "TMS_APPROVE_WF_ALL";
    ConsApp.WORKFLOW_CODE_SHOW = "TMS_APPROVE_WF_SHOW";
    ConsApp.WORKFLOW_STATE = [
        { State: "Draft", Name: "Đang soạn thảo", Label: "label label-primary", Color: "#36c6d3", IconClass: "fa fa-pencil" },
        { State: "HO_EXECUTE", Name: "Chờ duyệt", Label: "label label-warning", Color: "#b57f58", IconClass: "fa fa fa-send" },
        { State: "HO_DENIED", Name: "Bị từ chối", Label: "label label-danger", Color: "#000099", IconClass: "fa fa-thumbs-down" },
        { State: "HO_APPROVED", Name: "Đã duyệt", Label: "label label-success", Color: "#cc0000", IconClass: "fa fa-thumbs-up" },
        { State: "HO_CANCELED", Name: "Hủy duyệt", Label: "label label-danger", Color: "#ff0000", IconClass: "fa fa-ban" },
    ];
    ConsApp.WORKFLOW_COMMAND = [
        { Command: "StartProcessing", CommandName: "Trình duyệt", ButtonClass: "btn btn-primary", IconClass: "fa fa-paper-plane" },
        { Command: "Approve", CommandName: "Duyệt", ButtonClass: "btn btn-success", IconClass: "fa fa-thumbs-up" },
        { Command: "Denial", CommandName: "Từ chối", ButtonClass: "btn btn-danger", IconClass: "fa fa-thumbs-down" },
        { Command: "CancelApprove", CommandName: "Hủy duyệt", ButtonClass: "btn btn-danger", IconClass: "fa fa-thumbs-down" },
    ];

    //#endregion

    // #region API
    //Based Api url
    ConsApp.BASED_API_URL = "http://183.81.32.66:9026";
    ConsApp.BASED_UPLOADFILE_URL = "http://183.81.32.66:9034";
    ConsApp.BASED_FILE_URL = "http://183.81.32.66:9034/files";
    ConsApp.BASED_WEBSITE_ADMIN_URL = "http://183.81.32.66:9030/";
    ConsApp.BASED_API_MOBILE_URL = "http://183.81.32.66:9033/";

    //Api url
    ConsApp.API_SYSTEM_TRACKING_URL = "/api/v1/system/tracking";
    ConsApp.API_ROLE_URL = "/api/system/roles";
    ConsApp.API_USER_URL = "/api/system/users";
    ConsApp.API_SITE_URL = "/api/system/sites";
    ConsApp.API_RIGHT_URL = "/api/system/rights";



    ConsApp.API_PAYMENT_ONEPAY_GET_LIST_URL = "/api/payment";
    ConsApp.API_BANK_ONEPAY_GET_LIST_URL = "/api/bankonepay";
    ConsApp.API_MERCHANT_ONEPAY_GET_LIST_URL = "/api/merchant";


    //api phòng chiếu
    ConsApp.API_SCREEN_URL_ADD = "/api/screen/add";
    ConsApp.API_SCREEN_URL_EDIT = "/api/screen/edit";
    ConsApp.API_SCREEN_URL_DELETE = "/api/screen/delete";
    ConsApp.API_SCREEN_URL_DELETE_MANY = "/api/screen/deletes";
    ConsApp.API_SCREEN_URL_GET_BY_ID = "/api/screen/getbyid";
    ConsApp.API_SCREEN_URL_GET_LIST = "/api/screen/list";
    ConsApp.API_SCREEN_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/loai-phong-chieu/itemlist";
    ConsApp.API_SCREEN_URL_GET_BY_CINEMA_AND_SCREENTYPE = "/api/screen/get/{0}/{1}/{2}";

    ConsApp.API_CITY_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-tinh/itemlist";
    ConsApp.API_CITY_URL_GET_LIST_LOYALTY = "/api/catalogs/catalogmaster/danh-muc-tinh-thanh-pho/itemlist";
    ConsApp.API_DISTRICT_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-quan-huyen/TinhId";
    ConsApp.API_CINEMA_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/loai-rap-chieu/itemlist";
    ConsApp.API_OBJECT_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/object-type-loyalty/itemlist";
    ConsApp.API_DISCOUNT_PERCENT_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-ty-le-tich-luy-loyalty/itemlist";
    ConsApp.API_KIOT_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-loai-quay/itemlist";
    ConsApp.API_PAYMENT_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-loai-hinh-thanh-toan/itemlist";

    ConsApp.API_GENRE_URL = "/api/catalogs/catalogmaster/the-loai-phim/itemlist";
    ConsApp.API_FILM_FORMAT_URL = "/api/catalogs/catalogmaster/dinh-dang-phim/itemlist";
    ConsApp.API_FILM_DUBBING_URL = "/api/catalogs/catalogmaster/danh-muc-hinh-thuc-phim/itemlist";
    ConsApp.API_DISTRIBUTOR_URL = "/api/catalogs/catalogmaster/nha-phan-phoi/itemlist";
    ConsApp.API_FILM_RESTRICT_AGE_URL = "/api/catalogs/catalogmaster/phan-loai-do-tuoi/itemlist";
    ConsApp.API_SUBTITTLE_URL = "/api/catalogs/catalogmaster/phu-de/itemlist";
    ConsApp.API_TAXATION_URL = "/api/catalogs/catalogmaster/danh-muc-thue/itemlist";
    ConsApp.API_STUDIO_URL = "/api/catalogs/catalogmaster/danh-muc-studio/itemlist";
    ConsApp.API_DISCOUNT_TYPE_URL = "/api/catalogs/catalogmaster/discount-type/itemlist";

    ConsApp.API_CINEMA_URL_GET_LIST = "/api/cinema/list";
    ConsApp.API_CINEMA_URL_ADD_NEW = "/api/cinema/add";
    ConsApp.API_CINEMA_URL_EDIT = "/api/cinema/edit";
    ConsApp.API_SCREEN_MANAGER_URL_GET_LIST = "/api/system/users/screen_mangager";
    ConsApp.API_CINEMA_TYPE_URL_DELETE_MANY = "/api/cinema/deletes";

    //Voucher
    ConsApp.API_VOUCHER_DONVITINH_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-don-vi-tinh/itemlist";
    ConsApp.API_VOUCHER_ITEMTYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-doi-tuong-ap-dung-the-uu-dai/itemlist";
    ConsApp.API_VOUCHER_ISSUINGUNIT_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-nha-phat-hanh-the-uu-dai/itemlist";
    ConsApp.API_VOUCHER_CARDTYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-loai-the-uu-dai/itemlist";
    ConsApp.API_VOUCHER_URL_GET_LIST = "/api/Voucher/";
    ConsApp.API_VOUCHER_CARD_URL_GET_LIST = "/api/Voucher/card";
    ConsApp.API_VOUCHER_URL_ADD_NEW = "/api/Voucher";
    ConsApp.API_VOUCHER_URL_EDIT = "/api/Voucher";
    ConsApp.API_VOUCHER_CARD_URL_EDIT = "/api/vouchercard";
    ConsApp.API_VOUCHER_URL_DELETE_MANY = "/api/Voucher/deletes";
    ConsApp.API_VOUCHER_CARD_URL_DELETE_MANY = "/api/vouchercard/deletes";
    ConsApp.API_VOUCHER_CARD_INFO_URL_GET_LIST = "/api/vouchercard/info";

    //Combo
    ConsApp.API_COMBO_LOAIQUAY_URL_GET_LIST = "/api/catalogs/catalogmaster/danh-muc-loai-quay/itemlist";

    //CRM
    ConsApp.API_CRM_ACCOUNT_CLASS_URL_GET_LIST = "/api/catalogs/catalogmaster/hang-khach-hang-loyalty/itemlist";
    ConsApp.API_CRM_ACCOUNT_GROUP_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/nhom-khach-hang/itemlist";
    ConsApp.API_CRM_ACCOUNT_CARD_REASON_URL_GET_LIST = "/api/catalogs/catalogmaster/ly-do-cap-the-loyalty/itemlist";
    ConsApp.API_CRM_ACCOUNT_CARD_REASON_METADATA_URL_GET_LIST = "/api/catalogs/catalogmaster/filter-metadata/ly-do-cap-the-loyalty/ShowInTMS?stringFilter=",
    //


    //api upload
    ConsApp.API_UPLOAD_URL = "/api/file-upload";
    ConsApp.APP_MEDIA_DATA_URL = "/data";


    ConsApp.API_TICKET_TYPE_URL_ADD_NEW = "/api/ticket-type/add";
    ConsApp.API_TICKET_TYPE_URL_EDIT = "/api/ticket-type/edit";
    ConsApp.API_TICKET_TYPE_URL_GET_LIST = "/api/ticket-type/list";
    ConsApp.API_TICKET_TYPE_URL_GET_LIST_FOR_PACKAGE = "/api/ticket-type/list-for-package";
    ConsApp.API_TICKET_TYPE_URL_GET_BY_ID = "/api/ticket-type/getbyid";
    ConsApp.API_TICKET_TYPE_URL_DELETE_MANY = "/api/ticket-type/deletes";
    ConsApp.API_OBJECT_URL_GET_LIST = "/api/catalogs/catalogmaster/doi-tuong/itemlist";
    ConsApp.API_FILM_FORMAT_URL_GET_LIST = "/api/catalogs/catalogmaster/dinh-dang-phim/itemlist";
    ConsApp.API_SEAT_TYPE_URL_GET_LIST = "/api/catalogs/catalogmaster/loai-ghe/itemlist";
    ConsApp.API_SALE_CHANEL_URL_GET_LIST = "/api/catalogs/catalogmaster/sale-channel/itemlist";
    ConsApp.API_SALE_CHANNEL_URL_GET_LIST = "/api/catalogs/catalogmaster/sale-channel/itemlist";
    ConsApp.API_TICKET_CLASS_URL_GET_LIST = "/api/catalogs/catalogmaster/ticket-class/itemlist";
    // #endregion

    //#region App constant
    ConsApp.OPTION_ACTIVE_DEACTIVE = [
        { Key: true, Value: "Hoạt động", Label: "label-success" },
        { Key: false, Value: "Không hoạt động", Label: "label-danger" },
    ];

    ConsApp.GENDER = [
        { Id: 1, Name: "Nam", NameF: "Male" },
        { Id: 2, Name: "Nữ", NameF: "Female" },
        { Id: 3, Name: "Khác", NameF: "Other" }
        //{ Id: 4, Name: "Không muốn nói", NameF: "" },
        //{ Id: 0, Name: "Không xác định", NameF: "" }
    ];

    ConsApp.TRANSACTION_POINT_TYPE = [
        { Key: null, Code: "", Name: "Tất cả" },
        { Key: 1, Name: "Tích điểm", Code: "ACCUMULATION_POINT" },
        { Key: 2, Name: "Tiêu điểm", Code: "SPENDING_POINT" },
        { Key: 3, Name: "Giao dịch hủy", Code: "REFUND" },
    ];

    ConsApp.DAYS_IN_WEEK = [
        { Id: 1, Name: 'Thứ hai' },
        { Id: 2, Name: 'Thứ ba' },
        { Id: 3, Name: 'Thứ tư' },
        { Id: 4, Name: 'Thứ năm' },
        { Id: 5, Name: 'Thứ sáu' },
        { Id: 6, Name: 'Thứ bảy' },
        { Id: 0, Name: 'Chủ nhật' },
    ];

    ConsApp.LIST_REPORT_TYPE = [
        { Code: "session-report", Name: "Báo cáo ca bán hàng" },
        { Code: "admits-report", Name: "Báo cáo Admits" },
        { Code: "performance-report", Name: "Báo cáo doanh thu" },
        { Code: "marketing-report", Name: "Báo cáo Marketing" },
        { Code: "distributor-report", Name: "Báo cáo cho Nhà phát hành" },
        { Code: "control-report", Name: "Báo cáo kiểm soát" },
        { Code: "operation-report", Name: "Báo cáo vận hành" },
    ];

    ConsApp.LIST_REPORT = [
        //{ Code: "session-report", ReportType: "session-report", Name: "Báo cáo phiên bán hàng" },
        { Code: "admits-report-week", ReportType: "admits-report", Name: "Báo cáo Admits theo tuần" },
        { Code: "admits-report-month", ReportType: "admits-report", Name: "Báo cáo Admits theo tháng" },
        { Code: "admits-report-day", ReportType: "admits-report", Name: "Báo cáo Admits theo các thứ trong tuần" },
        { Code: "admits-report-usage", ReportType: "admits-report", Name: "Báo cáo tỷ lệ lấp đầy" },
        { Code: "performance-report-total", ReportType: "performance-report", Name: "Báo cáo Doanh thu toàn hệ thống" },
        { Code: "performance-report-box", ReportType: "performance-report", Name: "Báo cáo Doanh thu BOX" },
        { Code: "performance-report-nobox", ReportType: "performance-report", Name: "Báo cáo Doanh thu các quầy" },
        { Code: "performance-report-all", ReportType: "performance-report", Name: "Báo cáo Doanh thu tổng hợp" },
        { Code: "summary-performance-report", ReportType: "performance-report", Name: "Summary Performance Report" },
        { Code: "performance-report-average", ReportType: "performance-report", Name: "Báo cáo giá trung bình" },
        { Code: "performance-report-sold-item", ReportType: "performance-report", Name: "Báo cáo hàng hóa xuất bán" },
        { Code: "performance-report-sold-ticket", ReportType: "performance-report", Name: "Báo cáo vé xuất sử dụng" },
        { Code: "revenue-rental-report", ReportType: "performance-report", Name: "Báo cáo tổng hợp doanh thu và chi phí thuê phim" },
        { Code: "concession-sales", ReportType: "performance-report", Name: "Concession Sales" },
        { Code: "marketing-report", ReportType: "marketing-report", Name: "Báo cáo Marketing" },
        { Code: "marketing-report-voucher", ReportType: "marketing-report", Name: "Báo cáo Voucher" },
        { Code: "marketing-report-member-card", ReportType: "marketing-report", Name: "Báo cáo Thẻ thành viên" },
        //{ Code: "marketing-report-show-time", ReportType: "marketing-report", Name: "Báo cáo Lịch chiếu phim" },
        { Code: "distributor-report-by-film-show", ReportType: "distributor-report", Name: "Báo cáo nhà phát hành theo phim, show" },
        { Code: "distributor-report-by-film-day", ReportType: "distributor-report", Name: "Báo cáo nhà phát hành theo phim, ngày" },
        //{ Code: "distributor-report", ReportType: "distributor-report", Name: "Báo cáo doanh thu phát hành phim" },
        //{ Code: "distributor-report-detail", ReportType: "distributor-report", Name: "Báo cáo chi tiết doanh thu phát hành phim" },
        { Code: "distributor-report-rental", ReportType: "distributor-report", Name: "Báo cáo tổng hợp phí thuê phim" },
        { Code: "distributor-report-ticket-no-money", ReportType: "distributor-report", Name: "Báo cáo vé 0đ" },
        { Code: "complimentaries-level-out", ReportType: "distributor-report", Name: "Báo cáo vé mời vượt mức" },
        //{ Code: "distributor-report-admits", ReportType: "distributor-report", Name: "Báo cáo vé mời vượt mức" },
        { Code: "control-report-transaction", ReportType: "control-report", Name: "Báo cáo giao dịch" },
        { Code: "control-report-refund", ReportType: "control-report", Name: "Báo cáo Refund" },
        { Code: "control-report-payments-online", ReportType: "control-report", Name: "Báo cáo giao dịch online" },
        { Code: "control-report-pos", ReportType: "control-report", Name: "Báo cáo thao tác máy POS" },
        { Code: "control-report-risk", ReportType: "control-report", Name: "Báo cáo rủi ro" },
        { Code: "control-report-check-transaction", ReportType: "control-report", Name: "Báo cáo kiểm soát giao dịch" },
        { Code: "project-schedule-by-start-time", ReportType: "operation-report", Name: "Báo cáo lịch chiếu phim" },
        { Code: "operation-rpt", ReportType: "performance-report", Name: "Operation RPT" },
        { Code: "operation-rpt-incl-film-rental", ReportType: "performance-report", Name: "Operation RPT - Film Rental" },
    ];

    ConsApp.DISCOUNT_OBJECT = [
        { DiscountObjectId: 1, Code: 've', Name: 'Vé' },
        { DiscountObjectId: 2, Code: 'hang-hoa', Name: 'Hàng hóa' },
        { DiscountObjectId: 3, Code: 'hoa-don', Name: 'Hóa đơn' },
    ];

    ConsApp.GRID_ATTRIBUTES = {
        PageNumber: 1,
        PageSize: 10,
        DataCount: 0,
        TotalCount: 0,
        FromRecord: 0,
        ToRecord: 0,
        Data: [],
        Check: false,
        GrantAccess: true,
        PageSizeOptions: [5, 10, 25, 50]
    };

    ConsApp.SYSTEM_TRACKING_ACTION = [
        { Action: "LOGIN", Name: "Đăng nhập", Label: "success" },
        { Action: "LOGOUT", Name: "Đăng xuất", Label: "warning" },
        { Action: "ADD", Name: "Thêm mới", Label: "primary" },
        { Action: "UPDATE", Name: "Cập nhật", Label: "info" },
        { Action: "DELETE", Name: "Xóa", Label: "danger" },
    ];

    ConsApp.SYSTEM_STATUS_PAYMENT = [
        { StatusId: "Successful", Name: "Successful" },
        { StatusId: "Pending", Name: "Pending" },
        { StatusId: "Fail", Name: "Fail" },

    ];

    ConsApp.CARD_TYPE_PAYMENT = [
        { Type: "MC", Name: "MasterCard" },
        { Type: "JCB", Name: "JCB" },
        { Type: "AmEx", Name: "AmEx" },
        { Type: "CUP", Name: "CUP" },
        { Type: "VC", Name: "Visa" },
    ];

    ConsApp.POS_ACTION_PERMISSION = [
        { Key: 'GHI_NHAN_BAN', Value: 'Ghi nhận bán hàng', IsSelected: false },
        { Key: 'GHI_NHAN_MUA', Value: 'Ghi nhận trả hàng', IsSelected: false }
    ];

    ConsApp.SESSION_STATE = [
        { State: 'READY', Name: 'Sẵn sàng bán', Label: "label label-default" },
        { State: 'OPENING', Name: 'Đang mở bán', Label: "label label-primary" },
        { State: 'REOPENING', Name: 'Mở bán lại', Label: "label label-primary" },
        { State: 'PENDING', Name: 'Tạm nghỉ', Label: "label label-warning" },
        { State: 'CLOSED', Name: 'Đóng phiên', Label: "label label-danger" },
        { State: 'DELIVERED', Name: 'Đã giao tiền', Label: "label label-success" },
    ];

    ConsApp.POS_AUDIT_ACTION = [
        { ActionId: 1, Severity: 8, Action: "VIEW_REPORT_FINISH_SESSION", Name: "Xem báo cáo khi kết thúc ca bán hàng" },
        { ActionId: 2, Severity: 7, Action: "VIEW_REPORT_NOT_FINISH_SESSION", Name: "Xem báo cáo khi chưa kết thúc ca bán hàng" },
        { ActionId: 3, Severity: 3, Action: "REOPENING", Name: "Mở lại phiên" },
    ];

    //#endregion

    //#region Constant of Seat
    //seat type constant
    ConsApp.SEAT_TYPE_STANDARD_CLASS_STYLE = "seat-type-standard";
    ConsApp.SEAT_TYPE_VIP_CLASS_STYLE = "seat-type-vip";
    ConsApp.SEAT_TYPE_DOUBLE_CLASS_STYLE = "seat-type-double";

    ConsApp.SEAT_STATUS_USED_CLASS_STYLE = "seat-used";
    ConsApp.SEAT_STATUS_NOT_USED_CLASS_STYLE = "seat-not-used";
    ConsApp.SEAT_STATUS_FOR_WAY_CLASS_STYLE = "seat-for-way";

    ConsApp.SEAT_SELECTED_STATUS_CONST = {
        selected: 0,
        empty: 1,
        holded: 2,
        booked: 3,
        sold: 4,
        payment: 5
    }

    ConsApp.APPROVE_STATUS = {
        SAVED: 0,// Chờ gửi duyệt
        PENDDING: 1,//Chờ duyệt
        APPROVE: 2,//Đã Duyệt
        REJECT: 3//Từ chối duyệt
    }

    ConsApp.LIST_APPROVE_STATUS = [
        { Id: 0, Name: 'Nháp' },
        { Id: 1, Name: 'Chờ duyệt' },
        { Id: 2, Name: 'Đã duyệt' },
        { Id: 3, Name: 'Từ chối duyệt' },
    ]

    ConsApp.LIST_STATUS = [
        {
            Id: true,
            Name: "Sử dụng"
        },
        {
            Id: false,
            Name: "Không sử dụng"
        }
    ]

    ConsApp.SEAT_SELECTED_STATUS = [
        { Name: "Đang chọn", Value: 0, Class: "seat-selected" },
        { Name: "Còn trống", Value: 1, Class: "seat-empty" },
        { Name: "Đang giữ chỗ", Value: 2, Class: "seat-hold" },
        { Name: "Đã đặt", Value: 3, Class: "seat-booked" },
        { Name: "Đã bán", Value: 4, Class: "seat-sold" },
        { Name: "Đã thanh toán", Value: 5, Class: "seat-payment" }

    ];
    //#endregion 

    ConsApp.PasswordDefault = "12345a@";
    //Register constant, always in the end of the file
    angularAMD.constant("ConstantsApp", ConsApp);
});