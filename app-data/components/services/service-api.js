﻿define(["angularAMD"], function (angularAMD) {
    'use strict';

    angularAMD.service("OrganizationApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/organizations";

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetTree = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/listtree"
            });
            return promise;
        };

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + '/' + putData.OrganizationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("SiteApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_SITE_URL;

        service.GetAccessibleSite = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + '/accessible' });
        }

        service.GetDefaultSite = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + '/default' });
        }

        service.GetAllSite = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl });
        }

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + '/' + putData.OrganizationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("SystemTrackingApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_SYSTEM_TRACKING_URL;

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("RoleApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_ROLE_URL;

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetFilterByPrams = function (curentPage, pageSize, filterText) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + '?pageNumber=' + curentPage + '&pageSize=' + pageSize + '&textSearch=' + filterText
            });
            return promise;
        };

        service.GetListRightsByRoleId = function (roleId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/system/rights?roleId=" + roleId
            });
            return promise;
        };

        service.GetListRights = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/system/rights"
            });
            return promise;
        };

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + '/' + putData.RoleId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("RightApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_RIGHT_URL;

        service.GetAll = function () {
            var promise = $http({ method: 'GET', url: apiUrl });
            return promise;
        }

        service.GetAllGroups = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + '/groups' });
        };

        service.GetTree = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + '/tree' });
        };

        service.GetListRightsByRoleId = function (roleId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/system/rights?roleId=" + roleId
            });
            return promise;
        };

        service.GetListRights = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/system/rights"
            });
            return promise;
        };

        service.Add = function (postData) {
            return $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
        };

        service.Update = function (putData) {
            return $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + '/' + putData.RightCode,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
        };

        service.Delete = function (rightCode) {
            return $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl + '/' + rightCode,
                headers: {
                    'Content-type': ' application/json'
                },
                //data: delData
            });
        }

        return service;
    }]);
    angularAMD.service("SessionSetupApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/v1/erp/session-setup";

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            return $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
        }
        service.Add = function (postData) {
            return $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
        };
        service.DeleteMany = function (deleteModel) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                data: deleteModel,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("SessionInstanceApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/v1/erp/sessions";

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            return $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
        }
        service.Add = function (postData) {
            return $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
        };
        service.Update = function (sessionId, putData) {

            return $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + "/" + sessionId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
        };
        service.DeleteMany = function (deleteModel) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                data: deleteModel,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        };
        service.ValidateSession = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/validate" });
        };
        service.UpdateState = function (sessionId, state) {
            return $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + "/" + sessionId + "/state?sessionState=" + state,
                headers: {
                    'Content-type': ' application/json'
                },
            });
        };
        service.GetSessionSaleReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-sale" });
        };
        service.GetSessionComboReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-combo" });
        };
        service.GetSessionPaymentReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-payment" });
        };
        service.GetSessionAccessReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-access" });
        };
        service.GetSessionNoteItemReportById = function (sessionId, noteCode) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-note-return?noteCode=" + noteCode });
        };
        service.GetSessionRedeemItemReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-redeem-item" });
        };
        service.GetSessionTransReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-trans" });
        };
        service.GetSessionVoucherReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-voucher" });
        };
        service.GetSessionVoucherOldReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-voucher-old" });
        };
        service.GetSessionTicketReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-ticket" });
        };
        service.GetSessionTicketInPackageReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-ticket-in-package" });
        };
        service.GetSessionItemInPackageReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-item-in-package" });
        };
        service.GetSessionDiscountReportById = function (sessionId) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + sessionId + "/report-discount-invoice" });
        };
        service.LogAuditPOS = function (postData) {
            return $http({
                method: 'POST',
                url: basedUrl + "/api/v1/erp/audit-session-pos",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
        };
        service.ExcelExport = function (model) {
            var stringFilter = angular.toJson(model);
            //var promise = $http({
            //    method: 'GET',
            //    url: baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter
            //});
            return basedUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };
        return service;
    }]);
    angularAMD.service("PointOfSaleApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/v1/erp/point-of-sales";

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            return $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
        };
        service.GetAllActiveByCinema = function (cinemaId) {
            return $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + cinemaId + "/active"
            });
        };
        service.Add = function (postData) {
            return $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
        };
        service.Update = function (posId, putData) {

            return $http({
                method: 'PUT',
                url: basedUrl + prefixApiUrl + "/" + posId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
        };
        service.DeleteMany = function (deleteModel) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                data: deleteModel,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("ItemApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/items";

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        };

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.ExcelExport = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            //var promise = $http({
            //    method: 'GET',
            //    url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            //});
            return basedUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };

        service.CheckExistCode = function (code) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/checkexistcode?id=" + code
            });
            return promise;
        };

        service.GetReference = function (data) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/reference?itemId=" + data.Id
            });
            return promise;
        };

        service.GetProperties = function (data) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/properties?itemId=" + data.Id
            });
            return promise;
        };

        service.GetPriceHistory = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/pricehistory?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.CopyItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/copy',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.UpdatePrice = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/updateprice',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.HideItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/hideitem',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.UnHideItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/unhideitem',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.DeleteRefs = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/deleterefs',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                //url: basedUrl + prefixApiUrl + '/' + putData.id,
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.Deletes = function (delData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/deletes',
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        };

        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        };

        service.PriceHistoryDeletes = function (delData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/pricehistorydeletes",
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        };

        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/items/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };

        return service;
    }]);
    angularAMD.service("ComboPacketApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/combopacket";
        var getListLoaiQuayApiUrl = ConstantsApp.API_COMBO_LOAIQUAY_URL_GET_LIST;

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        };

        service.GetFilter = function (filterModel) {

            var promise = $http({
                method: 'POSt',
                url: basedUrl + prefixApiUrl + "/filter",
                headers: {
                    'Content-type': ' application/json'
                },
                data: filterModel
            });
            return promise;
        };

        service.GetComboPacket = function (postData) {

            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/list",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.GetReference = function (data) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/reference?itemId=" + data.Id
            });
            return promise;
        };

        service.GetItemInComboPacket = function (data) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/itemincombopacket?itemId=" + data.Id
            });
            return promise;
        };

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/add",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                //url: basedUrl + prefixApiUrl + '/' + putData.id,
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.Deletes = function (delData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/deletes',
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        };

        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        };

        //DM
        service.GetListLoaiQuay = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListLoaiQuayApiUrl
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("KiotApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/kiot";

        service.GetReference = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/reference/roleids",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/role",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.CopyItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/item',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.DeleteItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/deleteitem',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        return service;
    }]);
    angularAMD.service("SalesApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/v1/sales";

        service.GetFilter = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/transaction?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + id + "/transaction"
            });
            return promise;
        };

        service.GetPaymentByItemId = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + id + "/paymentdetails"
            });
            return promise;
        };

        service.GetChildOfItemById = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/transaction/items/" + id + "/child"
            });
            return promise;
        };

        service.Pay = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/transaction/pay",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.UnSaleAdmin = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/transaction/unsaleadmin",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.UpdateStateOfItem = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/transaction/items/updatestate",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.UpdateShowSeat = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/shows/update-sale-ticket",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.ExcelExport = function (model) {
            var stringFilter = angular.toJson(model);
            //var promise = $http({
            //    method: 'GET',
            //    url: baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter
            //});
            return basedUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };
        return service;
    }]);

    angularAMD.service('CatalogService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/catalogs/catalogmaster";
        service.ApiUrl = prefixApiUrl;
        service.GetApiLoadItemListSrc = function (catalogCode) {
            return "[[configDomain]]/" + prefixApiUrl + "/" + catalogCode + "/itemlist";
        }
        service.GetApiLoadItemTreeSrc = function (catalogCode) {
            return "[[configDomain]]/" + prefixApiUrl + "/" + catalogCode + "/itemstree";
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetTree = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/tree"
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.GetItemTree = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id + "/itemtree"
            });
            return promise;
        };
        service.GetItemList = function (code) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + code + "/itemlist"
            });
            return promise;
        };

        //api/catalogs/catalogmaster/{id}/itembylevel
        service.GetItemByLevel = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id + "/itembylevel"
            });
            return promise;
        };
        service.GetFilterByMetadata = function (catalogMasterCode, code, model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/filter-metadata/" + catalogMasterCode + "/" + code + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('CatalogItemService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/catalogitem";
        service.apiUrl = prefixApiUrl;
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }


        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteManyLoki = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            //var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + "/delmany",
                data: model,
            });
            return promise;
        }


        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id + "/data/"
            });
            return promise;
        }

        return service;

    }]);
    angularAMD.service('MetadataFieldService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/metadatafield";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        return service;

    }]);
    angularAMD.service('MetadataTemplateService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/metadatatemplate";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.Clone = function (id) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + "/" + id + "/clone"
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetMetadataFieldByMetadataTemplateId = function (id) {
            if (id === "all") {
                id = "00000000-0000-0000-0000-000000000000";
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id + "/field/"
            });
            return promise;
        }
        return service;

    }]);
    angularAMD.service('FieldService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/field";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        return service;

    }]);
    angularAMD.service('TaxonomyService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixCoreApiUrl = "api/taxonomy";
        var prefixCoreApiUrlVoca = "api/taxonomy/vocabularies";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "terms?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetTree = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/tree"
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id
            });
            return promise;
        };

        service.GetByVocabularyId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/vocabularies/" + id + "/terms"
            });
            return promise;
        };

        service.GetByParentId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/parent=" + id
            });
            return promise;
        };

        service.Create = function (id, model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/vocabularies/" + id + "/terms",
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id,
            });
            return promise;
        }

        //service.DeleteMany = function (model, isDeleteChild) {
        //    var promise = $http({
        //        method: 'DELETE',
        //        url: baseUrl + prefixCoreApiUrl,
        //        data: model
        //    });
        //    return promise;
        //}

        return service;

    }]);
    angularAMD.service('TaxonomyVocabularyService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixCoreApiUrl = "api/taxonomy/vocabularies";

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByVocabularyId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/terms"
            });
            return promise;
        };

        service.Create = function (id, model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        //service.DeleteMany = function (model, isDeleteChild) {
        //    var promise = $http({
        //        method: 'DELETE',
        //        url: baseUrl + prefixCoreApiUrl,
        //        data: model
        //    });
        //    return promise;
        //}

        return service;

    }]);
    angularAMD.service('PermissionApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};

        var apiUrl = ConstantsApp.BASED_API_URL;
        var apiRightUrl = ConstantsApp.API_RIGHT_URL;
        var apiRoleUrl = ConstantsApp.API_ROLE_URL;
        var apiUserUrl = ConstantsApp.API_USER_URL;
        //Right contoller
        service.GetAllRights = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.EnableRight = function (RIGHT_CODE) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiRightUrl + "/" + RIGHT_CODE + "/enable",
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.DisableRight = function (RIGHT_CODE) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiRightUrl + "/" + RIGHT_CODE + "/disable",
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.UpdateRight = function (rightData) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiRightUrl + "/" + rightData.RightCode,
                data: rightData,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.AddRight = function (rightData) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + apiRightUrl + "/" + rightData.RightCode,
                data: rightData,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.CheckRightCodeName = function (RightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/checkname/" + RightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.GetQueryRight = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&textSearch=" + textSearch,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetRightByCode = function (rightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/" + rightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetRightsOfRole = function (roleId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "?roleId=" + roleId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetRightsOfUser = function (userId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "?userId=" + userId + "&applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.CheckRightOfUserById = function (userId, rightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/check?userId=" + userId + "&rightCode=" + rightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.CheckRightOfUserInApplication = function (rightCode, userId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/check?userId=" + userId + "&rightCode=" + rightCode + "&applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.CheckRightOfRole = function (roleId, rightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/check?roleId=" + roleId + "&rightCode=" + rightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetAllowedApplicationByRightAndUser = function (userId, rightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/site?userId=" + userId + "&rightCode=" + rightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetAllowedApplicationByUser = function (userId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/site?userId=" + userId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.ChangeAllowedApplication = function (userId, rightCode, listApplications) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRightUrl + "/site?userId=" + userId + "&rightCode=" + rightCode + "&applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        ///Role controler
        service.GetRoleByCode = function (roleId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl + "/" + roleId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetAllRole = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetQueryRole = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&textSearch=" + textSearch,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.UpdateRole = function (roleId, roleModel) {
            var putData = {};
            putData = roleModel;


            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiRoleUrl + "/" + roleId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData,
            });
            return promise;
        };

        service.AddRole = function (roleId, roleModel) {
            var postData = {};
            postData = roleModel;


            var promise = $http({
                method: 'POST',
                url: apiUrl + apiRoleUrl + "/" + roleId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData,
            });
            return promise;
        };

        service.DeleteRole = function (roleId) {

            var promise = $http({
                method: 'DELETE',
                url: apiUrl + apiRoleUrl + "/" + roleId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.DeleteRoles = function (deletesRoleModel) {
            var deleteData = {};
            deleteData = deletesRoleModel;
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + apiRoleUrl + "/" + roleId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: deleteData
            });
            return promise;
        };
        service.GetUsersOfRole = function (roleId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl + "/" + roleId + "/users?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetUsersOfRoleByCode = function (roleId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl + "/" + roleId + "/users/bycode?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetRolesByRightCode = function (rightCode) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiRoleUrl + "?rightcode=" + rightCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.DeleteRightsOfRole = function (roleId, rightModel) {
            var deleteData = {};
            deleteData = rightModel;
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + apiRoleUrl + "/" + roleId + "rights",
                headers: {
                    'Content-type': ' application/json'
                },
                data: deleteData
            });
            return promise;
        };
        service.AddRightsOfRole = function (roleId, rightModel) {
            var postData = {};
            postData = rightModel;
            var promise = $http({
                method: 'POST',
                url: apiUrl + apiRoleUrl + "/" + roleId + "rights",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        ///User controler
        service.GetUserById = function (userId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl + "/" + userId + "?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetAllUser = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetQueryUser = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&textSearch=" + textSearch,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GetQueryUserFilter = function (filterModel) {
            var postData = {};
            postData = filterModel;
            var promise = $http({
                method: 'POST',
                url: apiUrl + apiUserUrl + "/query",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData

            });
            return promise;
        };

        service.GetActiveUsers = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl + "/active",
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.EnableRightOfUser = function (userId, rightCode, applicationId) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiUserUrl + "/" + userId + "/rights/enable/" + rightCode + "?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.DisableRightOfUser = function (userId, rightCode, applicationId) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiUserUrl + "/" + userId + "/rights/disable/" + rightCode + "?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.DeleteRightsOfUser = function (userId, rightModel, applicationId) {
            var deleteData = {};
            deleteData = rightModel;
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + apiUserUrl + "/" + userId + "/rights?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: deleteData
            });
            return promise;
        };

        service.AddRightsOfUser = function (userId, rightModel, applicationId) {
            var postData = {};
            postData = rightModel;
            var promise = $http({
                method: 'POST',
                url: apiUrl + apiUserUrl + "/" + userId + "/rights?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.DeleteRolesOfUser = function (userId, roleModel, applicationId) {
            var deleteData = {};
            deleteData = roleModel;
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + apiUserUrl + "/" + userId + "/roles?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: deleteData
            });
            return promise;
        };

        service.AddRolesOfUser = function (userId, roleModel, applicationId) {
            var postData = {};
            postData = roleModel;
            var promise = $http({
                method: 'POST',
                url: apiUrl + apiUserUrl + "/" + userId + "/roles?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.GetRolesOfUser = function (userId, applicationId) {

            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl + "/" + userId + "/roles?applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.CheckRoleOfUser = function (userId, roleId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + apiUserUrl + "/check?userId=" + userId + "&roleId=" + roleId + "&applicationId=" + applicationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        //UpdateNavigationInRole
        service.UpdateNavigationInRole = function (roleId, navigationModel) {
            var putData = {};
            putData = navigationModel;
            console.log('navigationModel:', navigationModel);
            var promise = $http({
                method: 'PUT',
                url: apiUrl + apiRoleUrl + "/" + roleId + "/navigation",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData,
            });
            return promise;
        };
        // Return service
        return service;
    }]);
    angularAMD.service('UserApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_USER_URL;

        service.GetUserById = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + ConstantsApp.API_USER_URL + "/" + id
            });
            return promise;
        };
        service.GetAllUserActive = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + ConstantsApp.API_USER_URL + "/active"
            });
            return promise;
        };
        service.GetAllUserByOrg = function (orgId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + ConstantsApp.API_USER_URL + "/" + orgId + "/organization"
            });
            return promise;
        };
        service.GetUserByOrgInTreeView = function (orgId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + ConstantsApp.API_USER_URL + "/" + orgId + "/organization/treeview"
            });
            return promise;
        };
        service.GetAllUserInOrg = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + ConstantsApp.API_USER_URL + "/organization"
            });
            return promise;
        };
        service.GetFilter = function (model) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + ConstantsApp.API_USER_URL + "/query",
                data: model
            });
            return promise;
        };
        service.GetRolesByUser = function (userId, applicationId) {
            var promise = $http({
                method: "GET",
                url: basedUrl + ConstantsApp.API_USER_URL + "/" + userId + "/roles?applicationId=" + applicationId
            });
            return promise;
        };
        service.GetUserInfo = function (userId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + userId + "/profile"
            });
            return promise;
        };
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl,
                headers: {
                    'Content-type': 'application/json'
                },
                data: postData
            });
            return promise;
        };
        service.Update = function (putData) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/" + putData.UserId,
                headers: {
                    'Content-type': "application/json"
                },
                data: putData
            });
            return promise;
        };
        service.DeleteMany = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + '/deletes',
                headers: {
                    'Content-type': 'application/json'
                },
                data: postData
            });
            return promise;
        };
        service.CheckNameAvailability = function (value) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/availability?userName=" + value
            });
            return promise;
        }
        service.CheckUserExistAndActive = function () {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/existandactive" });
        }
        service.CheckUserExistAndActiveByType = function (type) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/existandactive?type=" + type });
        }
        service.ActiveUser = function (userId, state) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/" + userId + "/active/" + state,
                headers: {
                    'Content-type': "application/json"
                }
            });
            return promise;
        };
        service.ChangePass = function (userId, postData) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/" + userId + "/password",
                data: postData
            });
            return promise;
        };
        service.ValidatePINCinemaManager = function (userName, pin) {
            return $http({ method: 'GET', url: basedUrl + ConstantsApp.API_USER_URL + "/pin/validate-cinema-manager?userName=" + userName + "&pin=" + pin });
        }
        service.UpdatePIN = function (putData) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/pin",
                headers: {
                    'Content-type': "application/json"
                },
                data: putData
            });
            return promise;
        }
        //#region AD
        service.GetUserADInfo = function (userName) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/ad?userName=" + userName
            });
            return promise;
        };
        //#endregion
        //#region Employee
        service.AddNewEmployee = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/employee",
                headers: {
                    'Content-type': 'application/json'
                },
                data: postData
            });
            return promise;
        };
        service.UpdateEmployee = function (putData) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/employee/" + putData.UserId,
                headers: {
                    'Content-type': "application/json"
                },
                data: putData
            });
            return promise;
        };
        service.GetEmployeeByCinema = function (cinemaId) {
            return $http({ method: 'GET', url: basedUrl + ConstantsApp.API_USER_URL + "/" + cinemaId + "/employees" });
        };
        service.GetEmployeeByMultipleCinema = function (listCinemaId) {
            var promise = $http({
                method: "POST",
                url: basedUrl + ConstantsApp.API_USER_URL + "/employees/multiple-cinema",
                headers: {
                    'Content-type': "application/json"
                },
                data: listCinemaId
            });
            return promise;
        };
        service.ChangePassEmployee = function (putData) {
            var promise = $http({
                method: "PUT",
                url: basedUrl + prefixApiUrl + "/employee/change-password",
                data: putData
            });
            return promise;
        };
        //#endregion
        service.ExcelExport = function (model) {
            var stringFilter = angular.toJson(model);
            //var promise = $http({
            //    method: 'GET',
            //    url: baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter
            //});
            return basedUrl + ConstantsApp.API_USER_URL + "/query/excel-export?stringFilter=" + stringFilter;
        };
        // Return service
        return service;

    }]);
    angularAMD.service('WorkflowApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/workflow/documents";

        service.GetAvailableProcessCommands = function (id) {
            return $http({
                method: 'GET',
                ignoreLoadingBar: true, url: basedUrl + prefixApiUrl + "/" + id + "/availablecommands"
            });
        };
        service.GetTransitionHistory = function (id) {
            return $http({ method: 'GET', url: basedUrl + prefixApiUrl + "/" + id + "/transitionhistory" });
        };
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + prefixApiUrl + "/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };

        return service;

    }]);
    angularAMD.service("ScreenApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các biến
        var basedUrl = ConstantsApp.BASED_API_URL;
        var addApiUrl = ConstantsApp.API_SCREEN_URL_ADD;
        var editApiUrl = ConstantsApp.API_SCREEN_URL_EDIT;
        var deleteApiUrl = ConstantsApp.API_SCREEN_URL_DELETE;
        var deleteManyApiUrl = ConstantsApp.API_SCREEN_URL_DELETE_MANY;
        var getByIdApiUrl = ConstantsApp.API_SCREEN_URL_GET_BY_ID;
        var getListApiUrl = ConstantsApp.API_SCREEN_URL_GET_LIST;
        var getListScreenTypeApiUrl = ConstantsApp.API_SCREEN_TYPE_URL_GET_LIST;
        var getScreenByCinemaAndScreenTypeApiUrl = ConstantsApp.API_SCREEN_URL_GET_BY_CINEMA_AND_SCREENTYPE;
        var getListCinemaApiUrl = ConstantsApp.API_CINEMA_URL_GET_LIST;
        var getListScreenManagerApiUrl = ConstantsApp.API_SCREEN_MANAGER_URL_GET_LIST;
        //#endregion

        //#region thêm, sửa, xóa phòng chiếu
        //tao moi phong chieu
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + addApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //sua phong chieu
        service.Edit = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //xoa phong chieu theo Id
        service.Delete = function (screenId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: screenId
            });
            return promise;
        };

        //Xoa nhieu phong chieu
        //tham số là list ID phòng chiếu
        service.DeleteMany = function (listScreenId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listScreenId
            });
            return promise;
        };
        //#endregion thêm, sửa, xóa phòng chiếu

        //lay thong tin phong chieu theo id
        service.GetById = function (screenId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getByIdApiUrl + '?screenId=' + screenId,
            });
            return promise;
        }

        service.CheckStatus = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + '/api/screen/check-seat-show',
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //lay danh sach phong chieu 
        service.GetList = function (pageNumber, pageSize, textSearch, CinemaId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch + '&CinemaId=' + CinemaId,
            });
            return promise;
        }

        //lay phong chiếu cuối cùng theo rap và loai phong chieu
        service.GetScreenByCinemaAndScreenTypeApiUrl = function (cinemaId, screenType, responseType) {
            var url = getScreenByCinemaAndScreenTypeApiUrl.format(cinemaId, screenType, responseType);
            var promise = $http({
                method: 'GET',
                url: basedUrl + url,
            });
            return promise;
        }

        //lay danh sach rap chieu - de tam day da
        service.GetListCinema = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCinemaApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        //lay danh sach loại phòng chiếu - de tam day da
        service.GetListTypeScreen = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListScreenTypeApiUrl,
            });
            return promise;
        }

        //lay danh sach quản lý phòng - de tam day da
        service.GetListScreenManager = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListScreenManagerApiUrl,
            });
            return promise;
        }

        //lay danh sach phong chieu đang active
        service.GetListScreenActive = function (cinemaId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/screen/cinema/' + cinemaId + '/active/true',
            });
            return promise;
        }

        service.GetListSeatType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/catalogs/catalogmaster/loai-ghe/itemlist",
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("CinemaApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các biến
        var basedUrl = ConstantsApp.BASED_API_URL;
        var addCinemaApiUrl = ConstantsApp.API_CINEMA_URL_ADD_NEW;
        var editCinemaApiUrl = ConstantsApp.API_CINEMA_URL_EDIT;
        var getListCinemaApiUrl = ConstantsApp.API_CINEMA_URL_GET_LIST;
        var getListCityApiUrl = ConstantsApp.API_CITY_URL_GET_LIST;
        var getListCinemaTypeApiUrl = ConstantsApp.API_CINEMA_TYPE_URL_GET_LIST;
        var deleteManyApiUrl = ConstantsApp.API_CINEMA_TYPE_URL_DELETE_MANY;
        //#endregion

        //#region thêm, sửa, xóa phòng chiếu
        //tao moi phong chieu
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + addCinemaApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //sua phong chieu
        service.Edit = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editCinemaApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //xoa phong chieu theo Id
        service.Delete = function (screenId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: screenId
            });
            return promise;
        };

        //Xoa nhieu phong chieu
        //tham số là list ID phòng chiếu
        service.DeleteMany = function (listScreen) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listScreen
            });
            return promise;
        };
        //#endregion thêm, sửa, xóa phòng chiếu

        //lay thong tin rap theo id
        service.GetById = function (cinemaId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/cinema/getbyid' + '?cinemaId=' + cinemaId,
            });
            return promise;
        }

        //lay phong chiếu cuối cùng theo rap và loai phong chieu
        service.GetScreenByCinemaAndScreenTypeApiUrl = function (cinemaId, screenType, responseType) {
            var url = getScreenByCinemaAndScreenTypeApiUrl.format(cinemaId, screenType, responseType);
            var promise = $http({
                method: 'GET',
                url: basedUrl + url,
            });
            return promise;
        }

        //lay danh sach rap chieu 
        service.GetListCinema = function (pageNumber, pageSize, textSearch, cinemaTypeId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCinemaApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch + '&cinemaTypeId=' + cinemaTypeId,
            });
            return promise;
        }

        //lay danh sach rap chieu 
        service.GetAllCinemaActive = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/cinema/active/true',
            });
            return promise;
        }

        //lay danh sach loại rạp
        service.GetListCinemaType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCinemaTypeApiUrl,
            });
            return promise;
        }

        //lay danh sach tỉnh thành
        service.GetListCity = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCityApiUrl,
            });
            return promise;
        }

        //lay mã sinh tự động
        service.GetCinemaAutoCode = function (prefix, suffixlength) {
            return $http({ method: 'GET', url: basedUrl + "/api/cinema/auto-code?prefix=" + prefix + "&suffixlength=" + suffixlength, });
        }

        //lay danh sach quản lý phòng - de tam day da
        service.GetListScreenManager = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListScreenManagerApiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('NavigationApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {

        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixCoreApiUrl = "/api/navigation";

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/all"
            });
            return promise;
        }
        service.GetNavByUserId = function (userId) {
            return $http({ method: 'GET', url: baseUrl + prefixCoreApiUrl + "?userId=" + userId });
        }
        service.GetAllNav = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/allnav"
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        }
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }
        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;
        }
        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.UpdateNavigationInRole = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/role/update",
                data: model
            });
            return promise;
        }
        service.GetAppVersion = function () {
            return $http({ method: 'GET', url: baseUrl + "/api/catalogs/catalogmaster/phien-ban-ung-dung/itemlist" });
        };

        return service;

    }]);

    angularAMD.service('AccountApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var getListCityApiUrl = ConstantsApp.API_CITY_URL_GET_LIST_LOYALTY;
        var getListDistrictApiUrl = ConstantsApp.API_DISTRICT_URL_GET_LIST;
        var getListAccountGroupApiUrl = ConstantsApp.API_CRM_ACCOUNT_GROUP_TYPE_URL_GET_LIST;
        var getListAllocationReasonApiUrl = ConstantsApp.API_CRM_ACCOUNT_CARD_REASON_METADATA_URL_GET_LIST;
        var prefixApiUrl = "/api/erp/accounts";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetFilterClient = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByCardNumber = function (cardNumber) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/by-card-number?cardNumber=" + cardNumber
            });
            return promise;
        };
        service.GetByIdClient = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client/" + id
            });
            return promise;
        };
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }
        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;
        }
        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        service.UpgradeClass = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + '/upgrade-class' + id,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        service.UpgradeClassMany = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + '/upgrade-class',
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        //lay danh sach tỉnh thành phố
        service.GetListCity = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListCityApiUrl + "filter?stringFilter={'level':0}",
            });
            return promise;
        }
        //GetListDistrict
        service.GetListDistrict = function (cityId, termId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListCityApiUrl + "filter?stringFilter={'Level':1,'ParentTermId':'" + termId + "'}",
            });
            return promise;
        }
        //GetList account group
        service.GetListAccountGroup = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListAccountGroupApiUrl,
            });
            return promise;
        }
        //GetList Allocation Reason
        service.GetListAllocationReason = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListAllocationReasonApiUrl + stringFilter,
            });
            return promise;
        }
        //GetList full relationship
        service.GetFullRelation = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + '/full-relationship/' + id,
            });
            return promise;
        }
        //export
        service.GetExportExelLink = function (code, tenBaoCao, pageNumber, model) {
            tenBaoCao = tenBaoCao.toUpperCase();
            var stringFilter = angular.toJson(model);
            return baseUrl + prefixApiUrl + "/export/" + code + "/" + tenBaoCao + "/" + pageNumber + "?stringFilter=" + stringFilter + "&token=841d49b8-2136-4cc1-a2a1-a62051827683";
        }
        return service;
    }]);
    angularAMD.service('CustomerClassApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/class";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetFilterClient = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetAllIsActive = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={'Status':1}"
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByIdClient = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client/" + id
            });
            return promise;
        };
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }
        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;
        }
        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('AccountCardApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var getListAllocationReasonApiUrl = ConstantsApp.API_CRM_ACCOUNT_CARD_REASON_URL_GET_LIST;
        var prefixApiUrl = "/api/erp/cards";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        //GetList Allocation Reason
        service.GetListAllocationReason = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListAllocationReasonApiUrl,
            });
            return promise;
        }
        service.GetFilterClient = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetAllIsActive = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={'Status':1}"
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByCardNumber = function (cardNumber) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?cardNumber=" + cardNumber
            });
            return promise;
        };
        service.GetByIdClient = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/client/" + id
            });
            return promise;
        };
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }
        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;
        }
        service.UpdateTotalPayment = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/update-total-payment/" + id,
                data: model
            });
            return promise;
        }
        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('DiscountItemApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var getListObjectTypeApiUrl = ConstantsApp.API_OBJECT_TYPE_URL_GET_LIST;
        var getListDiscountPercentApiUrl = ConstantsApp.API_DISCOUNT_PERCENT_URL_GET_LIST;
        var getListKiotTypeApiUrl = ConstantsApp.API_KIOT_TYPE_URL_GET_LIST;
        var getListTicketClassApiUrl = ConstantsApp.API_TICKET_CLASS_URL_GET_LIST;
        var prefixApiUrl = "/api/erp/discount-item";
        service.GetFilterItemObject = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetFilterTicketObject = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "-ticket?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        //api/erp/discount-point-by-order
        service.GetDiscountPointByOrder = function (model) {
            var listItem = angular.toJson(model);
            var promise = $http({
                method: 'POST',
                data: model,
                url: baseUrl + prefixApiUrl + "/discount-point-by-order",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }
        service.CreateMany = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + '/list-item',
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }
        service.CreateManyOnGrid = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + '/list-item-difference',
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;
        }
        service.UpdateMany = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }
        service.UpdateManyOnGrid = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + '-difference',
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        }
        //Get List object-type-loyalty
        service.GetListObjectType = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListObjectTypeApiUrl,
            });
            return promise;
        }
        //Get List Discount Percent
        service.GetListDiscountPercent = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListDiscountPercentApiUrl,
            });
            return promise;
        }
        //Get List Kiot Type
        service.GetListKiotType = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListKiotTypeApiUrl,
            });
            return promise;
        }
        //Get List Ticket Class
        service.GetListTicketClass = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListTicketClassApiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('LoyaltyCardClassHistoryApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/card-class-history";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        };
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/all"
            });
            return promise;
        };
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        };
        service.ExcelExport = function (model) {
            var stringFilter = angular.toJson(model);
            //var promise = $http({
            //    method: 'GET',
            //    url: baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter
            //});
            return baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };
        return service;
    }]);
    angularAMD.service('AccountTransactionPointApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/transaction-point";
        var getListSaleChannelApiUrl = ConstantsApp.API_SALE_CHANNEL_URL_GET_LIST;
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/all"
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByTransactionId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/by-transaction-id/" + id
            });
            return promise;
        };
        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }
        //lay danh sach danh sach kenh ban hang từ danh mục động
        service.GetListSaleChannel = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + getListSaleChannelApiUrl,
            });
            return promise;
        }
        service.ExcelExport = function (model) {
            var stringFilter = angular.toJson(model);
            //var promise = $http({
            //    method: 'GET',
            //    url: baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter
            //});
            return baseUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };
        return service;
    }]);
    angularAMD.service("TicketTypeApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các url
        var basedUrl = ConstantsApp.BASED_API_URL;
        var addTicketTypeApiUrl = ConstantsApp.API_TICKET_TYPE_URL_ADD_NEW;
        var editTicketTypeApiUrl = ConstantsApp.API_TICKET_TYPE_URL_EDIT;
        var getListTicketTypeApiUrl = ConstantsApp.API_TICKET_TYPE_URL_GET_LIST;
        var getListTicketTypeForPackageApiUrl = ConstantsApp.API_TICKET_TYPE_URL_GET_LIST_FOR_PACKAGE;
        var getByIdApiUrl = ConstantsApp.API_TICKET_TYPE_URL_GET_BY_ID;
        var deleteManyApiUrl = ConstantsApp.API_TICKET_TYPE_URL_DELETE_MANY;
        var getListObjectApiUrl = ConstantsApp.API_OBJECT_URL_GET_LIST;
        var getListFilmFormatApiUrl = ConstantsApp.API_FILM_FORMAT_URL_GET_LIST;
        var getListSeatTypeApiUrl = ConstantsApp.API_SEAT_TYPE_URL_GET_LIST;
        var getListSaleChanelApiUrl = ConstantsApp.API_SALE_CHANNEL_URL_GET_LIST;
        var getListTicketClassApiUrl = ConstantsApp.API_TICKET_CLASS_URL_GET_LIST;
        //#endregion

        //#region thêm, sửa, xóa ticket type
        //tao moi ticket type
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + addTicketTypeApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //sua ticket type
        service.Edit = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editTicketTypeApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //Xoa nhieu ticket type
        //tham số là list object ticket type
        service.DeleteMany = function (listTicketType) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listTicketType
            });
            return promise;
        };
        //#endregion thêm, sửa, xóa phòng chiếu
        service.GetList = function (pageNumber, pageSize, textSearch, workflowCurrentState, objectId, filmFormatId, seatType, saleChanelId, ticketClassId) {
            if (objectId == undefined) {
                objectId = "";
            }

            if (filmFormatId == undefined) {
                filmFormatId = "";
            }

            if (seatType == undefined) {
                seatType = "";
            }

            if (saleChanelId == undefined) {
                saleChanelId = "";
            }

            if (ticketClassId == undefined) {
                ticketClassId = "";
            }
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListTicketTypeApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize
                    + '&textSearch=' + textSearch + '&workflowCurrentState=' + workflowCurrentState
                    + '&objectId=' + objectId + '&filmFormatId=' + filmFormatId
                    + '&seatType=' + seatType + '&saleChanelId=' + saleChanelId
                    + '&ticketClassId=' + ticketClassId,
            });
            return promise;
        }

        service.GetListFullInfo = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/ticket-type/listfull' + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        service.GetListTicketTypeForPackage = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListTicketTypeForPackageApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        //Lấy danh sách các ticket type đang được áp dụng
        service.GetListActive = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/ticket-type/listactive',
            });
            return promise;
        }

        //lay thong tin ticket_type theo id
        service.GetById = function (ticketTypeId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getByIdApiUrl + '?ticketTypeId=' + ticketTypeId,
            });
            return promise;
        }

        //lay danh sach rap chieu 
        service.GetListObject = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListObjectApiUrl
            });
            return promise;
        }
        //lay danh sach rap chieu 
        service.GetListVAT = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/catalogs/catalogmaster/danh-muc-thue/itemlist'
            });
            return promise;
        }



        //lay danh sach định dạng film
        service.GetListFilmFormat = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListFilmFormatApiUrl,
            });
            return promise;
        }

        //lay danh sach loại chỗ ngồi
        service.GetListSeatType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListSeatTypeApiUrl,
            });
            return promise;
        }

        //lay danh sach danh sach kenh ban hang
        service.GetListSaleChanel = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListSaleChanelApiUrl,
            });
            return promise;
        }

        //lay danh sach danh sach kenh ban hang
        service.GetListTicketClass = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListTicketClassApiUrl,
            });
            return promise;
        }

        //#region Workflow
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/ticket-type/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#region list Workflow
        service.ProcessListDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/ticket-type/documents/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#endregion

        //Lấy mã sinh tự động
        service.GetTicketTypeAutoCode = function (prefix, suffixLength) {
            return $http({ method: 'GET', url: basedUrl + "/api/ticket-type/auto-code?prefix=" + prefix + "&suffixLength=" + suffixLength, });
        }

        return service;
    }]);
    angularAMD.service("FilmApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/film";

        //Thêm
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //Sửa
        service.Update = function (filmId, putData) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + '/' + filmId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //Xóa 1
        service.DeleteOne = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + '/' + id,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        }

        //Xóa nhiều
        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        }

        //Lấy thông tin theo Id
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/" + id,
            });
            return promise;
        }

        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }

        //Lấy phân trang
        service.GetFilter = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&textSearch=" + textSearch
            });
            return promise;
        }

        service.GetAdvanceFilter = function (pageNumber, pageSize, name, distributorId, status, startDate, endDate, hot, order) {
            var promise = $http({
                method: 'PUT',
                data: {
                    PageNumber: pageNumber,
                    PageSize: pageSize,
                    Name: name,
                    Status: status,
                    DistributorId: distributorId,
                    StartDate: startDate,
                    EndDate: endDate,
                    Hot: hot,
                    Order: order
                },
                url: apiUrl + "/adsearch"
            });
            return promise;
        }

        //Lấy tất cả film active
        service.GetAllFilmActive = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/active/true",
            });
            return promise;
        }

        //Lấy danh sách rạp có tỉ lệ phim
        service.GetListCinemaRatio = function (filmId, pageNumber, pageSize, cinemaName) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/" + filmId + "/cinema-ratio?cinemaName=" + cinemaName + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize,
            });
            return promise;
        }

        //Lấy danh sách rạp chưa có tỉ lệ phim
        service.GetListCinemaNoRatio = function (filmId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/" + filmId + "/cinema-no-ratio",
            });
            return promise;
        }

        //Lấy danh sách tỉ lệ phim theo rạp và phim
        service.GetListRatioByFilmAndCinema = function (filmId, cinemaId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/" + filmId + "/ratio?cinemaId=" + cinemaId,
            });
            return promise;
        }

        //Thêm mới tỉ lệ phim
        service.CreateFilmRatio = function (filmId, listCinemaId, listRatio) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + "/" + filmId + "/ratio",
                data: {
                    ListCinemaId: listCinemaId,
                    ListRatio: listRatio
                }
            });
            return promise;
        }

        //Cập nhật tỉ lệ phim
        service.UpdateFilmRatio = function (filmId, listRatio) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + "/" + filmId + "/ratio",
                data: listRatio
            });
            return promise;
        }

        //Xóa tỉ lệ phim
        service.DeleteFilmRatio = function (filmId, listCinemaId) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + "/" + filmId + "/delete-ratio",
                data: listCinemaId
            });
            return promise;
        }

        //Generate new code
        service.GenerateNewCode = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/generate-code?prefix=MOVIE&suffixlength=6",
            });
            return promise;
        }

        return service;
    }]);

    angularAMD.service("DiscountApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/v1/erp/discount";

        //Thêm
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //Sửa
        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl + '/' + putData.DiscountId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //Xóa 1
        service.DeleteOne = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl + '/' + id,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        }

        //Xóa nhiều
        service.DeleteMany = function (delData) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: delData
            });
            return promise;
        }

        //Lấy thông tin theo Id
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "/" + id,
            });
            return promise;
        }

        //Lấy tất cả
        service.GetAllActive = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/active',
            });
            return promise;
        }

        //Lấy theo đối tượng

        //1: Vé
        //2: Hàng hóa
        //3: Hóa đơn
        //null: Tất cả
        service.GetByObject = function (objectId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/filter-by-object?objectId=' + objectId,
            });
            return promise;
        }

        //Lấy phân trang
        service.GetFilter = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize + "&textSearch=" + textSearch
            });
            return promise;
        }

        return service;
    }]);
    angularAMD.service("UserRightApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/cinema/permission";

        //Lấy tất cả
        service.GetAllCinema = function (code) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '?rightCode=' + code,
            });
            return promise;
        }

        return service;
    }]);

    angularAMD.service("GenreApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_GENRE_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("FilmFormatApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_FILM_FORMAT_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("FilmDubbingApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_FILM_DUBBING_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("DistributorApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_DISTRIBUTOR_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("FilmRestrictAgeApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_FILM_RESTRICT_AGE_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("SubtitleApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_SUBTITTLE_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("TaxationApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_TAXATION_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service("DiscountTypeApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_DISCOUNT_TYPE_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);

    angularAMD.service("StudioApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_STUDIO_URL;
        var apiUrl = basedUrl + prefixApiUrl;
        //Lấy tất cả
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: apiUrl,
            });
            return promise;
        }
        return service;
    }]);

    angularAMD.service("PriceCardApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các url
        var basedUrl = ConstantsApp.BASED_API_URL;
        var addPriceCardApiUrl = "/api/price-card/add";
        var editPriceCardApiUrl = "/api/price-card/update";
        var getListPriceCardApiUrl = "/api/price-card/list";
        var getByIdApiUrl = "/api/price-card/getbyid";
        var deleteManyApiUrl = "/api/price-card/deletes";
        //#endregion

        //#region thêm, sửa, xóa price card
        //tao moi price card
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + addPriceCardApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //cap nhat priceCard
        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editPriceCardApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //Xoa nhieu price card
        //tham số là list object price card
        service.DeleteMany = function (listPriceCard) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listPriceCard
            });
            return promise;
        };
        //#endregion thêm, sửa, xóa phòng chiếu
        service.GetList = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListPriceCardApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        service.AdSearch = function (pageNumber, pageSize, approveStatus, status, cinemaId, workflowCurrentState) {
            var data = {
                PageNumber: pageNumber,
                PageSize: pageSize,
                ApproveStatus: approveStatus,
                Status: status,
                CinemaId: cinemaId,
                workflowCurrentState: workflowCurrentState
            };
            var promise = $http({
                method: 'POST',
                url: basedUrl + '/api/price-card/adsearch',
                data: data
            });
            return promise;
        }


        service.GetListPriceCardActive = function (filter) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/price-card/show?filter=' + filter,
            });
            return promise;
        }
        //lay thong tin price card theo id
        service.GetById = function (priceCardTypeId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getByIdApiUrl + '?priceCardId=' + priceCardTypeId,
            });
            return promise;
        }
        //#region Workflow
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/price-card/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#region list Workflow
        service.ProcessListDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/price-card/documents/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#endregion

        //Lấy mã sinh tự động
        service.GetPriceCardAutoCode = function (prefix, suffixlength) {
            return $http({ method: 'GET', url: basedUrl + "/api/price-card/auto-code?prefix=" + prefix + "&suffixlength=" + suffixlength, });
        }
        return service;
    }]);

    angularAMD.service("ItemTermsApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/v1/erp/item-terms";

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.DeleteMany = function (listId) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listId
            });
            return promise;
        };

        service.GetList = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        service.AdSearch = function (pageNumber, pageSize, approveStatus, status, cinemaId, workflowCurrentState) {
            var data = {
                PageNumber: pageNumber,
                PageSize: pageSize,
                ApproveStatus: approveStatus,
                Status: status,
                CinemaId: cinemaId,
                WorkflowCurrentState: workflowCurrentState
            };
            var promise = $http({
                method: 'POST',
                url: apiUrl + '/adsearch',
                data: data
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/' + id,
            });
            return promise;
        }

        service.GetListItemTermByCinema = function (cinemaId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/filter-by-cinema?cinemaId=' + cinemaId,
            });
            return promise;
        }

        service.GetListDiscountByCinemaAndItem = function (cinemaId, itemId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/discounts?cinemaId=' + cinemaId + '&itemId=' + itemId,
            });
            return promise;
        }

        //#region Workflow
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + "/workflow/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#region list Workflow
        service.ProcessListDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + "/workflow/documents/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#endregion
        return service;
    }]);

    angularAMD.service("BillTermsApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/v1/erp/bill-terms";

        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.DeleteMany = function (listId) {
            var promise = $http({
                method: 'DELETE',
                url: apiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listId
            });
            return promise;
        };

        service.GetList = function (pageNumber, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch,
            });
            return promise;
        }

        service.AdSearch = function (pageNumber, pageSize, approveStatus, status, cinemaId, workflowCurrentState) {
            var data = {
                PageNumber: pageNumber,
                PageSize: pageSize,
                ApproveStatus: approveStatus,
                Status: status,
                CinemaId: cinemaId,
                WorkflowCurrentState: workflowCurrentState
            };
            var promise = $http({
                method: 'POST',
                url: apiUrl + '/adsearch',
                data: data
            });
            return promise;
        }

        //lay thong tin theo id
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/' + id,
            });
            return promise;
        }

        service.GetListBillTermByCinema = function (cinemaId) {
            var promise = $http({
                method: 'GET',
                url: apiUrl + '/filter-by-cinema?cinemaId=' + cinemaId,
            });
            return promise;
        }

        //#region Workflow
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + "/workflow/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#region list Workflow
        service.ProcessListDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: apiUrl + "/workflow/documents/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#endregion
        return service;
    }]);

    angularAMD.service("DashboardApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var apiUrl = ConstantsApp.BASED_API_URL + "/api/v1/erp/dashboard";

        service.GetData = function (startDate, endDate) {
            var promise = $http({
                method: 'GET',
                //url: apiUrl + '/revenue?startDate=30/04/2018&endDate=10/05/2018',
                url: apiUrl + '/revenue?startDate=' + startDate + '&endDate=' + endDate,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        return service;
    }]);

    angularAMD.service("OnePayNoiDiaApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_PAYMENT_ONEPAY_GET_LIST_URL;

        service.GetFilterPayMent = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        };
        service.ExcelExport = function (filterBaoCaoModel) {
            var stringFilter = angular.toJson(filterBaoCaoModel);
            return basedUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };

        service.GetFilterPaymentRefund = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/refund/filter?stringFilter=" + stringFilter
            });
            return promise;
        };
        service.GetFilterBaoCaoPayMent = function (filterBaoCaoModel) {
            var stringFilter = angular.toJson(filterBaoCaoModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filterbaocao?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.GetBySoGiaoDich = function (soGiaoDich) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + soGiaoDich,
            });
            return promise;
        };

        service.GetByLoaiGD = function (soGiaoDich, loaiGiaoDich) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/payments?soGiaoDich=" + soGiaoDich + "&loaiGiaoDich=" + loaiGiaoDich,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.PostRefund = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/payment/onepay/refund",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.UpdateStatusFromOnePay = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/payment/onepay/updatestatusonepay",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        return service;
    }]);
    angularAMD.service("OnePayQuocTeApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_PAYMENT_ONEPAY_GET_LIST_URL;

        service.GetFilterPayMent = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        };
        service.ExcelExport = function (filterBaoCaoModel) {
            var stringFilter = angular.toJson(filterBaoCaoModel);
            return basedUrl + prefixApiUrl + "/excel-export?stringFilter=" + stringFilter;
        };
        service.GetFilterPaymentRefund = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/refund/filter?stringFilter=" + stringFilter
            });
            return promise;
        };
        service.GetBySoGiaoDich = function (soGiaoDich) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + soGiaoDich,
            });
            return promise;
        }
        service.GetByLoaiGD = function (soGiaoDich, loaiGiaoDich) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/payments?soGiaoDich=" + soGiaoDich + "&loaiGiaoDich=" + loaiGiaoDich,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        service.PostRefund = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/payment/onepay/refund",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.UpdateStatusFromOnePay = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/payment/onepay/updatestatusonepay",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        return service;
    }]);
    angularAMD.service("OnePayBankApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_BANK_ONEPAY_GET_LIST_URL;

        service.GetAllBank = function () {
            // var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl
            });
            return promise;
        }


        return service;
    }]);

    angularAMD.service("OnePayMerChantApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = ConstantsApp.API_MERCHANT_ONEPAY_GET_LIST_URL;

        service.GetAllMerchant = function (cardType) {
            // var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + prefixApiUrl + "/" + cardType,
            });
            return promise;
        }


        return service;
    }]);

    angularAMD.service("ShowApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các biến
        var basedUrl = ConstantsApp.BASED_API_URL;
        //#endregion

        //#region thêm, sửa, xóa phòng chiếu
        //tao moi phong chieu
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/show",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.GetPriceCardNote = function (cinemaId, dateShow) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/price-card/note?cinemaId=" + cinemaId + "&dateShow=" + dateShow,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        }
        service.GetAll = function (startDate, CinemaId, saleChanelCode, numberDay, isIgnoreLoadingBar) {
            if (saleChanelCode === undefined) {
                saleChanelCode = '';
            }
            if (numberDay === undefined) {
                numberDay = 7;
            }
            if (isIgnoreLoadingBar === undefined) {
                isIgnoreLoadingBar = false;
            }
            var promise = $http({
                method: 'GET',
                ignoreLoadingBar: isIgnoreLoadingBar,
                url: basedUrl + "/api/shows?startDate=" + startDate + "&cinemaId=" + CinemaId + "&saleChanelCode=" + saleChanelCode + "&numberDay=" + numberDay,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        //sua phong chieu
        service.Update = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/shows",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.UpdateMutiple = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/shows/update-mutilple",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        service.UpdateDragDrop = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/shows/dragdrop",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        //xoa phong chieu theo Id
        service.Delete = function (showId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "/api/shows/" + showId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };

        //Duyệt phong chieu theo Id
        service.Approve = function (showId, status) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "/api/shows/" + showId + "/approve/" + status,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };

        service.GetShowByIdAndSaleChannel = function (showId, saleChanelCode) {
            if (saleChanelCode === undefined) {
                saleChanelCode = '';
            }
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/shows/" + showId + "/sale-channel/" + saleChanelCode,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        //Xoa nhieu phong chieu
        //tham số là list ID phòng chiếu
        service.DeleteMany = function (listScreen) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listScreen
            });
            return promise;
        };
        //#endregion thêm, sửa, xóa phòng chiếu

        //lấy show chieu theo id
        service.GetById = function (showId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/shows/' + showId,
            });
            return promise;
        }


        //lay phong chiếu cuối cùng theo rap và loai phong chieu
        service.GetScreenByCinemaAndScreenTypeApiUrl = function (cinemaId, screenType, responseType) {
            var url = getScreenByCinemaAndScreenTypeApiUrl.format(cinemaId, screenType, responseType);
            var promise = $http({
                method: 'GET',
                url: basedUrl + url,
            });
            return promise;
        }

        //lay danh sach rap chieu 
        service.GetListCinema = function (pageNumber, pageSize, textSearch, cinemaTypeId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCinemaApiUrl + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize + '&textSearch=' + textSearch + '&cinemaTypeId=' + cinemaTypeId,
            });
            return promise;
        }
        //hoant
        //lay danh sach rap chieu filter và phan trang
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/shows/filter' + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        //lay danh sach rap chieu 
        service.GetAllCinemaActive = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/cinema/active/true',
            });
            return promise;
        }

        //lay danh sach loại rạp
        service.GetListCinemaType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCinemaTypeApiUrl,
            });
            return promise;
        }

        //lay danh sach tỉnh thành
        service.GetListCity = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCityApiUrl,
            });
            return promise;
        }
        //lay danh sach quản lý phòng - de tam day da
        service.GetListScreenManager = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListScreenManagerApiUrl,
            });
            return promise;
        }

        service.Duplicate = function (showDuplicate) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + '/api/show/duplicate',
                headers: {
                    'Content-type': ' application/json'
                },
                data: showDuplicate
            });
            return promise;
        }

        //#region Workflow
        service.ProcessDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/show/" + processModel.DocumentId + "/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //#region list Workflow
        service.ProcessListDocument = function (processModel) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/workflow/show/documents/process",
                headers: {
                    'Content-type': 'application/json'
                },
                data: processModel
            });
            return promise;
        };
        //Xoa nhieu chuong trinh uu dai
        service.DeleteManyShow = function (listShowId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "/api/shows/deletemany",
                headers: {
                    'Content-type': ' application/json'
                },
                data: listShowId
            });
            return promise;
        };

        //Lay danh sach giao dich theo showId
        service.GetListTransByShowId = function (showId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/shows/" + showId + "/transactions",
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };

        //Check trang thai cua 1 ghe
        service.CheckSeatSaleStatus = function (seatIndex, showId) {
            var promise = $http({
                method: 'GET',
                ignoreLoadingBar: true,
                url: basedUrl + '/api/shows/check-seat-sales?seatIndex=' + seatIndex + "&showId=" + showId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        }

        // Swap show
        service.SwapShow = function (sourceTransactionId, postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/shows/transaction/" + sourceTransactionId + "/swap",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.RevertSwapShow = function (sourceTransactionId, desTransactionId, sourceShowId, desShowId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/shows/transaction/revert-swap?sourceTransactionId=" + sourceTransactionId + "&desTransactionId=" + desTransactionId + "&sourceShowId=" + sourceShowId + "&desShowId=" + desShowId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.GenStatisticSwapShow = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "/api/shows/transaction/statistic-swap-show",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.GetStatisticSwapShow = function (leftShowId, rightShowId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "/api/shows/transaction/statistic-swap-show?leftShowId=" + leftShowId + "&rightShowId=" + rightShowId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteStatisticSwapShow = function (leftShowId, rightShowId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "/api/shows/transaction/statistic-swap-show?leftShowId=" + leftShowId + "&rightShowId=" + rightShowId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        //#endregion
        return service;
    }]);
    angularAMD.service("VoucherApiService", ["$http", "ConstantsApp", function ($http, ConstantsApp) {
        var service = {};
        //#region khai báo các biến
        var basedUrl = ConstantsApp.BASED_API_URL;
        var addVoucherApiUrl = ConstantsApp.API_VOUCHER_URL_ADD_NEW;
        var editVoucherApiUrl = ConstantsApp.API_VOUCHER_URL_EDIT;
        var editVoucherCardApiUrl = ConstantsApp.API_VOUCHER_CARD_URL_EDIT;
        var getListVoucherApiUrl = ConstantsApp.API_VOUCHER_URL_GET_LIST;
        var getListVoucherCardApiUrl = ConstantsApp.API_VOUCHER_CARD_URL_GET_LIST;
        var getListCityApiUrl = ConstantsApp.API_CITY_URL_GET_LIST;
        var getListDonViTinhApiUrl = ConstantsApp.API_VOUCHER_DONVITINH_URL_GET_LIST;
        var getListItemTypeApiUrl = ConstantsApp.API_VOUCHER_ITEMTYPE_URL_GET_LIST;
        var getListIssuingUnitApiUrl = ConstantsApp.API_VOUCHER_ISSUINGUNIT_URL_GET_LIST;
        var getListCardTypeApiUrl = ConstantsApp.API_VOUCHER_CARDTYPE_URL_GET_LIST;
        var deleteManyVoucherApiUrl = ConstantsApp.API_VOUCHER_URL_DELETE_MANY;
        var deleteManyVoucherCardApiUrl = ConstantsApp.API_VOUCHER_CARD_URL_DELETE_MANY;
        var getInfoVoucherCardByVoucherCodeAndItemTypeApiUrl = ConstantsApp.API_VOUCHER_CARD_INFO_URL_GET_LIST;
        //#endregion

        //Them moi chuong trinh uu dai
        service.AddNew = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + addVoucherApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        //sua chuong trinh uu dai
        service.Edit = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editVoucherApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };

        //Xoa nhieu chuong trinh uu dai
        service.DeleteMany = function (listVoucher) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyVoucherApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listVoucher
            });
            return promise;
        };
        //Xoa nhieu nhieu voucher card
        service.DeleteManyVoucherCard = function (listVoucherCard) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + deleteManyVoucherCardApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: listVoucherCard
            });
            return promise;
        };

        service.GetById = function (VoucherId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + '/api/Voucher/getbyid' + '?VoucherId=' + VoucherId
            });
            return promise;
        }
        //lay danh sach chuong trinh uu dai
        service.GetListVoucher = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListVoucherApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        }
        //lay danh sach voucher card
        service.GetListVoucherCard = function (filterModel) {
            var stringFilter = angular.toJson(filterModel);
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListVoucherCardApiUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        }
        //GetList don vi tinh
        service.GetListDonViTinh = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListDonViTinhApiUrl
            });
            return promise;
        }
        //GetList item type
        service.GetListItemType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListItemTypeApiUrl
            });
            return promise;
        }
        //GetList Issuing Unit
        service.GetListIssuingUnit = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListIssuingUnitApiUrl
            });
            return promise;
        }
        //GetList Card Type
        service.GetListCardType = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCardTypeApiUrl
            });
            return promise;
        }
        //lay danh sach tỉnh thành
        service.GetListCity = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getListCityApiUrl
            });
            return promise;
        }
        service.GetVoucherCardInfo = function (type, voucherCode) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + getInfoVoucherCardByVoucherCodeAndItemTypeApiUrl + "?type=" + type + "&voucherCode=" + voucherCode
            });
            return promise;
        }
        //cap nhat thong tin cua voucher card
        service.UpdateVoucherCard = function (putData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + editVoucherCardApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        return service;
    }]);
    angularAMD.service('PurchasingPartnerApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/purchasing-partner";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl,
                data: model,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        }
        return service;
    }]);
    angularAMD.service('NotificationCampaignApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var baseUrl = ConstantsApp.BASED_API_URL;
        var prefixApiUrl = "/api/erp/notification-campaign";
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };
        service.PushNotification = function (campaignId, device, blockSize) {
            return $http({ method: 'GET', url: ConstantsApp.BASED_API_MOBILE_URL + '/api/v1/erp/notifications/push/' + campaignId + '?sanboxMode=1&deviceType=' + device + '&blockSize=' + blockSize });
        }
        service.RePushNotification = function (campaignId) {
            return $http({ method: 'GET', url: baseUrl + '/api/erp/notification/re-push?campaignId=' + campaignId });
        }
        service.GetStatics = function (campaignId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/statics/" + campaignId
            });
            return promise;
        }
        service.Sync = function (campaignId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/sync-queue/" + campaignId
            });
            return promise;
        }
        return service;
    }]);
});