﻿/* SAVIS Vietnam Corporation
*
* This module is to separate configuration of routing to app.js
* DuongPD - Jan 2018
*/

define(function () {
    'use strict';

    var basedUrl = "/app-data/";

    /* App version */
    var version = "";
    var factory = {};

    /* Setup essential routing for site select */
    factory.setupSiteAccessConfig = function (app) {
        // Route config
        app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/site-access/default");
            $stateProvider
                .state('site-access', {
                    url: "/site-access/:mode",
                    templateUrl: basedUrl + "views/template/site-access/site-access.html",
                    data: { pageTitle: 'Chọn hệ thống truy cập' },
                    controller: "SiteAccessCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/template/site-access/site-access.js'
                                ]
                            });
                        }]
                    }
                })
        }]);
    };

    /* Setup all routings */
    factory.setupRouteConfig = function (app) {
        version = app.Version;
        /* Setup Rounting For All Pages */
        app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/dashboard");

            $stateProvider
                // #region System core
                .state('dashboard', {
                    url: "/dashboard",
                    //templateUrl: basedUrl + "views/business/dashboard/dashboard.html" + version,
                    data: { pageTitle: 'Trang chủ' },
                    controller: "DashboardController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                //    'app-data/views/business/dashboard/dashboard.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('error-404', {
                    url: "/404",
                    templateUrl: basedUrl + "views/template/page-error/page_system_404_2.html" + version,
                    data: { pageTitle: 'Không tìm thấy trang' },
                })
                .state('systemtracking', {
                    url: "/system-tracking",
                    templateUrl: basedUrl + "views/core/system-tracking/system-tracking.html" + version,
                    data: { pageTitle: 'Nhật ký hệ thống' },
                    controller: "SystemTrackingController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/system-tracking/system-tracking.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('role', {
                    url: "/role",
                    templateUrl: basedUrl + "views/core/role/role-list.html" + version,
                    data: { pageTitle: 'Quản lý nhóm người dùng' },
                    controller: "RoleController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/role/role-list.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('right', {
                    url: "/right",
                    templateUrl: basedUrl + "views/core/right/right-list.html" + version,
                    data: { pageTitle: 'Quản lý quyền người dùng' },
                    controller: "RightController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/right/right-list.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('right-of-user', {
                    url: "/system/permission/user",
                    templateUrl: basedUrl + "views/core/right-of-user/right-of-user.html" + version,
                    data: { pageTitle: 'Phân quyền người dùng' },
                    controller: "RightOfUserController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/right-of-user/right-of-user.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('user', {
                    url: "/users",
                    templateUrl: basedUrl + "views/core/users/users.html" + version,
                    data: { pageTitle: 'Quản lý người dùng' },
                    controller: "UserController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/components/directives/checklist-model.js' + version,
                                    'app-data/views/core/users/users.js' + version,
                                    'app-data/views/core/users/users-ad.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('employee', {
                    url: "/employee",
                    templateUrl: basedUrl + "views/core/employee/employee-list.html" + version,
                    data: { pageTitle: 'Quản lý nhân viên bán hàng' },
                    controller: "EmployeeController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/components/directives/checklist-model.js' + version,
                                    'app-data/views/core/employee/employee-list.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('organization', {
                    url: "/organization",
                    templateUrl: basedUrl + "views/core/organization/organization-list.html" + version,
                    data: { pageTitle: 'Quản lý đơn vị, phòng ban' },
                    controller: "OrganizationCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/organization/organization-list.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('users', {
                    url: "/users",
                    templateUrl: basedUrl + "views/core/users/users.html" + version,
                    data: { pageTitle: 'Quản lý người dùng' },
                    controller: "UsersCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/users/users.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('navigation', {
                    url: "/navigation",
                    templateUrl: basedUrl + "views/core/navigation/navigation.html" + version,
                    data: { pageTitle: 'Quản lý, phân quyền điều hướng' },
                    controller: "NavigationCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/core/navigation/navigation.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('catalog', {
                    url: "/catalog",
                    templateUrl: basedUrl + "views/core/catalog/catalog-client.html" + version,
                    data: { pageTitle: 'Danh mục tổng' },
                    controller: "CatalogClientCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    //'app-data/components/formly-template/formly-factory.js',
                                    'bower_components/col-resizable/colResizable-1.6.js' + version,
                                    'bower_components/angular-bootstrap-contextmenu/contextMenu.js' + version,
                                    'bower_components/angular-messages/angular-messages.min.js' + version,
                                    'app-data/views/core/catalog/metadata-template/metadata-field-select.js' + version,
                                    'app-data/views/core/catalog/metadata-field/metadata-field-item.js' + version,
                                    'app-data/views/core/catalog/metadata-field/metadata-field.js' + version,
                                    'app-data/views/core/catalog/catalog-item.js' + version,
                                    'app-data/views/core/catalog/catalog-client-item.js' + version,
                                    'app-data/views/core/catalog/catalog-client.js' + version,

                                ]
                            });
                        }]
                    }
                })
                .state('film', {
                    url: "/film",
                    templateUrl: basedUrl + "views/business/film/film-list.html" + version,
                    data: { pageTitle: 'Danh sách phim' },
                    controller: "ListFilmController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/film/film-list.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('create-film', {
                    url: "/create-film",
                    templateUrl: basedUrl + "views/business/film/film-activity.html" + version,
                    data: { pageTitle: 'Thêm mới phim', inheritFrom: 'film' },
                    controller: "CreateFilmController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/film/film-activity.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('copy-film', {
                    url: "/copy-film/:filmId",
                    templateUrl: basedUrl + "views/business/film/film-activity.html" + version,
                    data: { pageTitle: 'Sao chép phim', cmd: 'copy', inheritFrom: 'film' },
                    controller: "CreateFilmController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/film/film-activity.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('update-film', {
                    url: "/update-film/:filmId",
                    templateUrl: basedUrl + "views/business/film/film-activity.html" + version,
                    data: { pageTitle: 'Cập nhật phim', cmd: 'update', inheritFrom: 'film' },
                    controller: "CreateFilmController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/film/film-activity.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('danh-sach-rap-chieu', {
                    url: "/danh-sach-rap-chieu",
                    templateUrl: basedUrl + "views/business/rap-chieu/list.html" + version,
                    data: { pageTitle: 'Danh sách rạp chiếu' },
                    controller: "ListCinemaCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/rap-chieu/list.js' + version,
                                    'app-data/views/business/rap-chieu/create.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('point-of-sale', {
                    url: "/point-of-sale",
                    templateUrl: basedUrl + "views/business/point-of-sale/point-of-sale-list.html" + version,
                    data: { pageTitle: 'Quản lý điểm bán hàng (POS)' },
                    controller: "PointOfSaleController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/point-of-sale/point-of-sale-list.controller.js' + version,
                                    'app-data/views/business/point-of-sale/point-of-sale-item.controller.js' + version
                                    // 'app-data/views/business/point-of-sale/point-of-sale-item.js' + version
                                ]
                            });
                        }]
                    }
                })
                // #endregion
                .state('datetime', {
                    url: "/datetime",
                    templateUrl: basedUrl + "views/business/loyalty/datetime.html" + version,
                    data: { pageTitle: 'Template datetime control' },
                    controller: "DatetimeCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'SavisApp',
                                files: [
                                    'app-data/views/business/loyalty/datetime.js' + version
                                ]
                            });
                        }]
                    }
                })
            ;
            //$locationProvider.html5Mode(true);
        }]);
    };

    return factory;
});
