﻿
/* SAVIS Vietnam Corporation
*
* This module is to separate configuration of authentication to app.js
* DuongPD - Jan 2018
*/

define(["jquery"], function ($) {
    'use strict';

    /* Authorization configs 
    *  configure service based uri, client id, login and logout redirect urls
    *  we can also configure the time client will redirect to login page to re-new tokens
    */

    var authConfig = {
        authServiceBaseUri: 'https://183.81.32.66:9025/',
        clientId: 'fgfnYoCtg8QRUDLPhwSzrqM7zIwa',
        logInRedirect: 'http://localhost:9027/index.html',
        logOutRedirect: 'http://localhost:9027',
        apiUrl: "http://183.81.32.66:9032",
        redirectAfter: 10,//(s)
        refreshTokenBeforeExpires: 120, //(s),       
    };

    var sessionTimeoutConfig = {
        Enable: false,
        Mode: "idle",// session or idle 
        WarnAfter: 5000,
        RedirAfter: 60000,
    }

    var initLogInUrl = function () {

        var authorizationUrl = authConfig.authServiceBaseUri;
        var client_id = authConfig.clientId;
        var redirect_uri = authConfig.logInRedirect;
        var response_type = "code";
        var scope = "openid";

        var url = authorizationUrl + "oauth2/authorize?" +
            "client_id=" + encodeURIComponent(client_id) + "&" +
            "redirect_uri=" + encodeURIComponent(redirect_uri) + "&" +
            "response_type=" + encodeURI(response_type) + "&" +
            "scope=" + encodeURIComponent(scope);

        return url;
    };

    var initLogOutUrl = function () {
        var authorizationUrl = authConfig.authServiceBaseUri;
        var id_token = window.localStorage['id_token'];
        var redirect_uri = authConfig.logOutRedirect;

        var logOutUrl =
            authorizationUrl + "oidc/logout?" +
            "post_logout_redirect_uri=" + encodeURIComponent(redirect_uri) + "&" +
            "id_token_hint=" + encodeURIComponent(id_token);

        return logOutUrl;
    };

    var clearLocalStorage = function () {
        window.localStorage.removeItem('valid_user');
        window.localStorage.removeItem('access_token');
        window.localStorage.removeItem('refresh_token');
        window.localStorage.removeItem('expires_in');
        window.localStorage.removeItem('expires_time');
        window.localStorage.removeItem('id_token');
        window.localStorage.removeItem('code');
        window.localStorage.removeItem('session_state');
        window.localStorage.removeItem('user_id');
        window.localStorage.removeItem('display_name');
        window.localStorage.removeItem('site_id');
        window.localStorage.removeItem('isCheckedVersion');
    };

    var sessionTimeOut = function () {
        var logOutUrl = "";
        var option = {
            keepAlive: false,
            warnAfter: sessionTimeoutConfig.WarnAfter,
            redirAfter: sessionTimeoutConfig.WarnAfter + sessionTimeoutConfig.RedirAfter,
            logoutUrl: logOutUrl,
            redirUrl: logOutUrl,
            logoutButton: "Đăng xuất",
            keepAliveButton: "Tiếp tục phiên",
            countdownSmart: true,
            countdownMessage: 'Hệ thống sẽ tự động đăng xuất sau {timer}',
            countdownBar: true,
            onRedir: function () {
                factory.logOut();
            }
        };
        if (sessionTimeoutConfig.Mode === "session") {
            option.title = "Cảnh báo phiên làm việc";
            option.message = 'Phiên làm việc của bạn đã hết. Chọn <strong>Đăng xuất</strong> hoặc <strong>Tiếp tục phiên</strong> để tiếp tục làm việc';
            option.ignoreUserActivity = true;
            option.isShowButton = true;
        } else if (sessionTimeoutConfig.Mode === "idle") {
            option.title = "Cảnh báo nhàn rỗi";
            option.message = "Không phát hiện hoạt động người dùng";
            option.ignoreUserActivity = false;
            option.isShowButton = false;
        }
        $.sessionTimeout(option);
    };

    var refreshToken = function (isCheckExpire) {
        isCheckExpire = typeof isCheckExpire === "undefined" ? true : false;
        var tokenRefresh = window.localStorage["refresh_token"];
        var timeActionRefresh = new Date(window.localStorage["expires_time"]).getTime() - authConfig.refreshTokenBeforeExpires * 1000;
        var timeAtMoment = new Date().getTime();
        if ((tokenRefresh && typeof tokenRefresh !== 'undefined' && timeAtMoment >= timeActionRefresh) || !isCheckExpire) {

            var xhr = new XMLHttpRequest();
            xhr.open("GET", authConfig.apiUrl + "/api/v1/wso2/" + tokenRefresh + "/refreshtoken", false);
            xhr.setRequestHeader("X-ClientId", authConfig.clientId);
            xhr.send();

            var authInfo = JSON.parse(xhr.response);

            if (authInfo.Status === 1) {
                window.localStorage['access_token'] = authInfo.Data.access_token;
                window.localStorage['expires_in'] = authInfo.Data.expires_in;
                window.localStorage['expires_time'] = authInfo.Data.expires_time;
                window.localStorage['id_token'] = authInfo.Data.id_token;
                window.localStorage['refresh_token'] = authInfo.Data.refresh_token;
            }
        }
    };

    var infoDialog = function ($uibModal, message, title, buttonConfirm, buttonCancel, popupSize, popupData) {
        var infoDialogTemplateUrl = '/app-data/views/template/confirm/info-dialog.html';
        var modalInstance = $uibModal.open({
            templateUrl: infoDialogTemplateUrl,
            controller: 'InfoDialogCtrl',
            size: popupSize,
            backdrop: "static",
            keyboard: false,
            resolve: {
                data: function () {
                    var obj = popupData;
                    return obj;
                },
                option: function () {
                    return {
                        Message: message,
                        Title: title,
                        ButtonConfirm: buttonConfirm,
                        ButtonCancel: buttonCancel
                    };
                }
            }
        });

        return modalInstance;
    };

    var factory = {};

    // Init auth configuration, add authConfig to constant of app module
    factory.refreshTokenAfterLogin = function () {
        refreshToken(false);
    };

    factory.getAuthenInfo = function () {
        var result = factory.checkAuthToken();
        if (!result) {
            window.common = (function () {
                var common = {};

                common.getFragment = function getFragment() {
                    if (window.location.search.indexOf("?") === 0) {
                        return parseQueryString(window.location.search.substr(1));
                    } else {
                        return {};
                    }
                };

                // This API call service of Identity Server (WSO2)
                common.getAuthenticationInfoByCode = function getAuthenticationInfoByCode(code) {
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", authConfig.apiUrl + "/api/v1/wso2/" + code + "/authenticationinfo", false);
                    xhr.setRequestHeader("X-ClientId", authConfig.clientId);
                    xhr.send();

                    var json = JSON.parse(xhr.response);
                    console.log("getAuthenticationInfoByCode", xhr.response);
                    return json;
                }

                function parseQueryString(queryString) {
                    var data = {},
                        pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

                    if (queryString === null) {
                        return data;
                    }

                    pairs = queryString.split("&");

                    for (var i = 0; i < pairs.length; i++) {
                        pair = pairs[i];
                        separatorIndex = pair.indexOf("=");

                        if (separatorIndex === -1) {
                            escapedKey = pair;
                            escapedValue = null;
                        } else {
                            escapedKey = pair.substr(0, separatorIndex);
                            escapedValue = pair.substr(separatorIndex + 1);
                        }

                        key = decodeURIComponent(escapedKey);
                        value = decodeURIComponent(escapedValue);

                        data[key] = value;
                    }
                    return data;
                }

                return common;
            })();
            var fragment = common.getFragment();

            if (fragment.code !== null && typeof fragment.code !== "undefined") {

                window.localStorage['code'] = fragment.code;
                window.localStorage['session_state'] = fragment.session_state;
                var authInfo = common.getAuthenticationInfoByCode(fragment.code);
                if (authInfo.Status === 1) {                    
                    if (authInfo.Data.UserInfo.IsAccessible) window.localStorage['site_id'] = authInfo.Data.UserInfo.site_id;
                    if (!authInfo.Data.UserInfo.IsActiveAndExist) window.localStorage['valid_user'] = "0";
                    else window.localStorage['valid_user'] = "1";
                    window.localStorage['access_token'] = authInfo.Data.access_token;
                    window.localStorage['expires_in'] = authInfo.Data.expires_in;
                    window.localStorage['expires_time'] = authInfo.Data.expires_time;
                    window.localStorage['id_token'] = authInfo.Data.id_token;
                    window.localStorage['refresh_token'] = authInfo.Data.refresh_token;
                    window.localStorage['user_id'] = authInfo.Data.UserInfo.user_id;
                    window.localStorage['display_name'] = authInfo.Data.UserInfo.display_name;

                    window.location = "/";
                }
            }
        }
    };

    factory.initAuthConfig = function (app) {
        app.constant('ngAuthSettings', authConfig);
        app.constant("ngAuthController", factory);
    };

    factory.checkAuthToken = function () {

        var authData = window.localStorage['access_token'];
        var expiresTime = window.localStorage["expires_time"];
        // Check has value
        if (!authData || typeof authData === 'undefined' || !expiresTime || typeof expiresTime === 'undefined') return false;

        var timeExpires = new Date(expiresTime).getTime();
        var timeAtMoment = new Date().getTime();

        // Check time expires
        if (timeExpires < timeAtMoment) return false;

        return true;
    };

    factory.setupAuth = function (app) {
        // Setup session timeout
        if (sessionTimeoutConfig.Enable) sessionTimeOut();

        // This variable keeps dialog just open 1 time
        var isDialogOpened = false;

        var openDialog = function ($uibModal, url, message, title, buttonConfirm, buttonCancel, popupSize) {
            var infoDialogTemplateUrl = '/app-data/views/template/confirm/connect-expired.html';
            var modalInstance = $uibModal.open({
                templateUrl: infoDialogTemplateUrl,
                controller: 'ConnectExpireDialogCtrl',
                size: popupSize,
                backdrop: 'static',
                resolve: {
                    data: function () {
                        var obj = {};
                        obj.Url = url;
                        return obj;
                    },
                    option: function () {
                        return { Message: message, Title: title, ButtonConfirm: buttonConfirm, ButtonCancel: buttonCancel };
                    }
                }
            });

            isDialogOpened = true;

            return modalInstance;
        };

        // Controller for displaying connect expired dialog
        app.controller('ConnectExpireDialogCtrl', ['$scope', '$timeout', '$uibModalInstance', 'data', 'option',
            function ($scope, $timeout, $uibModalInstance, data, option) {
                $scope.Title = option.Title;
                $scope.Message = option.Message;
                $scope.ButtonConfirm = option.ButtonConfirm;
                $scope.ButtonCancel = option.ButtonCancel;
                $scope.Url = data.Url;

                $scope.Confirm = function () {
                    $uibModalInstance.dismiss('cancel');
                }

                // debugger
                $timeout(function () {
                    window.location = $scope.Url;
                }, authConfig.redirectAfter * 1000);
            }]);

        // inject http request to add bearer token
        app.factory('authInterceptorService', ['$q', '$injector', '$location', 'ngAuthSettings', function ($q, $injector, $location, ngAuthSettings) {

            var authInterceptorServiceFactory = {};
            let $uibModal;

            var _request = function (config) {
                config.headers = config.headers || {};

                var authData = window.localStorage['access_token'];
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData;
                }
                config.url = config.url.replace('[[configDomain]]', authConfig.apiUrl);
                return config;
            }


            var _responseError = function (rejection) {
                console.log("rejection", rejection);
                //|| rejection.status === -1
                if (rejection.status === 401 || rejection.status === -1) {
                    $uibModal = $uibModal || $injector.get('$uibModal');

                    var logOutUrl = initLogOutUrl();
                    clearLocalStorage();

                    // Show dialog
                    var message = "Phiên xác thực của bạn không hợp lệ, vui lòng đăng nhập lại!";
                    //var message = "Phiên làm việc của bạn không hợp lệ hoặc hệ thống chưa được kích hoạt bản quyền, vui lòng liên hệ với quản trị hệ thống để được hỗ trợ!";
                    if (!isDialogOpened) openDialog($uibModal, logOutUrl, message, 'Cảnh báo', '', '', '');

                } else if (rejection.status === 403) {
                    $location.path('/forbidden');
                }
                //else if (rejection.status === -1) {                   
                //    // Show dialog
                //    message = "Lỗi không xác định!";
                //    if (!isDialogOpened) openDialog($uibModal, null, message, 'Cảnh báo', 'Xác nhận', '', '');
                //}
                return $q.reject(rejection);
            }

            authInterceptorServiceFactory.request = _request;
            authInterceptorServiceFactory.responseError = _responseError;

            return authInterceptorServiceFactory;
        }]);

        app.config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
        });

        app.run(['$http', '$uibModal', '$rootScope', '$route', '$location', 'ngAuthSettings', function ($http, $uibModal, $rootScope, $route, $location, ngAuthSettings) {

            //Do your $on in here, like this:
            $rootScope.$on("$locationChangeStart", function (event, next, current) {

                $uibModal = $uibModal || $injector.get('$uibModal');
                //Do your things
                var authData = window.localStorage['access_token'];

                if (!authData || typeof authData === 'undefined') {

                    var url = initLogOutUrl();
                    clearLocalStorage();

                    if (!isDialogOpened) openDialog($uibModal, url, 'Phiên xác thực của bạn không hợp lệ', 'Cảnh báo', '', '', '');

                }
            });
        }]);
    };

    factory.setupNonAuth = function (app) {
        // inject http request to add bearer token
        app.factory('authInterceptorService', ['$q', '$injector', '$location', 'ngAuthSettings', function ($q, $injector, $location, ngAuthSettings) {
            var authInterceptorServiceFactory = {};
            var _request = function (config) {
                config.url = config.url.replace('[[configDomain]]', authConfig.apiUrl);
                return config;
            }
            authInterceptorServiceFactory.request = _request;

            return authInterceptorServiceFactory;
        }]);

        app.config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
        });
    };

    factory.toLoginForm = function () {
        var logInUrl = initLogInUrl();
        // Routing to login page
        window.location = logInUrl;
    }

    factory.logOut = function () {
        var logOutUrl = initLogOutUrl();
        clearLocalStorage();
        // Routing to logout page
        window.location = logOutUrl;
    };

    factory.init = function () {
        factory.getAuthenInfo();

        // Set interval auto refresh token
        var refreshTokenIntervalTime = authConfig.refreshTokenBeforeExpires / 4;//(s)
        window.setInterval(refreshToken, refreshTokenIntervalTime * 1000);
    };

    // Init authen info when requirejs load config-auth.js
    factory.init();

    return factory;
});
