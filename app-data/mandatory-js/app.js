﻿// <reference path="main.js" />
define(['angularAMD', 'jquery',
    'config-route', 'config-auth',
    'service-formly-template',
    'SidebarModule',
    'HeaderModule',
    'directives-common',
    'angular-ui-router',
    'angular-route',
    'jquery-ui',
    'jquery.pulsate',
    'jquery.scannerdetection',
    'angular-loading-bar',
    'state-event',
    'service-api',
    'service-amd',
    'ngfileupload',
    'service-formly',
    'angular-formly',
    'angular-formly-templates-bootstrap',
    'constant-app',
    'ui-bootstrap',
    'oclazyload',
    'angular-toastr',
    'angular-sanitize',
    'angular-ui-select',
    'angular-ui-select2',
    'select2',
    'angular-base64',
    'App', 'Layout',
    'angular-datepicker',
    'bootstrap-session-timeout',
    'FooterModule',
    'bootstrap-timepicker',
    'daypilot',
    'signalR',
    'textAngular',
    'googleChart'
], function (angularAMD, $, routeConfig, authConfig, formlyConfig, sidebarModule) {
    'use strict';
    var savisApp = angular.module('SavisApp', [
        "ui.router",
        "ngRoute",
        'ui.router.state.events',
        "ui.select",
        "ui.select2",
        "ui.bootstrap",
        "oc.lazyLoad",
        "ngSanitize",
        "toastr",
        "base64",
        "angular-loading-bar",
        "720kb.datepicker",
        'formly',
        'formlyBootstrap',
        'ngFileUpload',
        'daypilot',
        'textAngular'
    ]);

    /* Setup global settings */
    savisApp.factory('settings', ['$rootScope', function ($rootScope) {
        // supported languages
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageContentWhite: true, // set page content layout
                pageBodySolid: false, // solid body color state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            assetsPath: '../../assets',
            globalPath: '../../assets/global',
            layoutPath: '../../assets/layouts/layout',
        };

        $rootScope.settings = settings;

        return settings;
    }]);

    /* Setup app version */
    savisApp.Version = "?bust=v1.2.14";

    // #region Setup Auth
    var isAuthenMode = true;
    // 1. Init config add to constant of app module
    authConfig.initAuthConfig(savisApp);
    // 2. Check auth in local storage
    var hasAuthToken = authConfig.checkAuthToken(savisApp);
    if (isAuthenMode) {
        if (hasAuthToken) {
            // Setup auth
            authConfig.setupAuth(savisApp, $);
            authConfig.refreshTokenAfterLogin();
            var siteId = window.localStorage['site_id'];
            var validUser = window.localStorage['valid_user'];            
            // Setup config for Routing
            if (siteId === null || typeof siteId === 'undefined' || siteId === 'undefined' || validUser === '0') {
                routeConfig.setupSiteAccessConfig(savisApp);
            } else {
                //Show template
                $("#headerDiv").css('display', '');
                $("#footerDiv").css('display', '');
                $("#sidebarDiv").css('display', '');
                setTimeout(function () {
                    $("#loadingDiv").css('display', 'none');
                }, 0);
                routeConfig.setupRouteConfig(savisApp);
            }
        } else {
            authConfig.toLoginForm();
        }
    } else {
        $("#headerDiv").css('display', '');
        $("#footerDiv").css('display', '');
        $("#sidebarDiv").css('display', '');
        $("#loadingDiv").css('display', 'none');

        // Setup route

        authConfig.setupNonAuth(savisApp);
        routeConfig.setupRouteConfig(savisApp);
    }
    // #endregion

    // #region Init formly
    formlyConfig.init(savisApp);
    // #endregion 

    // #region Init user info
    var initUserInfo = function () {
        savisApp.CurrentUser = {
            Id: window.localStorage['user_id'],
            //UserName: window.localStorage['user_name'],
            DisplayName: window.localStorage['display_name'],
            ApplicationId: window.localStorage['site_id']
        };
        if (!isAuthenMode) {
            savisApp.CurrentUser.Id = "07b2fd44-dd17-49f3-97a3-851b8835eae0";
            //savisApp.CurrentUser.UserName = "beta.admin";
            savisApp.CurrentUser.DisplayName = "Beta Administrator";
            savisApp.CurrentUser.ApplicationId = "48ed5b71-66dc-4725-9604-4c042e45fa3f";
        }
    };
    initUserInfo();
    // #endregion

    // #region Setup loading bar
    savisApp.config(["cfpLoadingBarProvider", function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div class="spinner-icon"></div>Đang nạp dữ liệu, vui lòng đợi ...</div>';
    }]);
    // #endregion

    // #region Setup Notifications
    savisApp.config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            tapToDismiss: true,
            closeButton: true,
            progressBar: true,
            timeOut: 5000,
            closeHtml: '<button>&times;</button>',
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });
    });
    // #endregion


    savisApp.run(["$rootScope", "settings", "$state", "$http", "UtilsService", function ($rootScope, settings, $state, $http, UtilsService) {

        // Set message headers 
        $http.defaults.headers.common['X-User'] = savisApp.CurrentUser.Id;
        $http.defaults.headers.common['X-ApplicationId'] = savisApp.CurrentUser.ApplicationId;

        // Init UtilsService
        UtilsService.init(savisApp);

        $rootScope.UserInfo = savisApp.CurrentUser;

        $rootScope.$state = $state;
        $rootScope.$settings = settings;

        $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
            Layout.setSidebarMenuActiveLink("match");
        });

        // Filter URL by permission
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            
            var toStateName = "";
            var listStateOfUser = UtilsService.User.ListStates;
            if (isAuthenMode) {
                if (listStateOfUser.length !== 0) {
                    // Kiểm tra có kế thừa từ state cha
                    var isInherit = (typeof toState.data.inheritFrom !== "undefined" && toState.data.inheritFrom !== "") ? true : false;
                    if (isInherit) toStateName = toState.data.inheritFrom;
                    else toStateName = toState.name;

                    var isStatePermission = listStateOfUser.filter(function (dt) { return dt.State === toStateName; })[0];
                    if (!(typeof isStatePermission !== "undefined" && isStatePermission !== null)) {
                        console.log("URL Access denied :" + toState.url);
                        var urlFistOrDefault = listStateOfUser.filter(function (dt) { return dt.Url !== "" && typeof dt.Url !== "undefined" })[0];
                        if (typeof urlFistOrDefault !== "undefined" && urlFistOrDefault !== null) {
                            window.location = urlFistOrDefault.Url;
                        } else
                            window.location = "/#!/dashboard";
                    }
                }
                else window.location = "/#!/dashboard";
            }
        });
    }]);

    String.prototype.format = function () {
        var a = this;
        for (var k in arguments) {
            a = a.replace("{" + k + "}", arguments[k])
        }
        return a
    }

    //#region Init template
    sidebarModule.init(savisApp);
    //#endregion

    return angularAMD.bootstrap(savisApp);
});