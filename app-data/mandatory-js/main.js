﻿/* 
* Main function Require
* By Savis Software Team
* Author: DuongPD
*/
require.config({
    baseUrl: "/app-data/mandatory-js",
    urlArgs: "bust=v1.2.14",
    waitSeconds: 0,
    paths: {
        /* 1.BEGIN CORE JQUERY PLUGINS */
        "jquery": ["../../bower_components/jquery/dist/jquery.min",
            "../../bower_components/jQuery/dist/jquery.min",
            "../../bower_components/jquery/jquery.min"],
        "moment": "../../bower_components/moment/min/moment.min",
        "bootstrap": "../../bower_components/bootstrap/dist/js/bootstrap.min",
        "jquery-slimscroll": "../../bower_components/jquery-slimscroll/jquery.slimscroll",
        "blockui": "../../bower_components/blockui/jquery.blockUI",
        "jquery-cookie": "../../bower_components/js-cookie/src/js.cookie",
        "bootstrap-switch": "../../bower_components/bootstrap-switch/dist/js/bootstrap-switch.min",
        "bootstrap-session-timeout": "../../bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout",
        "bootstrap-hover-dropdown": "../../bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min",
        /* END CORE JQUERY PLUGINS */

        /* 2.BEGIN CORE ANGULARJS PLUGINS */
        "angular": "../../bower_components/angular/angular.min",
        "angular-sanitize": "../../bower_components/angular-sanitize/angular-sanitize.min",
        "angular-loading-bar": "../../bower_components/angular-loading-bar/build/loading-bar",
        "angular-ui-router": "../../bower_components/angular-ui-router/release/angular-ui-router.min",
        "angular-route": "../../bower_components/angular-route/angular-route.min",
        "state-event": "../../bower_components/angular-ui-router/release/stateEvents.min",
        // Todo: chạy lại mini js thì chuyển sang select.min.js
        "angular-ui-select": "../../bower_components/angular-ui-select/dist/select",
        "angular-ui-select2": "../../bower_components/angular-ui-select2/src/select2",
        "select2": "../../bower_components/select2/dist/js/select2.min",
        "oclazyload": "../../bower_components/oclazyload/dist/oclazyload.min",
        "ui-bootstrap": "../../bower_components/angular-bootstrap/ui-bootstrap-tpls",
        "angularAMD": "../../bower_components/angularAMD/angularAMD.min",
        "angular-moment": "../../bower_components/angular-moment/angular-moment",
        "angular-daterangepicker": "../../bower_components/angular-daterangepicker/js/angular-daterangepicker",
        "jquery-ui": "../../bower_components/jquery-ui/jquery-ui.min",
        "jquery.pulsate": "../../bower_components/jquery.pulsate/jquery.pulsate",
        "jquery.scannerdetection": "../../bower_components/jquery-scanner-detection/jquery.scannerdetection",
        'api-check': '../../bower_components/api-check/dist/api-check.min',
        "angular-formly": "../../bower_components/angular-formly/dist/formly.min",
        "angular-formly-templates-bootstrap": "../../bower_components/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min",
        "ckeditor": "../../bower_components/ckeditor/ckeditor",
        "signalR": "../../assets/global/plugins/signalR/jquery.signalR-2.2.2",
        'textAngular': '../../bower_components/textAngular/textAngular.min',
        'googleChart': '../../assets/global/plugins/google-chart/google-chart-loader.js?',
        /* END CORE ANGULARJS PLUGINS */

        /* 3.BEGIN LEVEL CUSTOM PLUGINS */
        "directives-common": "../components/directives/directives-common",
        "config-route": "../components/config/config-route",
        "config-auth": "../components/config/config-auth",
        "angular-toastr": "../../bower_components/angular-toastr/dist/angular-toastr.tpls",
        "service-api": "../components/services/service-api",
        "service-amd": "../components/services/service-amd",
        "ngfileupload": "../../bower_components/ng-file-upload/ng-file-upload.min",
        "service-formly": "../components/formly-template/formly-factory",
        "service-formly-template": "../components/formly-template/formly-template",
        "constant-app": "../components/constants/constant-app",
        "angular-base64": "../../bower_components/angular-base64/angular-base64.min",
        "col-resizable": "../../bower_components/col-resizable/colResizable-1.6",
        "angular-datepicker": "../../bower_components/angularjs-datepicker/dist/angular-datepicker.min",
        "bootstrap-daterangepicker": "../../bower_components/bootstrap-daterangepicker/daterangepicker",
        "bootstrap-timepicker": "../../bower_components/bootstrap-timepicker/js/bootstrap-timepicker",
        "daypilot": "../../bower_components/daypilot/daypilot-all.min",
        // Template module
        "HeaderModule": "../views/template/header/header",
        "FooterModule": "../views/template/footer/footer",
        "SidebarModule": "../views/template/sidebar/sidebar",
        // Core script to handle the entire theme and core functions
        "App": "../../assets/global/scripts/app",
        "Layout": "../../assets/layouts/layout/scripts/layout",// waiting minify
    },
    shim: {
        "jquery": { exports: '$' },
        "moment": { exports: 'moment' },
        "angular": { deps: ["jquery"], exports: 'angular' },
        "col-resizable": { deps: ["jquery"] },
        "jquery-ui": { deps: ["jquery"] },
        "jquery.scannerdetection": { deps: ["jquery"] },
        "jquery.pulsate": { deps: ["jquery", "jquery-ui"] },
        "angularAMD": { deps: ["angular"] },
        "angular-ui-router": { deps: ["angular"] },
        "angular-route": { deps: ["angular"] },
        "state-event": { deps: ["angular", "angular-ui-router"] },
        "angular-ui-select": { deps: ["angular"] },
        "angular-ui-select2": { deps: ["angular"] },
        "angular-loading-bar": { deps: ["angular"] },
        "daypilot": { deps: ["angular"] },
        "angular-base64": { deps: ["angular"] },
        "ui-bootstrap": { deps: ["angular"] },
        "oclazyload": { deps: ["jquery", "angular"] },
        "angular-sanitize": { deps: ["angular"] },
        "jquery-slimscroll": { deps: ["angular", "jquery"] },
        "angular-datepicker": { deps: ["angular"] },
        "bootstrap": { deps: ["jquery"] },
        "bootstrap-timepicker": { deps: ["jquery"] },
        "bootstrap-session-timeout": { deps: ["jquery", "bootstrap"] },
        "bootstrap-hover-dropdown": { deps: ["jquery"] },
        "App": { deps: ["jquery", "bootstrap", "jquery-slimscroll", "config-auth"] },
        "Layout": { deps: ["App"] },
        "angular-toastr": { deps: ["angular"] },
        "ngfileupload": ['angular'],
        "angular-moment": { deps: ["moment", "angular"] },
        "angular-daterangepicker": { deps: ["jquery", "moment", "angular", "angular-moment", "bootstrap-daterangepicker"] },
        "angular-formly": {
            exports: "formly",
            deps: ['api-check', 'angular']
        },
        "angular-formly-templates-bootstrap": {
            deps: ['angular-formly']
        },
        "signalR": { deps: ["jquery"] },
        "textAngular": { deps: ["angular", "angular-sanitize"] }
    },
    deps: ["app"]
});