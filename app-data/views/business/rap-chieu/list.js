﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('ListCinemaCtrl', ['$scope', '$timeout', '$location', '$filter', '$http', '$uibModal', '$q', 'UtilsService', 'ConstantsApp', 'CinemaApiService', 'toastr',
        function ($scope, $timeout, $location, $filter, $http, $uibModal, $q, UtilsService, ConstantsApp, CinemaApiService, $notifications) {

            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            var appVersion = app.Version;
            $scope.listPromise = [];

            $scope.LoaiRapChieu = {};

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/business/rap-chieu/create.html' + appVersion;


            /* ------------------------------------------------------------------------------- */

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'CreateCinemaCtrl',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'add' }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        $notifications.success('Thêm mới thành công', 'Thông báo');
                        initData();
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'CreateCinemaCtrl',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'edit' }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.Grid.PageNumber = 1;
                    } else if (response.Status === 1) {
                        $notifications.success('Cập nhật thành công');
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DETAIL
            $scope.Button.Detail = { GrantAccess: true };
            $scope.Button.Detail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'CreateCinemaCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'detail' }];;
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDelete = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDelete.push(item);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDelete.push($scope.Grid.Data[i]);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những rạp chiếu này ?";
                } else {
                    message = "Bạn có chắc muốn xóa rạp chiếu này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = CinemaApiService.DeleteMany(listItemDelete);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                listDeleteField.push({ Name: item.Name, Result: item.Result, Message: item.Message });
                            });
                            $scope.Grid.PageNumber = 1;
                            initData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [
                { Key: 'Order', Value: "T.Tự", Width: '1%', IsSortable: true },
                { Key: 'Code', Value: "Mã rạp", Width: '10%', IsSortable: false },
                { Key: 'Name', Value: "Tên rạp", Width: 'auto', IsSortable: false },
                { Key: 'CinemaType', Value: "Loại rạp", Width: '10%', IsSortable: false },
                { Key: 'Status', Value: "Trạng thái", Width: '10%', IsSortable: true },
                { Key: 'Actions', Value: "Thao tác", Width: '15%', IsSortable: false }
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            $scope.FilterLoaiRapChieu = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };

            $scope.DanhMuc = {};
            //lay danh muc loại rap chiếu
            var getListCinemaType = function () {
                var p = CinemaApiService.GetListCinemaType();
                $scope.listPromise.push(p);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        var responseResult = response.data;
                        $scope.DanhMuc.LoaiRapChieu = responseResult.Data;

                    } else $notifications.error("Lấy danh sách thất bại", "Lỗi");
                });
            };

            var initButtonByRightOfUser = function () {
                $q.all([UtilsService.Promise.Rights]).then(function () {
                    $scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("CENIMA-ADD");
                    $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("CENIMA-UPDATE");
                    $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("CENIMA-DELETE");
                    $scope.Button.Detail.GrantAccess = UtilsService.CheckRightOfUser("CENIMA-DETAIL");
                    $scope.Grid.GrantAccess = UtilsService.CheckRightOfUser("CENIMA-VIEW");
                });
                return true;
            };
            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                angular.forEach($scope.Grid.Data, function (items) {
                    items.selected = null;
                });
                $scope.Grid.Check = null;


                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch-----*/
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                // Get data from api
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;
                var cinemaTypeId = ''
                if ($scope.LoaiRapChieu.Selected != null) {
                    if ($scope.LoaiRapChieu.Selected.CatalogItemId != null) {
                        cinemaTypeId = $scope.LoaiRapChieu.Selected.CatalogItemId;
                    }
                }
                var p = CinemaApiService.GetListCinema($scope.Grid.PageNumber, $scope.Grid.PageSize, $scope.Filter.Text, cinemaTypeId);
                $scope.listPromise.push(p);
                bindingCategory();
                p.then(function onSuccess(response) {
                    console.log("SUCCESS", response);
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;

                    //Enable next button
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };
            //lấy item của mảng *category* với điều kiện là trường *field* có giá trị là *value*
            var getItemFromList = function (field, category, value) {
                for (var i = 0; i < category.length; i++) {
                    if (category[i][field] == value) {
                        return category[i];
                    }
                }
            }
            var bindingCategory = function () {
                $q.all($scope.listPromise).then(function () {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {
                        var item = $scope.Grid.Data[i];
                        item.CinemaType = getItemFromList('CatalogItemId', $scope.DanhMuc.LoaiRapChieu, item.CinemaTypeId);

                    }
                });
            }
            var initMain = function () {
                getListCinemaType();
                initData();
                bindingCategory();
                initButtonByRightOfUser();
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }]);
}();

