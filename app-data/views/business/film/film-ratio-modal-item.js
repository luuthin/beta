﻿define(function() {
    'use strict';
    var app = angular.module("SavisApp");
    //Controler for FilmRtaio
    app.controller("FilmRatioModalCtrl", ["$scope", "$location", "$q", "$uibModalInstance", "item", "option", "toastr", "FilmApiService", "ConstantsApp", "UtilsService",
        function($scope, $location, $q, $uibModalInstance, item, option, $notifications, FilmApiService, ConstantsApp, UtilsService) {

            var i = 0;
            var cinemaAll = { CinemaId: "00000000-0000-0000-0000-000000000000", Name: "Tất cả" };

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.Option = option;
            $scope.IsEdit = false;

            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function() {
                $uibModalInstance.dismiss('cancel');
            };

            

            // Button UPDATE
            $scope.Button.Save = { GrantAccess: true };
            $scope.Button.Save.Click = function(form) {
                if (form.$invalid) {
                    $notifications.warning("Kiểm tra các trường đã nhập", "Lỗi");
                    return;
                }
                var listCinemaId = [];
                for (i = 0; i < $scope.Film.ListCinema.length; i++) {
                    listCinemaId.push($scope.Film.ListCinema[i].CinemaId);
                }
                if (listCinemaId.length === 0 && !$scope.IsEdit) {
                    $notifications.warning("Rạp không được để trống", "Lỗi");
                    return;
                }
                var listRatio = $scope.Film.ListRatio;

                for (i = 0; i < listRatio.length; i++) {
                    if (listRatio[i].DistributorPercent < 0 || listRatio[i].DistributorPercent > 100) {
                        $notifications.warning("Tỉ lệ phim phải trong khoảng từ 0 đến 100", "Lỗi");
                        return;
                    }
                }

                if (!$scope.IsEdit) {
                    var p = FilmApiService.CreateFilmRatio(item.FilmId, listCinemaId, listRatio);
                    p.then(function onSuccess(response) {
                        var responseResult = response.data;
                        if (responseResult.Status === 1) {
                            $notifications.success("Thêm mới tỉ lệ phim thành công", "Thông báo");
                            var dataResult = responseResult.Data;
                            init();
                        } else {
                            $notifications.error("Có lỗi xảy ra", "Lỗi");
                            return;
                        };
                    });
                } else {
                    var p1 = FilmApiService.UpdateFilmRatio(item.FilmId, listRatio);
                    p1.then(function onSuccess(response) {
                        var responseResult = response.data;
                        if (responseResult.Status === 1) {
                            var dataResult = responseResult.Data;
                            $notifications.success("Cập nhật tỉ lệ phim thành công", "Thông báo");
                            init();
                        } else {
                            $notifications.error("Có lỗi xảy ra", "Lỗi");
                            return;
                        };
                    });
                }
            };

            $scope.Button.ReturnAdd = function() {
                $scope.IsEdit = false;
                initNewListRatio();
            }

            $scope.Button.CinemaRatio = {};
            $scope.Button.CinemaRatio.Update = {};
            $scope.Button.CinemaRatio.Update.Click = function(cinema) {
                $scope.Film.ListCinema = [];
                $scope.Film.Cinema = cinema;
                initListRatio();
                $scope.IsEdit = true;
            };

            $scope.Button.CinemaRatio.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn" };
            $scope.Button.CinemaRatio.Delete.Click = function(cinema) {
                var listItemDeleteId = [];
                var count = 0;
                if (cinema !== null && typeof cinema !== 'undefined') {
                    listItemDeleteId.push(cinema.CinemaId);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {
                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].CinemaId);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa tỉ lệ phim của những rạp này?";
                } else {
                    message = "Bạn có chắc muốn xóa tỉ lệ phim của rạp này?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function(modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];
                        var promise = FilmApiService.DeleteFilmRatio(item.FilmId, listItemDeleteId);

                        promise.then(function(response) {
                            angular.forEach(response.data.Data, function(itemDeleted) {
                                if (itemDeleted.Result !== -1) {
                                    listDeleteField.push({
                                        Name: itemDeleted.Name,
                                        Result: itemDeleted.Result === 1 ? true : false,
                                        Message: itemDeleted.Result === 1 ? ('Xóa tỉ lệ phim thành công') : ('Xóa tỉ lệ phim thất bại')
                                    });
                                }
                                else if (itemDeleted.Result === -1) {
                                    $scope.error('Lỗi hệ thống !');
                                    $notifications.error("Lỗi hệ thống", "Lỗi");
                                }
                            });
                            $scope.Grid.PageNumber = 1;
                            init();
                            $scope.Button.CinemaRatio.Delete.Visible = false;
                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            $scope.Button.DateTime = {};
            $scope.Formats = ['yyyy-MM-dd', 'dd-MM-yyyy', 'dd/MM/yyyy', 'shortDate'];
            $scope.Button.DateTime.Format = $scope.Formats[2];

            $scope.Button.DateTime.OpenToRatioDatePicker = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsToRatioDatePickerOpened = true;
            };

            $scope.Button.DateTime.OpenWeek1ToRatioDatePicker = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsToWeek1RatioDatePickerOpened = true;
            };

            $scope.Button.DateTime.OpenFromRatioDatePicker = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsFromRatioDatePickerOpened = true;
            };

            function addDays(date, days) {
                var result = new Date(date);
                result.setDate(result.getDate() + days);
                return result;
            }

            $scope.DatePicker = {};
            // Disable weekend selection
            //$scope.DatePicker.DayDisabled = function(date, mode) {
            //    return (mode === 'day' && (date.getDay() !== 4));
            //};

            $scope.OnToDateWeek1Change = function() {
                var date = $scope.Film.ListRatio[0].ToDate;
                if (date === null || date === '' || date === undefined) {
                    return;
                }
                $scope.Film.ListRatio[0].ToDate = addDays(date, 0);
                $scope.Film.ListRatio[1].FromDate = addDays(date, 1);
                $scope.Film.ListRatio[1].ToDate = addDays(date, 7);
                $scope.Film.ListRatio[2].FromDate = addDays(date, 8);
                $scope.Film.ListRatio[2].ToDate = addDays(date, 14);
                $scope.Film.ListRatio[3].FromDate = addDays(date, 15);
                $scope.Film.ListRatio[3].ToDate = addDays(date, 21);
                $scope.Film.ListRatio[4].FromDate = addDays(date, 22);
                $scope.Film.ListRatio[4].ToDate = addDays(date, 28);
                $scope.Film.ListRatio[5].FromDate = addDays(date, 29);
                $scope.Film.ListRatio[5].ToDate = addDays(date, 35);
                $scope.Film.ListRatio[6].FromDate = addDays(date, 36);
                $scope.Film.ListRatio[6].ToDate = addDays(date, 42);

                $scope.Button.DateTime.IsToWeek1RatioDatePickerOpened = false;

                //if ($scope.Film.ListRatio[6].FromDate > $scope.Film.ListRatio[6].ToDate) {
                //    $scope.Film.ListRatio[6].ToDate = addDays($scope.Film.ListRatio[6].FromDate, 6);
                //}
            }

            $scope.DropdownList = {
                ListCinemaNoRatio: [],
            };

            $scope.OnCinemasChange = function (item, model) {
                if (item === cinemaAll) {
                    $scope.Film.ListCinema = [];
                    for (i = 1; i < $scope.DropdownList.ListCinemaNoRatio.length; i++) {
                        $scope.Film.ListCinema.push($scope.DropdownList.ListCinemaNoRatio[i]);
                    }
                }
                initListItem();
            }

            //Grid Modal
            $scope.GridRatio = angular.copy(ConstantsApp.GRID_ATTRIBUTES);

            $scope.GridRatio.DataHeader = [
                { Key: 'Week', Value: "Tuần", Width: '5%' },
                { Key: 'FromDate', Value: "Từ ngày", Width: '20%' },
                { Key: 'ToDate', Value: "Đến ngày", Width: '20%' },
                { Key: 'Beta', Value: "Tỉ lệ NPH", Width: '17%' },
                { Key: 'Distributor', Value: "Tỉ lệ Beta", Width: '17%' },
                { Key: 'Money', Value: "Số tiền tối thiểu (VNĐ)", Width: '21%' },
            ];

            $scope.Grid = angular.copy(ConstantsApp.GRID_ATTRIBUTES);
            $scope.Grid.PageNumber = 1;
            $scope.Grid.PageSize = 10;
            $scope.Grid.DataHeader = [
                { Key: 'STT', Value: "STT", Width: '5%' },
                { Key: 'Name', Value: "Tên rạp", Width: 'auto' },
                { Key: 'Action', Value: "Thao tác", Width: '30%' },
            ];

            $scope.Grid.Filter = {};
            $scope.Grid.Filter.CinemaName = '';

            $scope.Grid.Search = function() {
                initListCinemaRatio();
            };

            $scope.Grid.Refresh = function() {
                $scope.Grid.Filter.CinemaName = '';
                //$scope.Grid.PageNumber = 1;
                //$scope.Grid.PageSize = 10;
                initListCinemaRatio();
            };

            $scope.Grid.Data = [];
            $scope.Grid.Order = function(predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function() {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function(item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.CinemaRatio.Delete.Visible = false;
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.CinemaRatio.Delete.Visible = false;
                    $scope.Button.CinemaRatio.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function(item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function(item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function(item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function(items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function(item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function(item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.CinemaRatio.Delete.Visible = false;
                $scope.Button.CinemaRatio.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.CinemaRatio.Delete.Visible = false;
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.CinemaRatio.Delete.Visible = false;
                    $scope.Button.CinemaRatio.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function(pageTogo) {
                $scope.Grid.PageNumber = pageTogo;
                initListCinemaRatio();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function() {
                initListCinemaRatio();
            };

            $scope.Film = {
                Name: item.Name,
                FilmFormatName: item.FilmFormatName,
                OpeningDate: item.OpeningDate,
                CreatedOnDate: item.CreatedOnDate,
                ListCinemaRatio: [],
                ListRatio: [],
                ListCinema: [],
                Cinema: []
            };
            $scope.Search = {
                CinemaName: '',
            }
            $scope.Film.ListRatio = [];
            $scope.Film.ListRatio = [
                { Week: 1, FromDate: addDays($scope.Film.CreatedOnDate, 0), ToDate: addDays($scope.Film.OpeningDate, 6), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 2, FromDate: addDays($scope.Film.OpeningDate, 7), ToDate: addDays($scope.Film.OpeningDate, 13), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 3, FromDate: addDays($scope.Film.OpeningDate, 14), ToDate: addDays($scope.Film.OpeningDate, 20), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 4, FromDate: addDays($scope.Film.OpeningDate, 21), ToDate: addDays($scope.Film.OpeningDate, 27), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 5, FromDate: addDays($scope.Film.OpeningDate, 28), ToDate: addDays($scope.Film.OpeningDate, 34), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 6, FromDate: addDays($scope.Film.OpeningDate, 35), ToDate: addDays($scope.Film.OpeningDate, 41), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                { Week: 7, FromDate: addDays($scope.Film.OpeningDate, 42), ToDate: addDays($scope.Film.OpeningDate, 48), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
            ];

            $scope.InputMoneyFormatChange = function(index) {
                var numberFormat = $scope.Film.ListRatio[index].MoneyFormat;
                var str = new String(numberFormat);
                str = str.replace(/\./g, "");
                var number = Math.round(str);
                if (Number.isNaN(number)) {
                    number = 0;
                }
                $scope.Film.ListRatio[index].MoneyFormat = number.toLocaleString("vi-VN");
                $scope.Film.ListRatio[index].Money = number;
            }

            $scope.InputDistributorPercentChange = function(index) {
                var numberFormat = $scope.Film.ListRatio[index].DistributorPercent;
                var str = new String(numberFormat);
                str = str.replace(/\./g, "");
                var number = Math.round(str);
                if (Number.isNaN(number)) {
                    number = 0;
                    $scope.Film.ListRatio[index].DistributorPercent = number;
                }
                $scope.Film.ListRatio[index].BetaPercent = 100 - number;
            }

            $scope.IsInfo = false;

            var initButtonByRightOfUser = function () {
                $q.all([UtilsService.Promise.Rights]).then(function () {
                    $scope.Button.Save.GrantAccess = UtilsService.CheckRightOfUser("FILM-RATIO-UPDATE");
                });
                return true;
            };

            var initListRatio = function() {
                $scope.Film.ListRatio = [];
                var p = FilmApiService.GetListRatioByFilmAndCinema(item.FilmId, $scope.Film.Cinema.CinemaId);
                p.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                        var dataResult = responseResult.Data;
                        if (dataResult.length > 0) {
                            for (i = 0; i < dataResult.length; i++) {
                                $scope.Film.ListRatio.push(
                                    {
                                        FilmRatioId: dataResult[i].FilmRatioId,
                                        FilmId: dataResult[i].FilmId,
                                        CinemaId: dataResult[i].CinemaId,
                                        Week: dataResult[i].Week,
                                        FromDate: new Date(dataResult[i].FromDate),
                                        ToDate: new Date(dataResult[i].ToDate),
                                        BetaPercent: dataResult[i].BetaPercent,
                                        DistributorPercent: dataResult[i].DistributorPercent,
                                        Money: dataResult[i].Money,
                                        MoneyFormat: dataResult[i].Money.toLocaleString("vi-VN"),
                                    }
                                );
                            }
                        } else {
                            $scope.Film.ListRatio = [
                                { Week: 1, FromDate: addDays($scope.Film.CreatedOnDate, 0), ToDate: addDays($scope.Film.OpeningDate, 6), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 2, FromDate: addDays($scope.Film.OpeningDate, 7), ToDate: addDays($scope.Film.OpeningDate, 13), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 3, FromDate: addDays($scope.Film.OpeningDate, 14), ToDate: addDays($scope.Film.OpeningDate, 20), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 4, FromDate: addDays($scope.Film.OpeningDate, 21), ToDate: addDays($scope.Film.OpeningDate, 27), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 5, FromDate: addDays($scope.Film.OpeningDate, 28), ToDate: addDays($scope.Film.OpeningDate, 34), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 6, FromDate: addDays($scope.Film.OpeningDate, 35), ToDate: addDays($scope.Film.OpeningDate, 41), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                                { Week: 7, FromDate: addDays($scope.Film.OpeningDate, 42), ToDate: addDays($scope.Film.OpeningDate, 48), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                            ];
                        }

                    } else {
                        $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                        return;
                    };
                });
                return p;
            }

            var initListCinemaRatio = function() {
                $scope.Film.ListCinemaRatio = [];

                $scope.Grid.Data = [];

                angular.forEach($scope.Grid.Data, function(items) {
                    items.selected = null;
                });

                if ($scope.Grid.PageNumber === "" || $scope.Grid.PageNumber === null || $scope.Grid.PageNumber === undefined) {
                    $scope.Grid.PageNumber = 1;
                }
                if ($scope.Grid.PageSize === "" || $scope.Grid.PageSize === null || $scope.Grid.PageSize === undefined) {
                    $scope.Grid.PageSize = 10;
                }
                if ($scope.Grid.Filter.CinemaName === null || $scope.Grid.Filter.CinemaName === undefined) {
                    $scope.Grid.Filter.CinemaName = "";
                }

                var p = FilmApiService.GetListCinemaRatio(item.FilmId, $scope.Grid.PageNumber, $scope.Grid.PageSize, $scope.Grid.Filter.CinemaName);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                        var responseResult = dataResult.Data;

                        $scope.Film.ListCinemaRatio = responseResult;

                        $scope.Grid.Data = dataResult.Data;

                        //Enable next button
                        $scope.Grid.TotalCount = dataResult.TotalCount;
                        $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                        $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                        $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    } else {
                        $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                        return;
                    };
                });
                return p;
            }

            var initListCinemaNoRatio = function () {
                $scope.DropdownList.ListCinemaNoRatio = [];
                var p = FilmApiService.GetListCinemaNoRatio(item.FilmId);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                        var responseResult = dataResult.Data;
                        //$scope.DropdownList.ListCinemaNoRatio = responseResult;

                        if (responseResult.length > 0) {
                            $scope.DropdownList.ListCinemaNoRatio.push(cinemaAll);
                            for (i = 0; i < responseResult.length; i++) {
                                $scope.DropdownList.ListCinemaNoRatio.push(responseResult[i]);
                            }
                        }

                    } else {
                        $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                        return;
                    };
                });
                return p;
            };

            var initNewListRatio = function () {
                $scope.Film.ListRatio = [
                    { Week: 1, FromDate: addDays($scope.Film.CreatedOnDate, 0), ToDate: addDays($scope.Film.OpeningDate, 6), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 2, FromDate: addDays($scope.Film.OpeningDate, 7), ToDate: addDays($scope.Film.OpeningDate, 13), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 3, FromDate: addDays($scope.Film.OpeningDate, 14), ToDate: addDays($scope.Film.OpeningDate, 20), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 4, FromDate: addDays($scope.Film.OpeningDate, 21), ToDate: addDays($scope.Film.OpeningDate, 27), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 5, FromDate: addDays($scope.Film.OpeningDate, 28), ToDate: addDays($scope.Film.OpeningDate, 34), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 6, FromDate: addDays($scope.Film.OpeningDate, 35), ToDate: addDays($scope.Film.OpeningDate, 41), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                    { Week: 7, FromDate: addDays($scope.Film.OpeningDate, 42), ToDate: addDays($scope.Film.OpeningDate, 48), BetaPercent: 0, DistributorPercent: 0, Money: 0, MoneyFormat: 0 },
                ];

                var date = $scope.Film.ListRatio[0].ToDate;
                if (date === null || date === '' || date === undefined) {
                    return;
                }

                while (date.getDay() !== 4) {
                    date = addDays(date, 1);
                }

                $scope.Film.ListRatio[0].ToDate = addDays(date, 0);
                $scope.Film.ListRatio[1].FromDate = addDays(date, 1);
                $scope.Film.ListRatio[1].ToDate = addDays(date, 7);
                $scope.Film.ListRatio[2].FromDate = addDays(date, 8);
                $scope.Film.ListRatio[2].ToDate = addDays(date, 14);
                $scope.Film.ListRatio[3].FromDate = addDays(date, 15);
                $scope.Film.ListRatio[3].ToDate = addDays(date, 21);
                $scope.Film.ListRatio[4].FromDate = addDays(date, 22);
                $scope.Film.ListRatio[4].ToDate = addDays(date, 28);
                $scope.Film.ListRatio[5].FromDate = addDays(date, 29);
                $scope.Film.ListRatio[5].ToDate = addDays(date, 35);
                $scope.Film.ListRatio[6].FromDate = addDays(date, 36);
                $scope.Film.ListRatio[6].ToDate = addDays(date, 42);
            };

            var initData = function() {
                $scope.Form.Title = $scope.Form.Option.Title;
                $scope.TitleForm = $scope.Form.Option.TitleInfo;
                $scope.Messenger = $scope.Form.Option.Messager;
                $scope.Confirm = $scope.Form.Option.ButtonCancel;

                $scope.IsEdit = false;
                $scope.Film.ListCinema = [];
                $scope.Film.Cinema = '';
                initNewListRatio();
            };

            var init = function () {
                initButtonByRightOfUser();
                initListCinemaRatio();
                initListCinemaNoRatio();
                initData();
            };

            init();
        }]);
}());

