﻿define(function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('CreateFilmController', ['$scope', '$state', '$stateParams', '$timeout', 'Upload', '$location', '$filter', 'ConstantsApp', '$http', '$uibModal', '$q', 'UtilsService', 'FilmApiService', 'GenreApiService', 'FilmFormatApiService', 'DistributorApiService', 'FilmRestrictAgeApiService', 'SubtitleApiService', 'toastr', 'StudioApiService', 'FilmDubbingApiService',
        function ($scope, $state, $stateParams, $timeout, Upload, $location, $filter, ConstantsApp, $http, $uibModal, $q, UtilsService, FilmApiService, GenreApiService, FilmFormatApiService, DistributorApiService, FilmRestrictAgeApiService, SubtitleApiService, $notifications, StudioApiService, FilmDubbingApiService) {
            //$ocLazyLoad.load('testModule.js');
            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};
            var i = 0, j = 0;
            var version = app.Version;

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/business/film/film-modal-rental.html' + version;

            // Lấy id người dùng hiện tại
            var currentUserId = "f02d992f-c1c3-4d04-8a13-17f82a50ea81";

            $scope.Film = {
                ListPosterUrl: [],
                ListFilmFormat: [],
                ListGenre: [],
                OpeningDate: new Date()
            };
            $scope.Date = {};
            $scope.Button = {};

            $scope.ListGenre = [];
            $scope.ListFilmFormat = [];
            $scope.ListDistributor = [];
            $scope.ListFilmRestrictAge = [];
            $scope.ListSubtitle = [];
            $scope.ListStudio = [];
            $scope.ListFilmDubbing = [];

            $scope.IsUpdate = false;
            $scope.IsCopy = false;

            $scope.SaveAndClose = true;

            $scope.Date.Today = new Date();
            $scope.Button.DateTime = {};
            $scope.formats = ['yyyy-MM-dd', 'dd-MM-yyyy', 'dd/MM/yyyy', 'shortDate'];
            $scope.Button.DateTime.format = $scope.formats[2];
            $scope.format1 = $scope.formats[1];

            $scope.Button.DateTime.OpenDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.Button.DateTime.IsDatePickerOpened = true;
            };

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */

            /* ------------------------------------------------------------------------------- */

            $scope.ListImage = [];

            $scope.Upload = {};
            $scope.UploadFiles = function (item, files, errFiles, typeUpload) {
                $scope.Upload.Files = files;
                $scope.Upload.ErrFiles = errFiles;
                angular.forEach(files, function (fileitem) {
                    fileitem.IsUploadDone = false;
                    fileitem.upload = Upload.upload({
                        url: ConstantsApp.BASED_UPLOADFILE_URL + ConstantsApp.API_UPLOAD_URL + "?token=",//can dinh nghia token la gi
                        data: {
                            file: fileitem,
                            token: "asd" //can dinh nghia token la gi
                        }
                    });

                    fileitem.upload.then(function (response) {
                        $timeout(function () {
                            if (response.data.data === null) {
                                fileitem.IsUploadDone = true;
                            } else {
                                var lenghtPath = response.data.Data[0].AbsolutePath.split("/").length - 1;
                                if (lenghtPath >= 0) {
                                    $scope.NameFile = response.data.Data[0].AbsolutePath.split("/")[lenghtPath];
                                }
                                $scope.Film.ListPosterUrl.push({
                                    AbsolutePath: response.data.Data[0].AbsolutePath,
                                    Link: ConstantsApp.BASED_FILE_URL + "/" + response.data.Data[0].AbsolutePath,
                                });
                                if ($scope.Film.ListPosterUrl.length === 0 || $scope.Film.MainPosterURL === "" || $scope.Film.MainPosterURL === null || $scope.Film.MainPosterURL === undefined) {
                                    $scope.Film.MainPosterURL = response.data.Data[0].AbsolutePath;
                                }
                            }
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            fileitem.IsUploadDone = true;
                            $scope.Upload.ErrFiles.push(fileitem);
                        }
                    }, function (evt) {
                        fileitem.progress = parseInt(100.0 * evt.loaded / evt.total);
                    });
                });
            };

            $scope.RemoveImage = function (item) {
                var message = "Bạn có chắc muốn xóa ảnh này?";
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        for (var t = 0; t < $scope.Film.ListPosterUrl.length; t++)
                            if ($scope.Film.ListPosterUrl[t].AbsolutePath === item.AbsolutePath) {
                                $scope.Film.ListPosterUrl.splice(t, 1);
                                break;
                            }
                    }
                });
            };

            //Grid Modal
            $scope.GridModal = angular.copy(ConstantsApp.GRID_ATTRIBUTES);

            $scope.GridModal.DataHeader = [
                { Key: 'STT', Value: "STT", Width: '5%' },
                { Key: 'Week', Value: "Tuần", Width: '5%' },
                { Key: 'FromDate', Value: "Từ ngày", Width: '25%' },
                { Key: 'ToDate', Value: "Đến ngày", Width: '25%' },
                { Key: 'Beta', Value: "Tỉ lệ Beta", Width: '20%' },
                { Key: 'Distributor', Value: "Tỉ lệ NPH", Width: '20%' },
            ];

            function addDays(date, days) {
                var result = new Date(date);
                result.setDate(result.getDate() + days);
                return result;
            }

            $scope.OpeningDateChange = function () {
                
            }

            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */

            // Button SAVE
            $scope.Button.Save = {};
            $scope.Button.Save.Click = function (form) {
                if (form.$invalid) {
                    $notifications.warning("Kiểm tra các trường đã nhập", "Lỗi");
                    return;
                }
                var postData = {};
                var i = 0;

                // #region Validate data
                //Tên
                postData.Name = $scope.Film.Name;
                if (postData.Name === "" || postData.Name === undefined || postData.Name === null) {
                    $notifications.warning('Tên phim (VI) không được để trống', 'Cảnh báo');
                    return;
                }
                //Tên tiếng anh
                postData.Name_F = $scope.Film.Name_F;
                if (postData.Name_F === "" || postData.Name_F === undefined || postData.Name_F === null) {
                    $notifications.warning('Tên phim (EN) không được để trống', 'Cảnh báo');
                    return;
                }
                //Mã
                postData.Code = $scope.Film.Code;
                if (postData.Code === "" || postData.Code === undefined || postData.Code === null) {
                    $notifications.warning('Mã phim không được để trống', 'Cảnh báo');
                    return;
                }
                //Thể loại
                postData.ListGenreId = [];
                for (i = 0; i < $scope.Film.ListGenre.length; i++) {
                    postData.ListGenreId.push($scope.Film.ListGenre[i].CatalogItemId);
                }
                if (postData.ListGenreId.length === 0) {
                    $notifications.warning('Thể loại không được để trống', 'Cảnh báo');
                    return;
                }
                //Độ tuổi
                if ($scope.Film.FilmRestrictAge !== null && $scope.Film.FilmRestrictAge !== undefined && $scope.Film.FilmRestrictAge !== "") {
                    postData.FilmRestrictAgeId = $scope.Film.FilmRestrictAge.CatalogItemId;
                }
                //if (postData.FilmRestrictAgeId === "" || postData.FilmRestrictAgeId === undefined || postData.FilmRestrictAgeId === null) {
                //    $notifications.warning('Phân loại độ tuổi không được để trống', 'Cảnh báo');
                //    return;
                //}
                //Định dạng phim
                //postData.ListFilmFormatId = [];
                //for (i = 0; i < $scope.Film.ListFilmFormat.length; i++) {
                //    postData.ListFilmFormatId.push($scope.Film.ListFilmFormat[i].CatalogItemId);
                //}
                //if (postData.ListFilmFormatId.length === 0) {
                //    $notifications.warning('Định dạng phim không được để trống', 'Cảnh báo');
                //    return;
                //}

                if ($scope.Film.FilmFormat !== null && $scope.Film.FilmFormat !== undefined && $scope.Film.FilmFormat !== "") {
                    postData.FilmFormatId = $scope.Film.FilmFormat.CatalogItemId;
                }
                if (postData.FilmFormatId === "" || postData.FilmFormatId === undefined || postData.FilmFormatId === null) {
                    $notifications.warning('Định dạng phim không được để trống', 'Cảnh báo');
                    return;
                }

                if ($scope.Film.FilmDubbing !== null && $scope.Film.FilmDubbing !== undefined && $scope.Film.FilmDubbing !== "") {
                    postData.FilmDubbingId = $scope.Film.FilmDubbing.CatalogItemId;
                }
                if (postData.FilmDubbingId === "" || postData.FilmDubbingId === undefined || postData.FilmDubbingId === null) {
                    $notifications.warning('Hình thức phim không được để trống', 'Cảnh báo');
                    return;
                }

                //Nhà phát hành
                if ($scope.Film.Distributor !== null && $scope.Film.Distributor !== undefined && $scope.Film.Distributor !== "") {
                    postData.DistributorId = $scope.Film.Distributor.CatalogItemId;
                }
                if (postData.DistributorId === "" || postData.DistributorId === undefined || postData.DistributorId === null) {
                    $notifications.warning('Nhà phát hành không được để trống', 'Cảnh báo');
                    return;
                }

                //Studio
                //if ($scope.Film.Studio !== null && $scope.Film.Studio !== undefined && $scope.Film.Studio !== "") {
                //    postData.StudioId = $scope.Film.Studio.CatalogItemId;
                //}
                //if (postData.StudioId === "" || postData.StudioId === undefined || postData.StudioId === null) {
                //    $notifications.warning('Studio không được để trống', 'Cảnh báo');
                //    return;
                //}
                if ($scope.Film.StudioName !== null && $scope.Film.StudioName !== undefined && $scope.Film.StudioName !== "") {
                    postData.StudioName = $scope.Film.StudioName;
                }
                if (postData.StudioName === "" || postData.StudioName === undefined || postData.StudioName === null) {
                    $notifications.warning('Studio không được để trống', 'Cảnh báo');
                    return;
                }
                //Thời lượng
                postData.Duration = $scope.Film.Duration;
                if (postData.Duration === "" || postData.Duration === undefined || postData.Duration === null) {
                    $notifications.warning('Thời lượng không được để trống', 'Cảnh báo');
                    return;
                }
                //Phụ đề
                if ($scope.Film.Subtitle !== null && $scope.Film.Subtitle !== undefined && $scope.Film.Subtitle !== "") {
                    postData.SubtitleId = $scope.Film.Subtitle.CatalogItemId;
                }
                //if (postData.SubtitleId === "" || postData.SubtitleId === undefined || postData.SubtitleId === null) {
                //    $notifications.warning('Phụ đề không được để trống', 'Cảnh báo');
                //    return;
                //}
                //Nội dung tóm tắt Vi
                postData.ShortDescription = $scope.Film.ShortDescription;
                if (postData.ShortDescription === "" || postData.ShortDescription === undefined || postData.ShortDescription === null) {
                    $notifications.warning('Nội dung tóm tắt (VI) không được để trống', 'Cảnh báo');
                    return;
                }
                //Nội dung tóm tắt En
                postData.ShortDescription_F = $scope.Film.ShortDescription_F;
                if (postData.ShortDescription_F === "" || postData.ShortDescription_F === undefined || postData.ShortDescription_F === null) {
                    $notifications.warning('Nội dung tóm tắt (EN) không được để trống', 'Cảnh báo');
                    return;
                }
                //TrailerURL
                postData.TrailerURL = $scope.Film.TrailerURL;
                if (postData.TrailerURL === "" || postData.TrailerURL === undefined || postData.TrailerURL === null) {
                    $notifications.warning('Trailer Url không được để trống', 'Cảnh báo');
                    return;
                }
                var regurl = new RegExp("^http(s?)\:\/\/");
                if (!regurl.test(postData.TrailerURL) && postData.TrailerURL !== null && postData.TrailerURL !== "" && postData.TrailerURL !== undefined) {
                    $notifications.warning('Trailer Url phải bắt đầu bằng http\:\/\/ hoặc https\:\/\/', 'Cảnh báo');
                    return;
                }
                //Poster
                postData.ListPosterUrl = [];
                for (i = 0; i < $scope.Film.ListPosterUrl.length; i++) {
                    postData.ListPosterUrl.push(
                        {
                            AbsolutePath: $scope.Film.ListPosterUrl[i].AbsolutePath,
                            MainPoster: $scope.Film.MainPosterURL === $scope.Film.ListPosterUrl[i].AbsolutePath ? true : false
                        }
                    );
                }
                if (postData.ListPosterUrl.length === 0) {
                    $notifications.warning('Hình ảnh phim không được để trống', 'Cảnh báo');
                    return;
                }
                var checkMainPoster = false;
                for (i = 0; i < postData.ListPosterUrl.length; i++) {
                    if (postData.ListPosterUrl[i].MainPoster === true) {
                        checkMainPoster = true;
                    }
                }
                if (!checkMainPoster) {
                    $notifications.warning('Bạn phải chọn hình ảnh đại diện', 'Cảnh báo');
                    return;
                }
                postData.ListGenreId = [];
                for (i = 0; i < $scope.Film.ListGenre.length; i++) {
                    postData.ListGenreId.push($scope.Film.ListGenre[i].CatalogItemId);
                }
                postData.Description = $scope.Film.Description;
                postData.Description_F = $scope.Film.Description_F;


                postData.OpeningDate = $scope.Film.OpeningDate;
                postData.Rate = $scope.Film.Rate;

                var rateReg = /^\d(\.?\d?)(\d?)\/\d(\d?)$/;
                if (!rateReg.test(postData.Rate)) {
                    $notifications.warning('Định dạng xếp hạng phải đúng định dạng (8.5/10 hoặc 8/10)', 'Cảnh báo');
                    return;
                }

                if ($scope.Film.FilmRestrictAge !== null && $scope.Film.FilmRestrictAge !== undefined && $scope.Film.FilmRestrictAge !== "") {
                    postData.FilmRestrictAgeId = $scope.Film.FilmRestrictAge.CatalogItemId;
                }
                postData.Order = $scope.Film.Order;
                postData.Status = $scope.Film.Status;
                //postData.CensorStatus = $scope.Film.CensorStatus;
                postData.Actors = $scope.Film.Actors;
                postData.MainLanguage = $scope.Film.MainLanguage;

                postData.Director = $scope.Film.Director;

                //postData.SpecialShow = $scope.Film.SpecialShow;
                postData.Hot = $scope.Film.Hot;
                postData.NowShowing = $scope.Film.NowShowing;
                postData.GroupId = $scope.Film.GroupId;

                //Tỉ lệ phim

                // #endregion

                if ($scope.IsUpdate) {
                    FilmApiService.Update($scope.Film.FilmId, postData)
                        .then(function onSuccess(response) {
                            var dataResult = response.data;
                            if (dataResult.Status === 1) {
                                $notifications.success("Cập nhật thông tin phim thành công", "Thông báo");
                                $location.path('/film');
                            } else if (dataResult.Status === 2) {
                                $notifications.warning('Mã phim đã tồn tại', 'Cảnh báo');
                            } else $notifications.error("Cập nhật thông tin phim thất bại", "Lỗi");
                        });
                } else {
                    FilmApiService.AddNew(postData)
                        .then(function onSuccess(response) {
                            var dataResult = response.data;
                            if (dataResult.Status === 1) {
                                $notifications.success("Thêm mới phim thành công", "Thông báo");
                                if ($scope.SaveAndClose) {
                                    $location.path('/film');
                                } else {
                                    $scope.Film = {
                                        ListPosterUrl: [],
                                        ListFilmFormat: [],
                                        ListGenre: [],
                                        Status: true
                                    };
                                }
                            } else if (dataResult.Status === 2) {
                                $notifications.warning('Mã phim đã tồn tại', 'Cảnh báo');
                            } else if (dataResult.Status === 0) {
                                $notifications.warning('Phim với định dạng và hình thức này đã tồn tại!', 'Cảnh báo');
                            } else $notifications.error("Thêm mới thất bại", "Lỗi");
                        });
                }
            };

            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                var message = "Những thông tin thay đổi chưa được lưu.\nBạn có muốn thoát khỏi trang này?";

                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        $location.path('/film');
                    }
                });
            };

            $scope.Button.SaveOnly = {};
            $scope.Button.SaveOnly.Click = function () {
                $scope.SaveAndClose = false;
                return true;
            };

            $scope.Button.SaveAndClose = {};
            $scope.Button.SaveAndClose.Click = function () {
                $scope.SaveAndClose = true;
                return true;
            };

            /* SEARCH FUNCTIONS */
            /* ------------------------------------------------------------------------------- */


            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            //Lấy mã phim mới
            var initNewCode = function () {
                return FilmApiService.GenerateNewCode()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.Film.Code = responseResult.Data;

                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            }

            //khởi tạo danh sách thể loại
            var initGenre = function () {
                return GenreApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListGenre = responseResult.Data;

                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };
            //Khởi tạo danh sách định dạng film
            var initFilmFormat = function () {
                return FilmFormatApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListFilmFormat = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };
            //Khởi tạo danh sách nhà phát hành
            var initDistributor = function () {
                return DistributorApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListDistributor = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };
            //Khởi tạo danh sách lứa tuổi
            var initFilmRestrictAge = function () {
                return FilmRestrictAgeApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListFilmRestrictAge = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };
            //Khởi tạo danh sách phụ đề
            var initSubtitle = function () {
                return SubtitleApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListSubtitle = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };
            ////Khởi tạo danh sách studio
            //var initStudio = function () {
            //    return StudioApiService.GetAll()
            //        .then(function onSuccess(response) {
            //            var dataResult = response.data;
            //            if (dataResult.Status === 1) {
            //                //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
            //                var responseResult = response.data;
            //                $scope.ListStudio = responseResult.Data;
            //            } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
            //        });
            //};
            //Khởi tạo danh sách hình thức phim
            var initDubbing = function () {
                return FilmDubbingApiService.GetAll()
                    .then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                            var responseResult = response.data;
                            $scope.ListFilmDubbing = responseResult.Data;
                        } else $notifications.error("Có lỗi xảy ra khi lấy dữ liệu", "Lỗi");
                    });
            };

            //Lấy thông tin film theo Id
            var initFilmData = function () {
                var p = FilmApiService.GetById($stateParams.filmId);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                        //console.log(response.data);
                        var film = response.data.Data;
                        var i, j = 0;
                        // #region Convert data
                        //Tên
                        $scope.Film.Name = film.Name;
                        //Tên tiếng anh
                        $scope.Film.Name_F = film.Name_F;
                        //Mã
                        if ($state.current.data.cmd === 'update') {
                            $scope.Film.Code = film.Code;
                        }
                        //Thể loại
                        for (i = 0; i < film.ListGenreId.length; i++) {
                            for (j = 0; j < $scope.ListGenre.length; j++) {
                                if (film.ListGenreId[i] === $scope.ListGenre[j].CatalogItemId) {
                                    $scope.Film.ListGenre.push($scope.ListGenre[j]);
                                }
                            }
                        }
                        //Độ tuổi
                        for (i = 0; i < $scope.ListFilmRestrictAge.length; i++) {
                            if (film.FilmRestrictAgeId === $scope.ListFilmRestrictAge[i].CatalogItemId) {
                                $scope.Film.FilmRestrictAge = $scope.ListFilmRestrictAge[i];
                            }
                        }
                        //Định dạng phim
                        for (i = 0; i < $scope.ListFilmFormat.length; i++) {
                            if (film.FilmFormatId === $scope.ListFilmFormat[i].CatalogItemId) {
                                $scope.Film.FilmFormat = $scope.ListFilmFormat[i];
                            }
                        }
                        //Định dạng phim
                        for (i = 0; i < $scope.ListFilmDubbing.length; i++) {
                            if (film.FilmDubbingId === $scope.ListFilmDubbing[i].CatalogItemId) {
                                $scope.Film.FilmDubbing = $scope.ListFilmDubbing[i];
                            }
                        }

                        //Nhà phát hành
                        for (i = 0; i < $scope.ListDistributor.length; i++) {
                            if (film.DistributorId === $scope.ListDistributor[i].CatalogItemId) {
                                $scope.Film.Distributor = $scope.ListDistributor[i];
                            }
                        }
                        //Thời lượng
                        $scope.Film.Duration = film.Duration;
                        //Phụ đề
                        for (i = 0; i < $scope.ListSubtitle.length; i++) {
                            if (film.SubtitleId === $scope.ListSubtitle[i].CatalogItemId) {
                                $scope.Film.Subtitle = $scope.ListSubtitle[i];
                            }
                        }

                        ////Studio
                        //for (i = 0; i < $scope.ListStudio.length; i++) {
                        //    if (film.StudioId === $scope.ListStudio[i].CatalogItemId) {
                        //        $scope.Film.Studio = $scope.ListStudio[i];
                        //    }
                        //}

                        //Nội dung tóm tắt Vi
                        $scope.Film.ShortDescription = film.ShortDescription;
                        //Nội dung tóm tắt En
                        $scope.Film.ShortDescription_F = film.ShortDescription_F;
                        //TrailerURL
                        $scope.Film.TrailerURL = film.TrailerURL;
                        //Poster
                        for (i = 0; i < film.ListPosterUrl.length; i++) {
                            $scope.Film.ListPosterUrl.push({
                                AbsolutePath: film.ListPosterUrl[i].AbsolutePath,
                                Link: ConstantsApp.BASED_FILE_URL + "/" + film.ListPosterUrl[i].AbsolutePath,
                            });
                            if (film.ListPosterUrl[i].MainPoster === true) {
                                $scope.Film.MainPosterURL = film.ListPosterUrl[i].AbsolutePath;
                            }
                        }

                        $scope.Film.FilmId = film.FilmId;
                        $scope.Film.Description = film.Description;
                        $scope.Film.Description_F = film.Description_F;

                        if (film.OpeningDate !== null && film.OpeningDate !== undefined) {
                            $scope.Film.OpeningDate = new Date(film.OpeningDate);
                        }

                        $scope.Film.Rate = film.Rate;
                        $scope.Film.StudioName = film.StudioName;

                        $scope.Film.Order = film.Order;
                        $scope.Film.Status = film.Status;
                        $scope.Film.Hot = film.Hot;
                        $scope.Film.CensorStatus = film.CensorStatus;
                        $scope.Film.Actors = film.Actors;
                        $scope.Film.MainLanguage = film.MainLanguage;
                        $scope.Film.Director = film.Director;
                        //$scope.Film.SpecialShow = film.SpecialShow;
                        $scope.Film.NowShowing = film.NowShowing;
                        $scope.Film.GroupId = film.GroupId;

                        // #endregion
                    } else {
                        $notifications.error('Xảy ra lỗi trong quá trình lấy dữ liệu', 'Lỗi');
                    }
                });
            };

            var init = function () {
                if ($state.current.data.cmd === 'update') {
                    $scope.IsUpdate = true;
                } else if ($state.current.data.cmd === 'copy') {
                    $scope.IsCopy = true;
                } else {
                    $scope.Film.Status = true;
                }
                var cmd1 = initGenre();
                var cmd2 = initFilmFormat();
                var cmd3 = initDistributor();
                var cmd4 = initFilmRestrictAge();
                var cmd5 = initSubtitle();
                var cmd6 = initDubbing();
                $q.all([cmd1, cmd2, cmd3, cmd4, cmd5, cmd6]).then(function () {
                    if ($state.current.data.cmd === 'update' || $state.current.data.cmd === 'copy') {
                        initFilmData();
                    }
                    if ($state.current.data.cmd !== 'update') {
                        initNewCode();
                    }
                });
            };

            init();
        }]);
}());

