define(function () {
    'use strict';
    angular.module("SavisApp")
    .controller('PointOfSaleController', ['$scope', '$uibModal', 'ConstantsApp', 'UtilsService', 'CinemaApiService', 'PointOfSaleApiService',
    function PointOfSaleController($scope, $uibModal, ConstantsApp, UtilsService, CinemaApiService, PointOfSaleApiService){
        var listPromise = [];

        $scope.CONST_POS_ACTION_PERMISSION = ConstantsApp.POS_ACTION_PERMISSION;

        $scope.Grid = {};
        $scope.Grid.CheckAll = false;
        $scope.Grid.PageSize = 10;
        $scope.Grid.PageNumber = 1;
        $scope.Grid.Data = [];

        $scope.Button = {};
        $scope.Button.Delete=  {};
        $scope.Button.Delete.Visible = false;

        $scope.Cinema = {}
        $scope.Cinema.Data = [];
        $scope.Cinema.Selected = '';

        $scope.Search = {};
        $scope.Search.Text = '';

        $scope.dataHeader = [
            { Key: '#', Value: "#", Width: '3%' },
            { Key: 'Code', Value: "Mã POS", Width: '17%' },
            { Key: 'Name', Value: "Tên POS", Width: '20%' },
            { Key: 'CinemaName', Value: "Thuộc rạp", Width: '10%' },
            { Key: 'ActionPermission', Value: "Quền hành động", Width: '20%' },
            { Key: 'Status', Value: "Trạng thái", Width: '10%' },
            { Key: 'Action', Value: "Thao tác", Width: 'auto' }
        ];

        // Paging
        $scope.Button.GoToPage = {};
        $scope.Button.ChangePageSize = {};

        $scope.Grid.PageSizeOptions = [10, 20];
        $scope.Grid.Refesh = function(){
            Init();
        }
        
        $scope.Button.GoToPage.Click = function (pageTogo) {
            $scope.Grid.PageNumber = pageTogo;
            $scope.Grid.Search();
        }

        $scope.Button.ChangePageSize.Click = function(){
            $scope.Grid.Search()
        }

        $scope.Grid.Search = function(){
            var dataSearch = {
                TextSearch: $scope.Search.Text != '' ? $scope.Search.Text : null,
                PageNumber: $scope.Grid.PageNumber,
                PageSize: $scope.Grid.PageSize,
                CinemaId: $scope.Cinema.Selected == "ALL" ? null: $scope.Cinema.Selected
            }

            var p = PointOfSaleApiService.GetFilter(dataSearch);
            listPromise.push(p);
            
            p.then(function onSuccess(response) {
                var responseResult = response.data;
                $scope.Grid.Data = angular.copy(responseResult.Data);

                angular.forEach($scope.Grid.Data, function(element){
                    element.ActionPermissionArr = JSON.parse(element.ActionPermission);
                });

                // Paging
                $scope.Grid.TotalCount = responseResult.TotalCount;
                $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

            }, function onError(response) {
                console.log("ERROR", response);
            });
        }

        $scope.Button.Delete = {};
        $scope.Button.Delete.Click = function(type, item){
            var infoResult = UtilsService.OpenDialog('Bạn có chắc muốn xóa POS này?', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
            var listItemDeleteId = [];

            if(type == 1){
                //delete multi
                angular.forEach($scope.Grid.Data, function(element){
                    if(element.Selected){
                        listItemDeleteId.push(element.POSId);
                    }
                });
            }else{
                //delete one
                listItemDeleteId.push(item.POSId);
            }

            infoResult.result.then(function (modalResult) {
                if (modalResult === 'confirm') {
                    var listDeleteField = [];

                    var promise = PointOfSaleApiService.DeleteMany(listItemDeleteId);

                    promise.then(function (response) {
                        angular.forEach(response.data.Data, function (item) {
                            if (item.Result !== -1) {
                                listDeleteField.push({ Name: item.Name, Result: item.Code, Message: item.Message });
                            }
                            else if (item.Result === -1) {
                                $scope.error('Lỗi hệ thống !');
                                $notifications.error("Lỗi hệ thống", "Lỗi");
                            }
                        });
                        Init();
                        UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                    });
                }
            });
        }

        $scope.Button.Detail = {};
        $scope.Button.Detail.Click = function(item){
            $uibModal.open({
                animation: true,
                templateUrl: "app-data/views/business/point-of-sale/point-of-sale-item.html",
                controller: 'PosItemController',
                size: 'lg',
                resolve: {
                    Data: function() {
                        return item;
                    },
                    TypeModal: function() {
                        return 1; // Detail
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
            });
        }

        $scope.Button.Update = {};
        $scope.Button.Update.Click = function(item){
            $uibModal.open({
                animation: true,
                templateUrl: "app-data/views/business/point-of-sale/point-of-sale-item.html",
                controller: 'PosItemController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    Data: function() {
                        return item;
                    },
                    TypeModal: function() {
                        return 0; // Update
                    }
                }
            }).result.then(function (result) {
                Init();
                UtilsService.OpenDialog(result, 'Thông báo', '', 'Đóng', 'sm', '');
             }, function () {
                console.log("Dialog dismissed");
             }).catch(function (resp) {
                if (['cancel'].indexOf(resp) === -1) throw resp;
            });
        }

        $scope.Button.Add = {};
        $scope.Button.Add.Click = function(){
            $uibModal.open({
                animation: true,
                templateUrl: "app-data/views/business/point-of-sale/point-of-sale-item.html",
                controller: 'PosItemController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    Data: function() {
                        return null;
                    },
                    TypeModal: function() {
                        return 2; // Add New
                    }
                }
            }).result.then(function (result) {
                Init();
                UtilsService.OpenDialog(result, 'Thông báo', '', 'Đóng', 'sm', '');
             }, function () {
                console.log("Dialog dismissed");
             }).catch(function (resp) {
                if (['cancel'].indexOf(resp) === -1) throw resp;
            });
        }
        
        $scope.Grid.CheckAllItem = function() {
            angular.forEach($scope.Grid.Data, function (item) {
                item.Selected = $scope.Grid.CheckAll;
            });
            $scope.Button.Delete.Visible = $scope.Grid.CheckAll;
        };

        $scope.Grid.CheckItem = function() {
            var count = 0;
            // Calculate item selected count     
            angular.forEach($scope.Grid.Data, function (item) {
                if (item.Selected == true) { count++; }
            });

            if(count > 0){
                $scope.Button.Delete.Visible = true;
            }else{
                $scope.Button.Delete.Visible = false;
            }

            // If all items is checked
            if (count === $scope.Grid.Data.length) {
                $scope.Grid.CheckAll = true;
            }else{
                $scope.Grid.CheckAll = false;
            }

        };

        var GetAllCinemaActive = function(){
            var p = CinemaApiService.GetAllCinemaActive();
            listPromise.push(p);
            p.then(function onSuccess(response) {
                var responseResult = response.data;
                $scope.Cinema.Data = angular.copy(responseResult.Data);

                $scope.Cinema.Selected = 'ALL';
            }, function onError(response) {
                console.log("ERROR", response);
            });
        }

        var Init = function(){
            // set default search
            $scope.Cinema.Selected = '';
            $scope.Grid.PageSize = 10;
            $scope.Grid.PageNumber = 1;

            GetAllCinemaActive();
            $scope.Grid.Search();
        }

        Init();

    }]);
}());