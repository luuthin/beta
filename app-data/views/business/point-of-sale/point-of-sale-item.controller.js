define(function () {
    'use strict';
    angular.module("SavisApp")
    .controller('PosItemController', ['$scope', '$uibModalInstance', 'ConstantsApp', 'Data', 'TypeModal', 'CinemaApiService', 'PointOfSaleApiService',
    function PosItemController($scope, $uibModalInstance, ConstantsApp, Data, TypeModal, CinemaApiService, PointOfSaleApiService){
        var listPromise = [];

        $scope.Cinema = {}
        $scope.Cinema.Data = [];
        $scope.Cinema.Default = true;

        $scope.POS = {};
        $scope.POS.Data = Data ? angular.copy(Data) : {};
        $scope.POS.Disabled = TypeModal == 1 ? true : false;

        $scope.Button = {}
        $scope.ActionPermission = {};

        $scope.CONST_POS_ACTION_PERMISSION = angular.copy(ConstantsApp.POS_ACTION_PERMISSION);

        var GetAllCinemaActive = function(){
            var p = CinemaApiService.GetAllCinemaActive();
            listPromise.push(p);
            p.then(function onSuccess(response) {
                var responseResult = response.data;
                $scope.Cinema.Data = angular.copy(responseResult.Data);

                if($scope.POS.Data.POSId != null && $scope.POS.Data.POSId != ""){
                    $scope.Cinema.Default = false;
    
                    angular.forEach($scope.Cinema.Data, function(element){
                        element.Checked = false;
                        
                        if(element.id == $scope.POS.Data.CinemaId){
                            element.Checked = true;
                        }
                    })
                    $scope.Cinema.Selected = $scope.POS.Data.CinemaId;
                }else{
                    $scope.Cinema.Selected = 'ALL';
                }

            }, function onError(response) {
                console.log("ERROR", response);
            });
        }

        $scope.Button.Edit = {}
        $scope.Button.Edit.Click = function(){
            $scope.POS.Disabled = false;
        }

        $scope.Button.Ok = {}
        $scope.Button.Ok.Click = function(){
            var p;
            $scope.ActionPermission.Data = [];

            //convert action permission
            angular.forEach($scope.CONST_POS_ACTION_PERMISSION, function(element){
                if(element.Checked){
                    $scope.ActionPermission.Data.push(element.Key);
                }
            });
            $scope.POS.Data.ActionPermission = angular.toJson($scope.ActionPermission.Data);
            // set CinemaId
            $scope.POS.Data.CinemaId = $scope.Cinema.Selected;

            if($scope.POS.Data.POSId != null && $scope.POS.Data.POSId != ''){
                // Update POS
                p = PointOfSaleApiService.Update($scope.POS.Data.POSId, $scope.POS.Data);
            }else{
                // Add new POS
                p = PointOfSaleApiService.Add($scope.POS.Data);
            }

            listPromise.push(p);

            p.then(function onSuccess(response) {
                $uibModalInstance.close(response.data.Message)
            }, function onError(response) {
                console.log("ERROR", response);
            });
        }

        $scope.Button.Cancel = {}
        $scope.Button.Cancel.Click = function() {
            $uibModalInstance.dismiss('cancel');
        };

        var init = function(){
            GetAllCinemaActive();
            // console.log('init add new pos');

            // set checked action permission
            if($scope.POS.Data.POSId != null && $scope.POS.Data.POSId != ""){
                angular.forEach($scope.CONST_POS_ACTION_PERMISSION, function(element){
                    if($scope.POS.Data.ActionPermission.indexOf(element.Key) != -1){
                        element.Checked = true;
                    }
                });
            }
        }

        init();

    }]);

}());