// 'use strict';
define(['jquery', 'app', 'components/directive/checklist-model', 'components/factory/factory', 'components/service/amdservice'], function (jQuery, app) {

    'use strict';
    /*
    *
    * User instance controller
    * This controller is to popup data for user
    */
    app.controller(
       // 'UserInstance',
       //function ($scope, $location, $uibModalInstance, $http, user, options, roles, applicationId, constantsFactory, $log, $uibModal, Notifications) {
           "UserInstance", ['$scope', '$location', '$http', '$uibModalInstance', '$filter', '$log', '$uibModal', 'constantsAMD', 'Notifications', 'constantsFactory', 'user', 'options', 'roles', 'applicationId',
           function ($scope, $location, $http, $uibModalInstance, $filter, $log, $uibModal, constantsAMD, Notifications, constantsFactory, user, options, roles, applicationId) {
               /* Notification */
               $scope.Notifications = Notifications;
               $scope.closeAlert = function (item) {
                   Notifications.pop(item);
               };
               $scope.success = function (title, message) {
                   constantsAMD.setNotification(Notifications, 'info-default', title, message);
               };
               $scope.error = function (title, message) {
                   constantsAMD.setNotification(Notifications, 'danger', title, message);
               };
               $scope.success = function (message) {
                   constantsAMD.setNotification(Notifications, 'info-default', 'Thông báo', message);
               };
               $scope.error = function (message) {
                   constantsAMD.setNotification(Notifications, 'danger', 'Thông báo', message);
               };
               //$scope.word = /^\s*\w*\s*\g*$/;
               $scope.word = /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,24}$/;
               $scope.error = function (title, message) {
                   constantsAMD.setNotification(Notifications, 'danger', title, message);
               };
               $scope.options = options;
               $scope.Select = {};
               var defaultOrgani = { OrganizationId: null, Name: "--Chọn đơn vị/phòng ban--" };
               var defaultChucVu = { CatalogItemId: null, Name: "--Chọn chức vụ--" };
               /* Declare constant domain and path to Api */
               var apiUrl = constantsFactory.ApiUrl;
               var ApiUsersUrl = constantsFactory.ApiUsersUrl;
               $scope.Autofocus = true;
               $scope.isEdit = false;
               $scope.IsInfo = false;
               /* assign parameters passed from Form Parent (programmanager.html) or controller (app)*/
               $scope.CurrentUser = user;
               if ($scope.CurrentUser != null) {
                   $scope.selected = {
                       item: $scope.CurrentUser[0]
                   };
               };

               $scope.items = {};

               $scope.readOnly = false;

               /* Define the form type : add/edit/info */
               var formType = $scope.options[0].Type;

               /*
               * System roles
               */
               $scope.Roles = roles;
               $scope.SelectedRoles = [];
               $scope.CurrentPhongBan = {};

               var initFormData = function () {
                   var organizations = initOrganizations();
                   var chucVuI = initChucVu();
                   /* Check parameters type = 'edit' to load data edit to form edit */
                   if ($scope.options[0].Type == 'edit') {

                       $scope.isEdit = true;
                       $scope.readOnly = true;
                       $scope.ProgramTitle = "Cập nhật người dùng";
                       if ($scope.CurrentUser != null) {
                           var promise = $http({
                               method: 'GET',
                               url: apiUrl + ApiUsersUrl + '/' + $scope.CurrentUser.UserId + '/profile',
                           })
                      .success(function (data) {
                          if (data.Data != null) {
                              $scope.UserName = data.Data.UserName;
                              $scope.Email = data.Data.Email;
                              $scope.Mobile = data.Data.Mobile;
                              $scope.Comment = data.Data.Comment;
                              $scope.NickName = data.Data.NickName;
                              $scope.FullName = data.Data.FullName;
                              $scope.AnhDaiDien = data.Data.Avatar;
                              if ($scope.AnhDaiDien == null || $scope.AnhDaiDien == '') {
                                  $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                              }

                              organizations.then(function (a) {
                                  angular.forEach($scope.PhongBan, function (item) {
                                      if (item.OrganizationId === data.Data.OrganizationId) {
                                          $scope.Select.DanhSachNhomOp = item;
                                      }
                                  });
                              });
                              chucVuI.then(function (a) {
                                  angular.forEach($scope.ChucVuData, function (item) {
                                      if (item.CatalogItemId === data.Data.ChucVuId) {
                                          $scope.Select.ChucVuOp = item;
                                      }
                                  });
                              });
                              // $scope.Mobile = $scope.CurrentUser.Mobile;
                              //$scope.ChucVu = $scope.CurrentUser.ChucVu;
                              $scope.Active = $scope.CurrentUser.Active;

                          }
                      }).error(function (response) {
                          $log.error();
                          $log.debug(response);
                      });
                       }

                   } /* Check parameters type = 'info' to load data info to form info */
                   else if ($scope.options[0].Type == 'info') {
                       $scope.IsInfo = true;
                       $scope.Messenger = $scope.options[0].Messager;
                       $scope.readOnly = true;
                       $scope.ProgramTitle = "Thông tin chi tiết tài khoản người dùng";
                       if ($scope.CurrentUser != null) {
                           var promise = $http({
                               method: 'GET',
                               url: apiUrl + ApiUsersUrl + '/' + $scope.CurrentUser.UserId + '/profile',
                           })
                      .success(function (data) {
                          if (data.Data != null) {
                              $scope.UserName = data.Data.UserName;
                              $scope.Email = data.Data.Email;
                              $scope.Mobile = data.Data.Mobile;
                              $scope.Comment = data.Data.Comment;
                              $scope.NickName = data.Data.NickName;
                              $scope.FullName = data.Data.FullName;
                              $scope.AnhDaiDien = data.Data.Avatar;
                              if ($scope.AnhDaiDien == null || $scope.AnhDaiDien == '') {
                                  $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                              }
                              $scope.PhongBanInfo = data.Data.OrganizationName;
                              $scope.DonVi = data.Data.ParentOrganizationName;

                              chucVuI.then(function (a) {
                                  angular.forEach($scope.ChucVuData, function (item) {
                                      if (item.CatalogItemId === data.Data.ChucVuId) {
                                          $scope.ChucVuInfo = item.Name;
                                      }
                                  });
                              });
                              // $scope.Mobile = $scope.CurrentUser.Mobile;
                              //$scope.ChucVu = $scope.CurrentUser.ChucVu;
                              $scope.Active = $scope.CurrentUser.Active;

                          }
                      }).error(function (response) {
                          $log.error();
                          $log.debug(response);
                      });
                       }
                   } /* Check parameters type = 'add' to load data add to form add */

                   else if ($scope.options[0].Type == 'add') {
                       $scope.ProgramTitle = "Thêm mới người dùng";
                       $scope.isAdd = true;
                       $scope.Active = true;
                       //Select first item            
                   }
                   else {
                       $scope.ProgramTitle = $scope.options[0].TitleInfo;
                       $scope.Messenger = $scope.options[0].Messager;
                       $scope.listDelete = $scope.options[0].Data;
                       $scope.Confirm = $scope.options[0].ButtonCancel;
                   };
               };

               var initRolesOfCurrentUser = function () {
                   var promise = $http({
                       method: 'GET',
                       url: apiUrl + ApiUsersUrl + "/" + $scope.CurrentUser.UserId + "/roles?applicationId=" + applicationId
                   });
                   promise.success(function (response) {
                       var userRoles = response.Data.Roles;
                       console.log(userRoles);

                       angular.forEach(userRoles, function (userRole) {
                           $scope.SelectedRoles.push(userRole.RoleCode);
                       });

                       console.log($scope.Roles);
                       // $scope.$apply();

                   });

                   promise.then(function () {
                       // $scope.$apply();
                   });

                   return promise;
               };

               //Todo:
               var initOrganizations = function () {
                   var promise = $http({
                       method: 'GET',
                       url: apiUrl + "api/organizations/listtree"
                   })
                   .success(function (data) {
                       $scope.DanhSachNhom = data.Data;
                       $scope.DanhSachNhom.unshift(defaultOrgani);
                       $scope.PhongBan = angular.copy($scope.DanhSachNhom);

                       if ($scope.options[0].Type == 'edit') {
                           var position = null;
                           for (var i = 0; i < $scope.CurrentUser.length; i++) {
                               if ($scope.CurrentUser[i].UserId == $scope.options[0].id) {
                                   position = i;
                                   break;
                               };
                           };
                           if (position != null) {

                               angular.forEach($scope.DanhSachNhom, function (item) {
                                   if (item.OrganizationId === items[position].OrganizationId) {
                                       $scope.Select.DanhSachNhomOp = item;
                                   }
                               });

                           } else {

                           };

                       }

                   });
                   return promise;
               };

               var initChucVu = function () {
                   var promise = $http({
                       method: 'GET',
                       url: apiUrl + "api/catalogs/catalogmaster/chuc-vu-user/itemlist"
                   })
                   .success(function (data) {
                       $scope.ChucVuData = data.Data;
                       $scope.ChucVuData.unshift(defaultChucVu);
                   });
                   return promise;
               };

               var initNhomByCurentUser = function () {

                   var promise = $http({
                       method: 'GET',
                       url: apiUrl + '/api/organizations/users?userId=' + app.CurrentUser.Id
                   }).success(function (response) {
                       console.log(response);
                       if (response != null) {
                           $scope.CurrentPhongBan = response[0];

                           angular.forEach($scope.DanhSachNhom, function (item) {
                               if ($scope.CurrentPhongBan.OrganizationId == item.OrganizationId) {
                                   $scope.Select.DanhSachNhomOp = item;
                               }
                           });
                       }
                   });
                   return promise;
               };


               /* 
               * User name availability check
               * This section provive user availability check, to check whether an user name is valid or not
               * A scope function will be defined to store information of availability
               */
               $scope.Availability = {};
               $scope.Availability.Status = 0;

               /* Check availability function */
               $scope.CheckNameAvailability = function (name) {
                   if (name != null && name.trim() != '' && name != 'undefined') {
                       var promise = $http({
                           method: 'GET',
                           url: apiUrl + ApiUsersUrl + "/availability?userName=" + name
                       }).success(function (response) {
                           $scope.Availability.Result = response;

                           if (response.Status == 1 && response.Data == true) {
                               $scope.Availability.Status = 1;

                           } else {
                               $scope.Availability.Status = -1;
                           }
                       });
                       return promise;
                   }
                   else
                       $scope.Name = "";

               };


               /* Function button "Save" with add and edit button*/
               $scope.Save = function () {
                   $scope.user_form.$setSubmitted();
                   if ($scope.Select.DanhSachNhomOp === undefined) {
                       $scope.$broadcast('UiSelectDemo1');
                       return false;
                       $scope.error("Vui lòng kiểm tra thông tin đã nhập");
                       return;
                   }
                   else {
                       if ($scope.Select.DanhSachNhomOp.OrganizationId === null) {
                           $scope.$broadcast('UiSelectDemo1');
                           return false;
                           $scope.error("Vui lòng kiểm tra thông tin đã nhập");
                           return;
                       }
                   }
                   if ($scope.user_form.$invalid) {
                       angular.element("[name='" + $scope.user_form.$name + "']").find('.ng-invalid:visible:first').focus();
                       $scope.error("Vui lòng kiểm tra thông tin đã nhập");
                       return false;
                   }

                   /* If Type of parameter = 'add' ==> request to API with parameter */
                   if ($scope.options[0].Type == 'add') {
                       $scope.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                       if ($scope.UserName == 'undefined')
                           $scope.UserName = "";
                       var checkName = $scope.CheckNameAvailability($scope.UserName);
                       checkName.then(function () {
                           if ($scope.Availability.Status == -1) {
                               $scope.error("Tên tài khoản đã tồn tại, vui lòng chọn tên khác!");
                               return false;
                           }
                           /* Check the two inserted password */
                           if ($scope.items.Password != $scope.items.ConfirmedPassword) {
                               $scope.error("Vui lòng kiểm tra lại mật khẩu!");
                               return false;
                           }

                           // Add user
                           addUser();
                       });
                   }
                       /* If Type of parameter = 'edit' ==> request to API with parameter */
                   else {
                       updateUser();
                   };
               };

               var updateUser = function () {
                   var putData = {};
                   putData.UserOrganization = {};
                   putData.ApplicationId = applicationId;
                   putData.UserId = $scope.CurrentUser.UserId;
                   putData.UserName = $scope.UserName;
                   putData.UserOrganization.OrganizationId = (typeof $scope.Select.DanhSachNhomOp != 'undefined' && $scope.Select.DanhSachNhomOp != null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                   putData.ChucVuId = (typeof $scope.Select.ChucVuOp != 'undefined' && $scope.Select.ChucVuOp != null) ? $scope.Select.ChucVuOp.CatalogItemId : null;
                   putData.CreatedByUserId = app.CurrentUser.Id;
                   putData.ModifiedByUserId = app.CurrentUser.Id;
                   putData.UserOrganization.CreatedByUserId = app.CurrentUser.Id;
                   putData.UserOrganization.LastModifiedByUserID = app.CurrentUser.Id;
                   putData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                   putData.Mobile = $scope.Mobile;
                   putData.Email = $scope.Email;
                   putData.Password = $scope.items.Password;
                   putData.Roles = [];
                   putData.Active = $scope.Active;
                   putData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                   putData.CreatedDate = new Date();
                   putData.UpdateDate = new Date();


                   // Process selected roles
                   angular.forEach($scope.SelectedRoles, function (selectedRoleCode) {
                       angular.forEach($scope.Roles, function (role) {
                           if (selectedRoleCode == role.RoleCode) {
                               // Add this selected role to putData
                               putData.Roles.push(role);
                           }
                       });
                   });

                   var request = $http({
                       method: 'PUT',
                       url: apiUrl + ApiUsersUrl + '/' + $scope.CurrentUser.UserId,
                       headers: {
                           'Content-type': ' application/json'
                       },
                       data: putData
                   }).success(function (response) {
                       /* If request Success then update to Grid*/

                       $uibModalInstance.close($scope.selected.item);
                       $scope.success("Cập nhật thành công người dùng: " + response.Data.UserName);
                   });
               };
               //SelectAllRole
               $scope.SelectAllRole = function () {
                   ////angular.forEach($scope.Roles, function (rl) {
                   ////    $scope.SelectedRoles.push(rl);
                   ////});
                   //$scope.SelectedRoles = $scope.Roles;
               };

               var addUser = function () {
                   var postData = {};
                   postData.ApplicationId = applicationId;
                   postData.UserOrganization = {};
                   postData.UserOrganization.OrganizationId = (typeof $scope.Select.DanhSachNhomOp != 'undefined' && $scope.Select.DanhSachNhomOp != null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                   postData.UserOrganization.CreatedByUserId = app.CurrentUser.Id;
                   postData.UserOrganization.LastModifiedByUserID = app.CurrentUser.Id;
                   postData.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                   postData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                   postData.Mobile = $scope.Mobile;
                   postData.Email = $scope.Email;
                   postData.CreatedByUserId = app.CurrentUser.Id;
                   postData.ModifiedByUserId = app.CurrentUser.Id;
                   postData.Password = $scope.items.Password;
                   postData.Roles = [];
                   postData.Active = $scope.Active;
                   postData.Type = true;
                   postData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                   postData.Avatar = "assets/layout4/img/icon-user-default.png";
                   postData.ChucVuId = (typeof $scope.Select.ChucVuOp != 'undefined' && $scope.Select.ChucVuOp != null) ? $scope.Select.ChucVuOp.CatalogItemId : null;
                   // Process selected roles
                   angular.forEach($scope.SelectedRoles, function (selectedRoleCode) {
                       angular.forEach($scope.Roles, function (role) {
                           if (selectedRoleCode == role.RoleCode) {
                               // Add this selected role to putData
                               postData.Roles.push(role);
                           }
                       });
                   });

                   var request = $http({
                       method: 'POST',
                       url: apiUrl + ApiUsersUrl,
                       headers: {
                           'Content-type': ' application/json'
                       },
                       data: postData
                   }).success(function (response) {
                       $uibModalInstance.close(response.Data);
                       $scope.success("Thêm mới thành công người dùng: " + response.Data.UserName);
                   });

                   return request;
               };

               /* Button close Popup Form */
               $scope.Cancel = function () {
                   $uibModalInstance.dismiss();
               };

               var profileDialogTemplateUrl = '/app-data/views/core/users/passwordChangeDialog.html';
               var infoDialogTemplateUrl = '/app-data/views/core/users/infoDialog.html';

               /*Open Edit Password Form*/
               $scope.OpenFormEditPassWord = function () {
                   var modalInstance = $uibModal.open({
                       templateUrl: profileDialogTemplateUrl,
                       controller: 'AdminChangePassWordCtrl',
                       size: 'md',
                       resolve: {
                           userId: function () {
                               return $scope.CurrentUser.UserId;

                           },
                           options: function () {
                               return [{ Type: 'edit', data: $scope.CurrentUser }];
                           }
                       }
                   });

                   modalInstance.result.then(function (selectedItem) {
                       if (selectedItem != null) {

                           showModalDialog("Cập nhật thành công", "Chú ý", "Chấp nhận", "Đóng");
                       } else {
                           showModalDialog("Cập nhật thất bại", "Chú ý", "Chấp nhận", "Đóng");
                       };
                   }, function () {
                       // $log.info('Modal dismissed at: ' + new Date());
                   });
               };


               /* Form data initiate */
               initFormData();
               if (formType != 'add') {
                   initRolesOfCurrentUser();
               }


           }]);
});