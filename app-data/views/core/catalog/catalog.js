﻿define(['jquery', 'app', 'angular-sanitize',
    'components/factory/factory',
    'components/service/apiservice',
    'components/service/amdservice',
    'views/catalog/catalog-item',
    'views/arm/archivetype/field/field-item',
    'views/arm/archivetype/field/field-select',
], function (jQuery, app) {
    app.controller('CatalogCtrl', ['$scope', '$rootScope', '$uibModal', '$sce', '$filter', '$timeout', '$location', '$log', '$http', 'constantsFactory',
        'constantsAMD', '$routeParams', 'Notifications', 'CatalogService',
        function ($scope, $rootScope, $uibModal, $sce, $filter, $timeout, $location, $log, $http, constantsFactory,
            constantsAMD, $routeParams, notifications, CatalogService) {
            /* Notification */
            $scope.Notifications = notifications;
            $scope.closeAlert = function (item) {
                notifications.pop(item);
            };
            $scope.success = function (title, message) {
                constantsAMD.setNotification(notifications, 'info-default', title, message);
            };
            $scope.error = function (title, message) {
                constantsAMD.setNotification(notifications, 'danger', title, message);
            };

            var addCatalogUrl = '/app-data/views/catalog/catalog-item.html';

            /*------------------------------------------------------------------**/

            $scope.ListSelected = [];
            $scope.SelectAll = false;
            $scope.Mode = 1;
            /* Header grid datatable */
            $scope.Headers = [
                { Key: 'Name', Value: "Tên danh mục", Width: '24px', Align: 'left' },
                { Key: 'Code', Value: "Mã danh mục", Width: '24px', Align: 'left' },
                { Key: 'Active', Value: "Trạng thái", Width: '130px', Align: 'left' },
                { Key: 'Description', Value: "Ghi chú", Width: 'auto', Align: 'left' },
                { Key: 'Handller', Value: "Hành động", Width: '15%', Align: 'left' },
            ];
            var loadData = function () {
                 var qs = $location.search();
                if (typeof (qs["search"]) !== 'undefined') {
                    $scope.TextSearch = qs["search"];
                } else {
                    $scope.IsNotReload = true; $location.search("search", "");
                    $scope.TextSearch = "";
                }

                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.PageNumber = qs["pn"];
                } else {
                    $scope.IsNotReload = true; $location.search("pn", "1");
                    $scope.PageNumber = 1;
                }

                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.PageSize = qs["ps"];
                } else {
                    $scope.IsNotReload = true; $location.search("ps", "10");
                    $scope.PageSize = 10;
                }

                var postData = {};
                postData.PageNumber = $scope.PageNumber;
                postData.PageSize = $scope.PageSize;
                postData.TextSearch = $scope.TextSearch;

                /* REGION : INIT */
                /* Init function, this function will be executed on page load */
                var init = function () {
                    var promise = CatalogService.GetFilter(postData);
                    promise.success(function (data) {
                        $log.debug(data)
                        if (data.Status != null) {
                            $scope.ListData = data.Data;
                            $scope.TotalCount = data.TotalCount;
                            $scope.MaxSizePage = 7;
                            $scope.FromCatalog = 0;
                            $scope.ToCatalog = 0;
                            if ($scope.TotalCount !== 0) {
                                $scope.FromCatalog = parseInt(($scope.PageNumber - 1) * $scope.PageSize + 1);
                                $scope.ToCatalog = $scope.FromCatalog + $scope.ListData.length - 1;
                            }
                        }
                    }).error(function (response) {
                        $log.debug(response);
                    });
                    return promise;
                };
                /* END REGION : INIT */
                init();
            };
            loadData(); 
            $scope.$on('$routeUpdate', function (e) {
                if (!$scope.IsNotReload) {
                    loadData();
                } else {
                    $scope.IsNotReload = false;
                }
            });
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.order = function (predicate, reverse) {
                $scope.ListData = orderBy($scope.ListData, predicate, reverse);
            };
            $scope.SelectItem = function (item) {
                if (!item.Selecting) {
                    var index = $scope.ListSelected.indexOf(item);
                    if (index >= 0) {
                        $scope.ListSelected.splice(index, 1);
                    }
                } else {
                    $scope.ListSelected.push(item);
                }
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.SelectAll = true;
                } else {
                    $scope.SelectAll = false;
                }
            }

            $scope.SelectAllItem = function () {
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = false;
                    });
                    $scope.SelectAll = false;
                } else {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = true;
                        $scope.ListSelected.push(file);
                    });
                    $scope.SelectAll = true;
                }
            }
            /* This function is to Check items in grid highlight*/
            $scope.GetItemHeaderClass = function (item) {
                if (item.selected == true) {
                    return 'table-sort-asc';
                } else if (item.selected == false) {
                    return 'table-sort-desc';
                } else {
                    return 'table-sort-both';
                };
            };
            /* Sorting grid By Click to header */
            $scope.ClickToHeader = function (item) {
                // Set all class header to default
                angular.forEach($scope.ListData, function (items) {
                    if (items != item) {
                        items.selected = null;
                    };
                });
                // Set for item click sorted
                if (item.selected == true) {
                    item.selected = false;
                    $scope.order(item.Key, false);
                } else {
                    item.selected = true;
                    $scope.order(-item.Key, false);
                };
            };
            $scope.Button = {};
            $scope.Button.Create = {};
            $scope.Button.Create.Click = function () {
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: addCatalogUrl,
                    controller: 'CatalogItemCtrl',
                    size: 'lg',
                    windowClass: "modal-full",
                    // Set parameter to childform (popup form)
                    resolve: {
                        item: function () {
                            return null;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "add";
                            return obj
                        },
                    }
                });
                modalInstance.result.then(function (response) {
                    loadData();
                }, function (response) { });
            };
            $scope.Button.Update = {};
            $scope.Button.Update.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: addCatalogUrl,
                    controller: 'CatalogItemCtrl',
                    size: 'lg',
                    windowClass: "modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return item;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "update";
                            return obj
                        },
                    }
                });

                modalInstance.result.then(function (response) {
                    loadData();
                }, function (response) { });
            };

            $scope.Button.Copy = {};
            $scope.Button.Copy.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var promise = CatalogService.Clone(item.CatalogMasterId);
                promise.success(function (response) {
                    $log.debug(response);
                    if (response.Status === 1) {
                        $scope.success("Thông báo!", "Sao chép thành công");
                    } else {
                        $scope.error("Thông báo!", "Sao chép thành công");
                    }
                    loadData();
                }).error(function (response) {

                    $log.error(response);
                    $scope.error("Thông báo!", "Sao chép thành công");
                });
            };
            $scope.Button.TemplateConfig = {};
            $scope.Button.TemplateConfig.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                window.location.href = '#/metadata-template-manager/' + item.MetadataTemplateId;
            };
            $scope.Button.Delete = {};
            $scope.Button.Delete.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var infoResult = constantsAMD.OpenDialog('Bạn có chắc chắn muốn xóa danh mục này?', 'Chú ý', 'Đồng ý', 'Đóng', 'sm');
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = CatalogService.Delete(item.CatalogMasterId);
                        promise.success(function (response) {
                            $log.debug(response);
                            loadData();
                            if (response.Status === 1) {
                                if (response.Data.Status) {
                                    $scope.success("Thông báo!", "Xóa thành công!");
                                } else {
                                    $scope.error("Thông báo!", "Danh mục đang được sử dụng!");
                                }
                            } else {
                                $scope.error("Thông báo!", "Danh mục đang được sử dụng!");
                            }
                        }).error(function () {
                            $scope.error("Thông báo!", "Xóa thất bại!");
                        });

                    };
                });
            };
            $scope.Button.DeleteMany = {};
            $scope.Button.DeleteMany.Click = function () {
                var listDeleteting = [];
                angular.forEach($scope.ListData, function (item) {
                    if (item.Selecting) {
                        listDeleteting.push(item.CatalogMasterId);
                    }
                });
                var infoResult = constantsAMD.OpenDialog('Bạn có chắc chắn muốn xóa những danh mục này?', 'Chú ý', 'Đồng ý', 'Đóng', 'sm');
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = CatalogService.DeleteMany(listDeleteting);
                        promise.success(function (response) {
                            $log.debug(response);
                            loadData();
                            if (response.Status === 1) {
                                var listDeleteField = [];
                                angular.forEach(response.Data, function (item) {
                                    if (item.Result == true) {
                                        listDeleteField.push({
                                            Name: item.Name,
                                            Result: "Xóa thành công",
                                            Description: ""
                                        });
                                        //$log.debug(item);
                                    } else if (item.Result == false) {
                                        //$log.debug(item);
                                        listDeleteField.push({
                                            Name: item.Name,
                                            Result: "Xóa thất bại",
                                            Description: item.Description
                                        });
                                    };
                                });

                                var data = {};
                                data.Items = listDeleteField;
                                var infoDeleteResult = constantsAMD.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', data);
                            } else {
                                $scope.error("Thông báo!", "Danh mục đang được sử dụng!");
                            }
                        }).error(function () {
                            $scope.error("Thông báo!", "Xóa thất bại!");
                        });
                    };
                });
            };

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.IsNotReload = true; $location.search("search", "");
                $scope.IsNotReload = true; $location.search("pn", "1");
                // $scope.IsNotReload = true; $location.search("ps", "10");
                loadData();
            };
            $scope.Button.Search = {};
            $scope.Button.Search.Click = function () {
                $scope.IsNotReload = true; $location.search("search", $scope.TextSearch);
                $scope.IsNotReload = true; $location.search("pn", "1");
                loadData();
            }
        }
    ]);
});
