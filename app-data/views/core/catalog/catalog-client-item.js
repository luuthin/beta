﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller("CatalogClientItemCtrl", ["$scope", "$q", "$log", "$uibModalInstance", "item", "option", "$timeout",
        'FormlyFactory', 'toastr', 'UtilsService', "ConstantsApp", "CatalogService", "FormlyService", "MetadataTemplateService", "CatalogItemService", "TaxonomyService",
        function ($scope, $q, $log, $uibModalInstance, item, option, $timeout,
            FormlyFactory, $notifications, UtilsService, ConstantsApp, CatalogService, FormlyService, MetadataTemplateService, CatalogItemService, TaxonomyService) {

            console.log(item);
            console.log("option", option);
            console.log("option.Catalog", option.Catalog);
            $scope.ListMetadataField = [];
            var defaultParentCatalog = { TermId: null, Name: "---Không có bản ghi cha---" };
            //Lấy các termcha có thể có
            var initListParrentOld = function () {
                // Set page loading
                var promise = TaxonomyService.GetByVocabularyId(option.Catalog.VocabularyId);
                promise.then(function onSuccess(data) {
                    console.log(data);
                    debugger
                    if (data.Data != null) {
                        $scope.ListCatalogData = data.Data;
                        $scope.ListCatalogData.unshift(defaultParentCatalog);
                        console.log($scope.ListCatalogData);
                    }
                }, function onError(response) {
                    $log.error();
                    $log.debug(response);
                });
                return promise;
            };
            var initListParrent = function () {
                // Set page loading
                var promise = CatalogService.GetItemByLevel(option.Catalog.CatalogMasterId);
                promise.then(function onSuccess(response) {
                    response = response.data;
                    if (response.Data != null) {
                        $scope.ListCatalogData = response.Data;
                        $scope.ListCatalogData.unshift(defaultParentCatalog);
                        console.log($scope.ListCatalogData);
                    }
                }, function onError(response) {
                    $log.error();
                    $log.debug(response);
                });
                return promise;
            };
            $scope.Data = {};
            $scope.DataPreview = {};
            $scope.PreviewSheme = [];
            //Lấy cấu trúc
            var initMetadataField = function () {
                debugger
                $scope.ListMetadataField = [];
                var formField = "";
                ///lấy thông tin template
                var cmd1 = MetadataTemplateService.GetById(option.Catalog.MetadataTemplateId);
                cmd1.then(function onSuccess(response) {
                    response = response.data;
                    if (response.Status === 1) {
                        console.log("SUCCESS", response);
                        formField = response.Data.FormConfig;
                        //$scope.success("Thông báo", "Tải dữ liệu thành công");
                    } else {
                        $notifications.error("Tải dữ liệu thất bại", "Thông báo");
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });

                var cmd2 = MetadataTemplateService.GetMetadataFieldByMetadataTemplateId(option.Catalog.MetadataTemplateId);
                cmd2.then(function onSuccess(response) {
                    debugger
                    response = response.data;
                    if (response.Status === 1) {
                        console.log("SUCCESS", response);
                        $scope.ListMetadataField = response.Data;
                        //delete $scope.PreviewSheme;
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                $q.all([cmd1, cmd2]).then(function () {
                    $scope.PreviewSheme = [];
                    try {
                        $scope.PreviewSheme = FormlyService.DeSerializeJSON(formField);
                    } catch (e) { }
                    if (!Array.isArray($scope.PreviewSheme)) {
                        //Sử dụng flow default
                        $scope.PreviewSheme = [];
                        angular.forEach($scope.ListMetadataField, function (item) {
                            item.FormlyContentObj = [];
                            try {
                                var obj = FormlyService.DeSerializeJSON(item.FormlyContent);
                                item.FormlyContentObj.push(obj);
                                $scope.PreviewSheme.push(obj);
                            } catch (e) { console.log(e); }
                        });
                    } else {
                        angular.forEach($scope.ListMetadataField, function (item) {
                            item.FormlyContentObj = [];
                            try {
                                var obj = FormlyService.DeSerializeJSON(item.FormlyContent);
                                item.FormlyContentObj.push(obj);
                            } catch (e) { console.log(e); }
                        });
                        // 3 lớp only
                        angular.forEach($scope.PreviewSheme, function (row) {
                            angular.forEach(row.fieldGroup, function (column) {
                                for (var i = 0; i < $scope.ListMetadataField.length; i++) {
                                    var field = $scope.ListMetadataField[i];
                                    if (Array.isArray(column.fieldGroup) && column.fieldGroup.length >= 1) {
                                        if (column.fieldGroup[0].template === "<loki-index-" + i + ">") {
                                            column.fieldGroup = [];
                                            column.fieldGroup.push(FormlyService.DeSerializeJSON(field.FormlyContent));
                                        }
                                    }
                                }
                            });
                        });
                    }

                });
            }
            //Lấy dữ liệu 
            var initMetadataFieldData = function (id) {
                $scope.Data = {};
                var promise = CatalogItemService.GetAtributeById(id);
                promise.then(function onSuccess(response) {
                    var data = response.data;
                    if (data.Status != null) {
                        for (var i = 0; i < data.Data.length; i++) {
                            var item = data.Data[i];

                            switch (item.DataType) {
                                case "NvarcharValue":
                                    {
                                        $scope.Data[item.Key] = item.NvarcharValue;
                                        break;
                                    }
                                case "VarcharValue":
                                    {
                                        $scope.Data[item.Key] = item.VarcharValue;
                                        break;
                                    }
                                case "BitValue":
                                    {
                                        $scope.Data[item.Key] = item.BitValue;
                                        break;
                                    }
                                case "IntValue":
                                    {
                                        $scope.Data[item.Key] = item.IntValue;
                                        break;
                                    }
                                case "DatetimeValue":
                                    {
                                        $scope.Data[item.Key] = item.DatetimeValue;
                                        $scope.Data[item.Key + '-TypeDateTime'] = item.TypeDateTime;
                                        break;
                                    }
                                case "TimeValue":
                                    {
                                        $scope.Data[item.Key] = item.TimeValue;
                                        break;
                                    }
                                case "DateValue":
                                    {
                                        $scope.Data[item.Key] = item.DateValue;
                                        $scope.Data[item.Key + '-TypeDateTime'] = item.TypeDateTime;
                                        break;
                                    }
                                case "MonthValue":
                                    {
                                        $scope.Data[item.Key] = item.MonthValue;
                                        break;
                                    }
                                case "YearValue":
                                    {
                                        $scope.Data[item.Key] = item.YearValue;
                                        break;
                                    }
                                case "GuidValue":
                                    {
                                        $scope.Data[item.Key] = item.GuidValue;
                                        break;
                                    }
                                case "JsonValue":
                                    {
                                        try {
                                            $scope.Data[item.Key] = angular.fromJson(item.JsonValue);
                                        } catch (e) {
                                            $scope.Data[item.Key] = [];
                                        }
                                        break;
                                    }

                                case "JsonGuidValue":
                                    {
                                        try {
                                            $scope.Data[item.Key] = angular.fromJson(item.JsonGuidValue);
                                        } catch (e) {
                                            $scope.Data[item.Key] = [];
                                        }
                                        break;
                                    }
                                default:
                                    break;
                            };
                        }
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            }

            var initFormMode = function () {
                $scope.Item = {};
                initMetadataField();
                var cmd = initListParrent();
                if (option.Mode === "add") {
                    $scope.FormMode = "Thêm mới";
                    $scope.Item = {};
                    $scope.Item.Status = 1;
                    $scope.Item.Order = 0;
                    if (typeof (option.Parent) != "undefined" && option.Parent != null) {
                        $scope.Item.ParentTermId = option.Parent.TermId;
                    }
                    $scope.CatalogName = option.Catalog.Name;
                } else {
                    $scope.FormMode = "Cập nhật";
                    $scope.Item = angular.copy(item);
                    if ($scope.Item.Status)
                        $scope.Item.Status = 1;
                    else
                        $scope.Item.Status = 0;
                    //Lấy data của item
                    initMetadataFieldData(item.CatalogItemId);
                    cmd.then(function () {
                        angular.forEach($scope.ListCatalogData, function (catalog) {
                            if (catalog.TermId === $scope.Item.MappedTermId) {
                                var index = $scope.ListCatalogData.indexOf(catalog);
                                $scope.ListCatalogData.splice(index, 1);
                            }
                        });
                    });
                }
            }

            initFormMode();
            $scope.Save = function () {
                $scope.MyForm.$setSubmitted();
                if ($scope.MyForm.$invalid) {
                    var deferred = $q.defer();
                    console.log("$scope.MyForm", $scope.MyForm);
                    angular.element("[name='" + $scope.MyForm.$name + "']").find('.ng-invalid:visible:first').focus();
                    $notifications.error("Vui lòng kiểm tra thông tin đã nhập");
                    deferred.resolve('');
                    return deferred.promise;
                }
                if (option.Mode === "add") {
                    debugger
                    var postData = $scope.Item;
                    postData.CatalogMasterId = option.Catalog.CatalogMasterId;
                    postData.DataAttribute = [];
                    for (var data in $scope.Data) {
                        if ($scope.Data.hasOwnProperty(data)) {
                            //alert("Key is " + k + ", value is" + target[k]);
                            var key = data;
                            var value = $scope.Data[data];
                            for (var i = 0; i < $scope.ListMetadataField.length; i++) {
                                var field = $scope.ListMetadataField[i];
                                if (field.FormlyContentObj[0].key === key) {
                                    var model = {
                                        "Key": key,
                                        "DataType": field.FormlyContentObj[0].data.dbType,
                                        "Value": value,
                                        "Name": field.Name,
                                        "MetadataFieldId": field.MetadataFieldId,
                                    };
                                    switch (model.DataType) {
                                        case "NvarcharValue":
                                            {
                                                model.NvarcharValue = value;
                                                break;
                                            }
                                        case "VarcharValue":
                                            {
                                                model.VarcharValue = value;
                                                break;
                                            }
                                        case "BitValue":
                                            {
                                                model.BitValue = value;
                                                break;
                                            }
                                        case "IntValue":
                                            {
                                                model.IntValue = value;
                                                break;
                                            }
                                        case "DatetimeValue":
                                            {
                                                model.DatetimeValue = value;

                                                break;
                                            }
                                        case "TimeValue":
                                            {
                                                model.TimeValue = value;
                                                break;
                                            }
                                        case "DateValue":
                                            {
                                                model.DateValue = value;
                                                //Check =))
                                                if (typeof ($scope.Data[data + "-TypeDateTime"]) != undefined && $scope.Data[data + "-TypeDateTime"] != null) {
                                                    model.TypeDateTime = $scope.Data[data + "-TypeDateTime"];
                                                }
                                                break;
                                            }
                                        case "MonthValue":
                                            {
                                                model.MonthValue = value;
                                                break;
                                            }
                                        case "YearValue":
                                            {
                                                model.YearValue = value;
                                                break;
                                            }
                                        case "GuidValue":
                                            {
                                                model.GuidValue = value;
                                                break;
                                            }
                                        case "JsonValue":
                                            {

                                                try {
                                                    model.JsonValue = angular.toJson(value);
                                                } catch (e) {
                                                    model.JsonValue = "[]";
                                                }
                                                break;
                                            }

                                        case "JsonGuidValue":
                                            {
                                                try {
                                                    model.JsonGuidValue = angular.toJson(value);
                                                } catch (e) {
                                                    model.JsonGuidValue = "[]";
                                                }

                                                break;
                                            }
                                        default:
                                            break;
                                    };
                                    postData.DataAttribute.push(model);
                                }
                            }
                        }
                    }
                    var promise = CatalogItemService.Create(postData);
                    promise.then(function onSuccess(response) {
                        data = response.data;
                        debugger
                        $log.debug(data)
                        if (data.Status === 1) {
                            console.log("Success", response);
                            $notifications.success("Tạo bản ghi danh mục thành công!");
                            $uibModalInstance.close(data.Data);
                        } else
                            if (data.Status === 0) {
                                $notifications.error("Tạo bản ghi danh mục thất bại!\n\n" + data.Message);
                                $('#txtName').focus();
                            }
                            else {
                                $notifications.error("Tạo bản ghi danh mục thất bại, kiểm tra lại dữ liệu nhập vào! Có thể bị trùng tên");
                                $('#txtName').focus();
                            }
                    }, function onError(response) {
                        $log.debug(response);
                        $notifications.error("Tạo bản ghi danh mục thất bại! Có thể bị trùng tên.");
                        $('#txtName').focus();
                    });
                } else {
                    var putData = {};
                    var putData = $scope.Item;
                    putData.CatalogMasterId = option.Catalog.CatalogMasterId;
                    putData.DataAttribute = [];
                    for (var data in $scope.Data) {
                        if ($scope.Data.hasOwnProperty(data)) {
                            //alert("Key is " + k + ", value is" + target[k]);
                            var key = data;
                            var value = $scope.Data[data];

                            for (var i = 0; i < $scope.ListMetadataField.length; i++) {
                                var field = $scope.ListMetadataField[i];
                                if (field.FormlyContentObj[0].key === key) {

                                    var model = {
                                        "Key": key,
                                        "DataType": field.FormlyContentObj[0].data.dbType,
                                        "Name": field.Name,
                                        "Value": (value === '' || value === null || value === undefined) ? '' : value.toString(),
                                        "MetadataFieldId": field.MetadataFieldId,
                                    };
                                    switch (model.DataType) {
                                        case "NvarcharValue":
                                            {
                                                model.NvarcharValue = value;
                                                break;
                                            }
                                        case "VarcharValue":
                                            {
                                                model.VarcharValue = value;
                                                break;
                                            }
                                        case "BitValue":
                                            {
                                                model.BitValue = value;
                                                break;
                                            }
                                        case "IntValue":
                                            {
                                                model.IntValue = value;
                                                break;
                                            }
                                        case "DatetimeValue":
                                            {
                                                model.DatetimeValue = value;

                                                break;
                                            }
                                        case "TimeValue":
                                            {
                                                model.TimeValue = value;
                                                break;
                                            }
                                        case "DateValue":
                                            {
                                                model.DateValue = value;
                                                //Check =))
                                                if (typeof ($scope.Data[data + "-TypeDateTime"]) != undefined && $scope.Data[data + "-TypeDateTime"] != null) {
                                                    model.TypeDateTime = $scope.Data[data + "-TypeDateTime"];
                                                }
                                                break;
                                            }
                                        case "MonthValue":
                                            {
                                                model.MonthValue = value;
                                                break;
                                            }
                                        case "YearValue":
                                            {
                                                model.YearValue = value;
                                                break;
                                            }
                                        case "GuidValue":
                                            {
                                                model.GuidValue = value;
                                                break;
                                            }
                                        case "JsonValue":
                                            {

                                                try {
                                                    model.JsonValue = angular.toJson(value);
                                                } catch (e) {
                                                    model.JsonValue = "[]";
                                                }
                                                break;
                                            }

                                        case "JsonGuidValue":
                                            {
                                                try {
                                                    model.JsonGuidValue = angular.toJson(value);
                                                } catch (e) {
                                                    model.JsonGuidValue = "[]";
                                                }

                                                break;
                                            }
                                        default:
                                            break;
                                    };
                                    putData.DataAttribute.push(model);
                                }
                            }
                        }
                    }
                    var promise = CatalogItemService.Update(item.CatalogItemId, putData);
                    promise.then(function onSuccess(response) {
                        debugger
                        data = response.data;
                        $log.debug(data)
                        if (data.Status === 1) {
                            $notifications.success("Cập nhật bản ghi danh mục thành công!");
                            $uibModalInstance.close(data.Data);
                        } else {
                            if (data.Status === 0)
                                $notifications.error("Cập nhật bản ghi danh mục thất bại!\n\n" + data.Message);
                            else
                                $notifications.error("Cập nhật bản ghi danh mục thất bại, kiểm tra lại dữ liệu nhập vào");
                        }
                    }, function onError(response) {
                        $log.debug(response);
                        $notifications.error("Cập nhật bản ghi danh mục thất bại! Lỗi không xác định. Có thể bị trùng tên.");
                    });
                }
            };
            $scope.Cancel = function () {
                $uibModalInstance.dismiss();
            };
            /* Validate Form*/
            $scope.validateForm = {
                validName: false
            };
            var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            var validName = function (name) {
                if (typeof name == 'undefined' || name == null || name.trim() === "") {
                    $scope.validateForm.validName = true;
                    return false;
                }
                //else {
                //    $scope.validateForm.validName = format.test(name);
                //    if (format.test(name)) {
                //        return false;
                //    } else {
                //        return true;
                //    }
                //}
            }
            $scope.ChangeName = function () {
                //check ky tu dac biet
                // validName($scope.Item.Name);
                if (option.Mode === "add")
                    $scope.Item.Code = UtilsService.RemoveVietNamSign($scope.Item.Name);
            };
        }
    ]);
}();