﻿//"use strict";
//define(["app",
//    //codemirror packages
//    "codemirror",
//    //add on
//    "codemirror/addon/hint/show-hint", "codemirror/addon/hint/html-hint", "codemirror/addon/hint/css-hint", "codemirror/addon/hint/javascript-hint",
//    "codemirror/addon/display/fullscreen",
//    "codemirror/addon/edit/closetag", "codemirror/addon/edit/matchtags",
//    //"codemirror/addon/edit/closebrackets", "codemirror/addon/edit/matchbrackets",
//    "codemirror/addon/fold/foldcode", "codemirror/addon/fold/foldgutter", "codemirror/addon/fold/brace-fold", "codemirror/addon/fold/xml-fold", "codemirror/addon/fold/comment-fold", "codemirror/addon/fold/indent-fold",
//    "codemirror/addon/dialog/dialog",
//    "codemirror/addon/mode/multiplex",
//    "codemirror/addon/search/search", "codemirror/addon/search/jump-to-line", "codemirror/addon/search/match-highlighter", "codemirror/addon/search/matchesonscrollbar", "codemirror/addon/search/searchcursor",
//    "codemirror/addon/lint/lint", "codemirror/addon/lint/css-lint", "codemirror/addon/lint/javascript-lint", "codemirror/addon/lint/json-lint",
//    //mode
//    "codemirror/mode/xml/xml", "codemirror/mode/htmlmixed/htmlmixed", "codemirror/mode/css/css", "codemirror/mode/htmlembedded/htmlembedded", "codemirror/mode/clike/clike",
//    //keymap
//    "codemirror/keymap/sublime",
//    //end codemirror packages

//    'components/service/amdservice',
//    'components/service/apiservice',
//    'components/formly-template/formly-factory',
!function () {
    'use strict';
    var app = angular.module("SavisApp");
    //, function (app, CodeMirror) {
    app.controller("MetadataFieldItemCtrl", ["$scope", "$log", "$uibModalInstance", "$uibModal", "item", "option", "$timeout", 'FormlyFactory', 'MetadataFieldService', 'toastr', 'UtilsService', 'FormlyService', 'CatalogService', 'CatalogItemService',
        function ($scope, $log, $uibModalInstance, $uibModal, item, option, $timeout, FormlyFactory, MetadataFieldService, $notifications, UtilsService, FormlyService, CatalogService, CatalogItemService) {
            /* Notification -----------------------------------------------------*/
            $scope.closeAlert = function (item) {
                Notifications.pop(item);
            };

            /*------------------------------------------------------------------**/
            $scope.ListAvailAbleDbType = FormlyFactory.ListAvailAbleDbType;
            $scope.ListAvailAbleControl = FormlyFactory.ListAvailAbleControl;
            $scope.ListAvailAbleClass = FormlyFactory.ListAvailAbleClass;

            /*-----------------------------------------------------------*/


            $scope.Mode = 1;
            $scope.Item = {};
            $scope.Data = {};
            $scope.PreviewSheme = [];
            $scope.PreviewSheme.push(angular.copy($scope.Item));
            $scope.PreviewData = {};

            var initCatalog = function () {
                var promise = CatalogService.GetAll();
                promise.then(function onSuccess(response) {
                    response = response.data;
                    console.log("SUCCESS", response);
                    if (response.Status === 1) {
                        $scope.ListAvailAbleCatalog = response.Data;
                        $scope.ListAvailAbleCatalog.unshift({ Name: "Không sử dụng danh mục", Code: "NONE" });
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                    $log.error();
                });
                return promise;
            }

            var initFormMode = function () {
                $scope.Item = {};
                if (option.Mode === "add") {
                    $scope.FormName = 'Thêm mới trường tin';
                    $scope.Item = angular.copy($scope.ListAvailAbleControl[0].Value);
                    $scope.Data = {};
                    $scope.Data.FieldWidth = 100;
                } else {
                    try {
                        $scope.FormName = 'Chỉnh sửa trường tin ' + item.Name;
                        $scope.Item = FormlyService.DeSerializeJSON(item.FormlyContent);
                        $scope.Data = {};
                        $scope.Data.Name = item.Name;
                        $scope.Data.Code = item.Code;
                        $scope.Data.Description = item.Description;
                        $scope.Data.IsDocumentCommonField = item.IsDocumentCommonField;
                        $scope.Data.IsRecordCommonField = item.IsRecordCommonField;
                        $scope.Data.DisplayCatalog = item.DisplayCatalog;
                        $scope.Data.DisplayReport = item.DisplayReport;
                        $scope.Data.DisplayReader = item.DisplayReader;
                        $scope.Data.DisplayReportSearch = item.DisplayReportSearch;
                        $scope.Data.DisplayReaderSearch = item.DisplayReaderSearch;
                        $scope.Data.FieldWidth = item.FieldWidth;
                    } catch (e) {

                    }
                }
            }

            $scope.$watch('Item', function () {
                $scope.Item.data.columnName = $scope.Item.key;
                $scope.Item.data.required = $scope.Item.templateOptions.required;
                $scope.Item.data.defaultValue = $scope.Item.defaultValue;
                $scope.PreviewSheme = [];
                $scope.PreviewSheme.push(angular.copy($scope.Item));
            }, true);
            initCatalog();
            initFormMode();
            $scope.ChangeAddonRight = function () {
                if ($scope.Item.templateOptions.addonRight.class == '' || $scope.Item.templateOptions.addonRight.class == null || typeof ($scope.Item.templateOptions.addonRight.class) == "undefined") {
                    delete $scope.Item.templateOptions.addonRight;
                }
            };
            $scope.ChangeAddonLeft = function () {
                if ($scope.Item.templateOptions.addonLeft.class == '' || $scope.Item.templateOptions.addonLeft.class == null || typeof ($scope.Item.templateOptions.addonLeft.class) == "undefined") {
                    delete $scope.Item.templateOptions.addonLeft;
                }
            };

            //if ($scope.Item.data.group == "input") {
            //var editorCustomValidate;
            //$timeout(function () {
            //    editorCustomValidate = CodeMirror.fromTextArea(document.getElementById("txtCustomValidate"), {
            //        lineNumbers: true,
            //        mode: 'text/javascript',
            //        viewportMargin: Infinity,
            //        lineWrapping: true,
            //        keyMap: "sublime",
            //        theme: "twilight",
            //        foldGutter: true,
            //        height: "500px",
            //        matchTags: {
            //            bothTags: true
            //        },
            //        autoCloseBrackets: true,
            //        autoCloseTags: true,
            //        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
            //        extraKeys: {
            //            "Ctrl-J": "toMatchingTag",
            //            "Ctrl-Space": "autocomplete",
            //            "Alt-F": "findPersistent",
            //            "F11": function (cm) {
            //                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            //            },
            //            "Esc": function (cm) {
            //                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            //            },
            //            "Ctrl-Q": function (cm) {
            //                cm.foldCode(cm.getCursor());
            //            }
            //        }
            //    });
            //    CodeMirror.modeURL = "codemirror/mode/%N/%N";

            //    editorCustomValidate.on("change", function (cm, change) {
            //        $scope.Item.templateOptions.customValidate = cm.getValue();
            //    });
            //});
            //var editorApiValidateCondition;
            //$timeout(function () {
            //    editorApiValidateCondition = CodeMirror.fromTextArea(document.getElementById("txtapiValidateCondition"), {
            //        lineNumbers: true,
            //        mode: 'text/javascript',
            //        viewportMargin: Infinity,
            //        lineWrapping: true,
            //        keyMap: "sublime",
            //        theme: "twilight",
            //        foldGutter: true,
            //        height: "500px",
            //        matchTags: {
            //            bothTags: true
            //        },
            //        //autoCloseBrackets: true,
            //        autoCloseTags: true,
            //        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
            //        extraKeys: {
            //            "Ctrl-J": "toMatchingTag",
            //            "Ctrl-Space": "autocomplete",
            //            "Alt-F": "findPersistent",
            //            "F11": function (cm) {
            //                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            //            },
            //            "Esc": function (cm) {
            //                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            //            },
            //            "Ctrl-Q": function (cm) {
            //                cm.foldCode(cm.getCursor());
            //            }
            //        }
            //    });
            //    CodeMirror.modeURL = "codemirror/mode/%N/%N";

            //    editorApiValidateCondition.on("change", function (cm, change) {
            //        $scope.Item.templateOptions.apiValidateCondition = cm.getValue();
            //    });
            //});
            ////}

            //if ($scope.Item.data.group == "checkbox" || $scope.Item.data.group == "dropdown") {
            $scope.DeleteOptions = function (options) {
                var index = $scope.Item.templateOptions.options.indexOf(options);

                if (index >= 0) {
                    $scope.Item.templateOptions.options.splice(index, 1);
                }
            };
            $scope.AddOptions = function (index) {
                if (typeof (index) == "undefined") {
                    index = -1;
                }
                $scope.Item.templateOptions.options.splice(index + 1, 0, {});
            };
            //}

            //var editorHideExpression;
            //$timeout(function () {
            //    editorHideExpression = CodeMirror.fromTextArea(document.getElementById("txtHideExpression"), {
            //        lineNumbers: true,
            //        mode: 'text/javascript',
            //        viewportMargin: Infinity,
            //        lineWrapping: true,
            //        keyMap: "sublime",
            //        theme: "twilight",
            //        foldGutter: true,
            //        height: "500px",
            //        matchTags: {
            //            bothTags: true
            //        },
            //        //autoCloseBrackets: true,
            //        autoCloseTags: true,
            //        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
            //        extraKeys: {
            //            "Ctrl-J": "toMatchingTag",
            //            "Ctrl-Space": "autocomplete",
            //            "Alt-F": "findPersistent",
            //            "F11": function (cm) {
            //                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            //            },
            //            "Esc": function (cm) {
            //                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            //            },
            //            "Ctrl-Q": function (cm) {
            //                cm.foldCode(cm.getCursor());
            //            }
            //        }
            //    });
            //    CodeMirror.modeURL = "codemirror/mode/%N/%N";

            //    editorHideExpression.on("change", function (cm, change) {
            //        $scope.Item.templateOptions.hideExpression = cm.getValue();
            //    });
            //});

            //var editorDisabledExpression;
            //$timeout(function () {
            //    editorDisabledExpression = CodeMirror.fromTextArea(document.getElementById("txtDisabledExpression"), {
            //        lineNumbers: true,
            //        mode: 'text/javascript',
            //        viewportMargin: Infinity,
            //        lineWrapping: true,
            //        keyMap: "sublime",
            //        theme: "twilight",
            //        foldGutter: true,
            //        height: "500px",
            //        matchTags: {
            //            bothTags: true
            //        },
            //        //autoCloseBrackets: true,
            //        autoCloseTags: true,
            //        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"],
            //        extraKeys: {
            //            "Ctrl-J": "toMatchingTag",
            //            "Ctrl-Space": "autocomplete",
            //            "Alt-F": "findPersistent",
            //            "F11": function (cm) {
            //                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            //            },
            //            "Esc": function (cm) {
            //                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            //            },
            //            "Ctrl-Q": function (cm) {
            //                cm.foldCode(cm.getCursor());
            //            }
            //        }
            //    });
            //    CodeMirror.modeURL = "codemirror/mode/%N/%N";

            //    editorDisabledExpression.on("change", function (cm, change) {
            //        $scope.Item.templateOptions.disabledExpression = cm.getValue();
            //    });
            //});


            $scope.ChangeControl = function () {
                for (var i = 0; i < $scope.ListAvailAbleControl.length; i++) {
                    var control = $scope.ListAvailAbleControl[i];
                    if (control.Name == $scope.Item.data.key) {
                        var obj = angular.copy(control.Value);
                        if (typeof ($scope.Item.data) === "undefined") {
                            $scope.Item.data = {};
                        }
                        if (typeof ($scope.Item.templateOptions) === "undefined") {
                            $scope.Item.templateOptions = {};
                        }

                        obj.key = $scope.Item.key;

                        obj.data.key = $scope.Item.data.key;
                        obj.templateOptions.required = $scope.Item.templateOptions.required;
                        obj.templateOptions.disabled = $scope.Item.templateOptions.disabled;
                        obj.templateOptions.label = $scope.Item.templateOptions.label;
                        //obj.templateOptions.placeholder = $scope.Item.templateOptions.placeholder;
                        obj.templateOptions.horizontalLabel = $scope.Item.templateOptions.horizontalLabel;
                        obj.templateOptions.labelSize = $scope.Item.templateOptions.labelSize;
                        obj.templateOptions.controlSize = $scope.Item.templateOptions.controlSize;
                        obj.templateOptions.isUseHideExpression = $scope.Item.templateOptions.isUseHideExpression;
                        obj.templateOptions.hideExpression = $scope.Item.templateOptions.hideExpression;
                        obj.templateOptions.isUseDisabledExpression = $scope.Item.templateOptions.isUseDisabledExpression;
                        obj.templateOptions.disabledExpression = $scope.Item.templateOptions.disabledExpression;

                        $scope.Item = obj;
                    }
                }
            };
            $scope.ChangeDbType = function (dbtype) {
                $scope.Item.data.dbtypeName = dbtype.Name;


            };
            $scope.ChangeDateMode = function (mode) {
                if (mode == 0) {
                    $scope.Item.templateOptions.datepickerOptions = {
                        "format": "dd/MM/yyyy",
                        'datepickerMode': "'day'",
                        'minMode': 'day'
                    }
                } else if (mode == 1) {
                    $scope.Item.templateOptions.datepickerOptions = {
                        "format": "MM/yyyy",
                        'datepickerMode': "'month'",
                        'minMode': 'month'
                    }
                } else if (mode == 2) {
                    $scope.Item.templateOptions.datepickerOptions = {
                        "format": "yyyy",
                        'datepickerMode': "'year'",
                        'minMode': 'year'
                    }
                }
            }
            $scope.ChangeCatalog = function () {
                if ($scope.Item.data.CatalogMasterCode == "NONE") {
                    $scope.Item.templateOptions.isGetOptionByApi = false;
                    if ($scope.Item.data.key == 'SelectMultiControl' || $scope.Item.data.key == 'CheckboxMultiControl' || $scope.Item.data.key == 'CheckboxMultiTreeControl') {
                        $scope.Item.data.dbType = "JsonValue";
                        $scope.Item.data.dbtypeName = "kiểu danh sách";
                    } else {
                        $scope.Item.data.dbType = "NvarcharValue";
                        $scope.Item.data.dbtypeName = "kiểu chữ";
                    }
                } else {
                    console.log($scope.Item.data.catalog);
                    $scope.Data.CatalogMasterCode = $scope.Item.data.CatalogMasterCode;
                    //Setting sử dụng api
                    $scope.Item.templateOptions.isGetOptionByApi = true;
                    //Gán API link
                    if ($scope.Item.data.key == 'SelectTreeControl'
                        ||
                        $scope.Item.data.key == 'CheckboxMultiTreeControl'
                    ) {
                        $scope.Item.templateOptions.apiGetOption = CatalogService.GetApiLoadItemTreeSrc($scope.Item.data.CatalogMasterCode);
                    } else {
                        $scope.Item.templateOptions.apiGetOption = CatalogService.GetApiLoadItemListSrc($scope.Item.data.CatalogMasterCode);
                    }
                    //Gán dữ liệu sử dụng
                    $scope.Item.templateOptions.apiResponseData = "$response.data.Data";//???

                    $scope.Item.templateOptions.valueProp = "CatalogItemId";
                    $scope.Item.templateOptions.labelProp = "Name";
                    if ($scope.Item.data.key == 'SelectMultiControl' || $scope.Item.data.key == 'CheckboxMultiControl' || $scope.Item.data.key == 'CheckboxMultiTreeControl') {
                        $scope.Item.data.dbType = "JsonGuidValue";
                        $scope.Item.data.dbtypeName = "Kiểu danh sách danh mục";
                    } else {
                        $scope.Item.data.dbType = "GuidValue";
                        $scope.Item.data.dbtypeName = "Kiểu danh mục";
                    }
                }
            };
            //todo: hiện không có hàm RemoveVietNamSign nên tạm thời đóng lại
            //$scope.ChangeFileName = function () {
            //    $scope.Item.templateOptions.label = $scope.Data.Name;
            //    $scope.Data.Code = UtilsService.RemoveVietNamSign($scope.Data.Name);
            //    $scope.Item.key = $scope.Data.Code;
            //    $scope.Item.data.FieldName = $scope.Data.Name;
            //};

            $scope.Save = function () {
                $scope.MyForm.$setSubmitted();
                if ($scope.MyForm.$invalid) {
                    angular.element("[name='" + $scope.MyForm.$name + "']").find('.ng-invalid:visible:first').focus();
                    return false;
                    $notifications.error("Vui lòng kiểm tra thông tin đã nhập");
                    return;
                }
                $scope.Item.data.FieldName = $scope.Data.Name;
                $scope.Item.key = $scope.Data.Code;

                if ($scope.Item.data.dbType !== "uniqueidentifier") {
                    $scope.Data.CatalogMasterId = null;
                } else {
                    $scope.Data.CatalogMasterId = $scope.Item.data.CatalogMasterId;
                }


                var sheme = FormlyService.SerializeJSON($scope.Item)
                //   console.log(sheme);
                if (option.Mode === "add") {
                    var postData = {};
                    postData.FormlyContent = sheme;
                    postData.Name = $scope.Data.Name;
                    postData.Code = $scope.Data.Code;
                    postData.Description = $scope.Data.Description;
                    postData.CatalogMasterId = $scope.Data.CatalogMasterId;

                    postData.DisplayCatalog = $scope.Data.DisplayCatalog;
                    postData.DisplayReport = $scope.Data.DisplayReport;
                    postData.DisplayReader = $scope.Data.DisplayReader;
                    postData.DisplayReportSearch = $scope.Data.DisplayReportSearch;
                    postData.DisplayReaderSearch = $scope.Data.DisplayReaderSearch;
                    postData.CatalogCode = $scope.Item.data.CatalogMasterCode;
                    postData.DataType = $scope.Item.data.dbType;
                    postData.FieldWidth = $scope.Data.FieldWidth;
                    if ($scope.Data.IsDocumentCommonField) { postData.IsDocumentCommonField = true; }
                    if ($scope.Data.IsRecordCommonField) { postData.IsRecordCommonField = true; }
                    var promise = MetadataFieldService.Create(postData);
                    promise.then(function onSuccess(response) {
                        response = response.data;
                        console.log("SUCCESS", response);
                        if (response.Status === 1) {
                            $notifications.success("Tạo field thành công!");
                            $uibModalInstance.close(response.Data);
                        } else {
                            $notifications.error("Tạo field thất bại!" + response.Message);
                        }
                    }, function onError(response) {
                        $log.debug(response);
                        $notifications.error("Tạo field thất bại! Lỗi server");
                        console.log("ERROR", response);
                    });
                } else {
                    var putData = {};
                    putData.FormlyContent = sheme;
                    putData.Name = $scope.Data.Name;
                    putData.Code = $scope.Data.Code;
                    putData.Description = $scope.Data.Description;
                    putData.CatalogMasterId = $scope.Data.CatalogMasterId;
                    putData.DisplayCatalog = $scope.Data.DisplayCatalog;
                    putData.DisplayReport = $scope.Data.DisplayReport;
                    putData.DisplayReader = $scope.Data.DisplayReader;
                    putData.DisplayReportSearch = $scope.Data.DisplayReportSearch;
                    putData.DisplayReaderSearch = $scope.Data.DisplayReaderSearch;
                    putData.CatalogCode = $scope.Item.data.CatalogMasterCode;
                    putData.DataType = $scope.Item.data.dbType;
                    putData.FieldWidth = $scope.Data.FieldWidth;
                    if ($scope.Data.IsDocumentCommonField) { putData.IsDocumentCommonField = true; }
                    if ($scope.Data.IsRecordCommonField) { putData.IsRecordCommonField = true; }
                    var promise = MetadataFieldService.Update(item.MetadataFieldId, putData);
                    promise.then(function onSuccess(response) {
                       var data = response.data;
                        $log.debug(data)
                        if (data.Status === 1) {
                            $notifications.success("Cập nhật field thành công!");
                            $uibModalInstance.close(data.Data);
                        } else {
                            $notifications.error("Cập nhật field thất bại!" + data.Message);
                        }
                    }, function onError(response) {
                        $log.debug(response);
                        $notifications.error("Cập nhật field thất bại! Lỗi server");
                        console.log("ERROR", response);
                    });
                }
            };
            $scope.Cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
}();