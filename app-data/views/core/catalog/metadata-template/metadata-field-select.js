﻿//define(['app', 'jquery', 'slimscroll',
//    'components/factory/factory',
//    'components/service/apiservice',
//    'components/service/amdservice',
//    'angular-sanitize',
//    'components/formly-template/formly-factory',
//], function (app, $) {
define(function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('MetadataFieldSelectCtrl', ['$scope', '$rootScope', '$filter', '$sce', '$uibModal', '$timeout', '$location', '$routeParams', '$uibModalInstance',
        '$log', 'ConstantsApp', 'UtilsService', 'toastr', 'MetadataFieldService', 'FormlyFactory', 'FormlyService',
        function ($scope, $rootScope, $filter, $sce, $uibModal, $timeout, $location, $routeParams, $uibModalInstance,
            $log, ConstantsApp, UtilsService, $notifications, MetadataFieldService, FormlyFactory, FormlyService) {
            /* Notification */
            $scope.closeAlert = function (item) {
                $notifications.pop(item);
            };

            $scope.ListSelected = [];
            $scope.IsSelectAll = false;
            /* Header grid datatable */
            $scope.Headers = [
                { Key: 'Name', Value: "Thông tin chung", Width: 'auto' },
                { Key: 'Number', Value: "Xem trước", Width: 'auto' },
            ];
            $scope.ListPageSize = [5, 10, 15, 20, 25, 30, 35];
            $scope.Pagination = {};
            $scope.Pagination.PageSize = 5;
            $scope.Pagination.PageNumber = 1;
            $scope.ListData = [];
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.order = function (predicate, reverse) {
                $scope.ListData = angular.copy(orderBy($scope.ListData, predicate, reverse));
            };

            var loadData = function () {
                $scope.ListSelected = [];
                var qs = $location.search();

                var postData = {};
                if ($scope.Pagination.PageNumber <= 0) {
                    $scope.Pagination.PageNumber = 1;
                }
                postData.PageNumber = $scope.Pagination.PageNumber;
                if ($scope.Pagination.PageSize <= 0) {
                    $scope.Pagination.PageSize = 10;
                }
                postData.PageSize = $scope.Pagination.PageSize;
                postData.TextSearch = $scope.TextSearch;
                postData.Condition = $scope.Condition;
                postData.IsRecordCommonField = $scope.IsRecordCommonField;
                postData.IsDocumentCommonField = $scope.IsDocumentCommonField;
                postData.DisplayCatalog = $scope.DisplayCatalog;
                postData.DisplayReport = $scope.DisplayReport;
                postData.DisplayReader = $scope.DisplayReader;
                postData.DisplayReportSearch = $scope.DisplayReportSearch;
                postData.DisplayReaderSearch = $scope.DisplayReaderSearch;
                var promise = MetadataFieldService.GetFilter(postData);
                promise.then(function onSuccess(response) {
                    console.log(response);
                    response = response.data;
                    $scope.ListData = [];
                    $scope.Pagination.TotalCount = 0;
                    $scope.Pagination.TotalPage = 0;
                    $scope.Pagination.FromRecord = 0;
                    $scope.Pagination.ToRecord = 0;
                    if (response.Status === 1) {
                        //$scope.sucess("Tải dữ liệu thành công !");
                        $scope.ListData = response.Data;
                        angular.forEach($scope.ListData, function (item) {
                            item.FormlyContentObj = [];
                            try {
                                var obj = FormlyService.DeSerializeJSON(item.FormlyContent);
                                item.FormlyContentObj.push(obj);
                            } catch (e) { console.log(e); }
                        });
                        $scope.Pagination.TotalCount = response.TotalCount;
                        $scope.Pagination.TotalPage = Math.ceil($scope.Pagination.TotalCount / $scope.Pagination.PageSize);
                        $scope.Pagination.FromRecord = 0;
                        $scope.Pagination.ToRecord = 0;
                        if ($scope.Pagination.TotalCount !== 0) {
                            $scope.Pagination.FromRecord = Math.ceil(($scope.Pagination.PageNumber - 1) * $scope.Pagination.PageSize + 1);
                            $scope.Pagination.ToRecord = $scope.Pagination.FromRecord + $scope.Pagination.PageSize - 1;
                            if ($scope.Pagination.ToRecord > $scope.Pagination.TotalCount) {
                                $scope.Pagination.ToRecord = $scope.Pagination.TotalCount;
                            }
                        }
                    } else if (response.Status == 0) {
                        $notifications.warning("Không có dữ liệu!");
                    } else {
                        $notifications.error("Không tải được dữ liệu:" + response.Message);
                    }
                    //$timeout(function () {
                    //    $("#table-region").slimScroll({
                    //        height: "470px",
                    //    });

                    //});
                }, function onError(reasonResponse) {
                    console.log(reasonResponse);
                    $notifications.error("Không tải được dữ liệu:" + reasonResponse);
                });
                return promise;
            };
            loadData();
            $scope.ChangeStatus = function (data) {
                data.Status = !data.Status;
            }
            $scope.SelectItem = function (item) {
                if (!item.Selecting) {
                    var index = $scope.ListSelected.indexOf(item);
                    if (index >= 0) {
                        $scope.ListSelected.splice(index, 1);
                    }
                } else {
                    $scope.ListSelected.push(item);
                }
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.IsSelectAll = true;
                } else {
                    $scope.IsSelectAll = false;
                }
            }

            $scope.SelectAllItem = function () {
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = false;
                    });
                    $scope.IsSelectAll = false;
                } else {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = true;
                        $scope.ListSelected.push(file);
                    });
                    $scope.IsSelectAll = true;
                }
            }

            $scope.GetItemHeaderClass = function (item) {
                if (item.selected == true) {
                    return 'table-sort-asc';
                } else if (item.selected == false) {
                    return 'table-sort-desc';
                } else {
                    return 'table-sort-both';
                };
            };

            $scope.ClickToHeader = function (item) {
                angular.forEach($scope.Headers, function (items) {
                    if (items != item) {
                        items.selected = null;
                    };
                });
                if (item.selected == true) {
                    item.selected = false;
                    $scope.order(item.Key, false);
                } else {
                    item.selected = true;
                    $scope.order("-" + item.Key, false);
                };
            };

            $scope.Button = {};
            $scope.Button.Update = {};
            $scope.Button.Update.Click = function (item) {
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: addMetadataFieldUrl,
                    controller: 'MetadataFieldItemCtrl',
                    size: 'lg',
                    //windowClass: "modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return item;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "update";
                            return obj
                        },
                    }
                });

                modalInstance.result.then(function (field) {
                    try {
                        field.FormlyContentObj = [];
                        var obj = FormlyService.DeSerializeJSON(field.FormlyContent);
                        field.FormlyContentObj.push(obj);
                    } catch (e) { console.log(e); }
                    item = angular.extend(item, field);
                }, function (response) { });
            };
            $scope.Button.Delete = {};
            $scope.Button.Delete.Click = function (item) {
                var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa thông tin này!', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = MetadataFieldService.Delete(item.MetadataFieldId);
                        promise.then(function onSuccess(response) {
                            console.log(response);
                            response = response.data;
                            loadData();
                            if (response.Status === 1) {
                                if (response.Data.Result) {
                                    $notifications.success("Xóa " + item.Name + " thành công!");
                                } else {
                                    $notifications.error("Xóa " + item.Name + " thất bại: " + response.Data.Message);
                                }
                            } else {
                                $notifications.error("Xóa " + item.Name + " thất bại: " + response.Message);
                            }
                        }, function onError(response) {
                            console.log(response);
                            $notifications.error("Xóa " + item.Name + " thất bại: " + response);
                        });
                    };
                });
            };
            $scope.Button.DeleteMulti = {};
            $scope.Button.DeleteMulti.Click = function () {
                var listDeleteting = [];
                angular.forEach($scope.ListData, function (item) {

                    if (item.Selecting) {
                        listDeleteting.push(item.MetadataFieldId);
                    }
                });

                var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa những thông tin này!', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = MetadataFieldService.DeleteMany(listDeleteting);
                        promise.then(function onSuccess(response) {
                            response = response.data;
                            console.log(response);
                            loadData();
                            if (response.Status === 1) {
                                var listDeleteField = [];
                                for (var i = 0; i < response.Data.length; i++) {
                                    var item = response.Data[i];
                                    if (item.Result == true) {
                                        listDeleteField.push({
                                            Name: item.Model.Name,
                                            Message: "Xóa thành công",
                                            Result: true
                                        });
                                    } else if (item.Result == false) {
                                        listDeleteField.push({
                                            Name: item.Model.Name,
                                            Message: item.Message,
                                            Result: false
                                        });
                                    };
                                }
                                var data = {};
                                data = listDeleteField;
                                var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Xác nhận', '', 'Hủy', 'md', data, null);
                            } else {
                                $notifications.error("Xóa " + item.Name + " thất bại: " + response.Message);
                            }
                        }, function onError(response) {
                            console.log(response);
                        });
                    };

                });
            };

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                //$scope.IsNotReload =true; $location.search("s", "");
                //$scope.IsNotReload =true; $location.search("as", "");
                //$scope.IsNotReload =true; $location.search("pn", "1");
                $scope.PageNumber = 1;
                $scope.PageSize = 5;
                $scope.TextSearch = "";
                $scope.IsRecordCommonField = false;
                $scope.IsDocumentCommonField = false;
                $scope.DisplayCatalog = false;
                $scope.DisplayReport = false;
                $scope.DisplayReader = false;
                $scope.DisplayReportSearch = false;
                $scope.DisplayReaderSearch = false;
                var cmd = loadData();
                cmd.then(function () {
                    $notifications.success("Tải lại thành công!");
                });
            };


            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $scope.Pagination.PageNumber = pageTogo;
                $scope.Pagination.PageSize = $scope.Pagination.PageSize;
                loadData();
            };

            $scope.Button.Search = {};
            $scope.Button.Search.Click = function () {

                var cmd = loadData();
                cmd.then(function () {
                    $notifications.success("Tìm kiếm thành công!");
                });
            };

            $scope.Button.ResetSearch = {};
            $scope.Button.ResetSearch.Click = function () {
                $scope.TextSearch = "";
                $scope.IsRecordCommonField = false;
                $scope.IsDocumentCommonField = false;
                $scope.DisplayCatalogog = false;
                $scope.DisplayReport = false;
                $scope.DisplayReader = false;
                $scope.DisplayReportSearchReportSearch = false;
                $scope.DisplayReaderSearchReaderSearch = false;

                $scope.Button.Search.Click();
            };
            $scope.Save = function () {
                $uibModalInstance.close($scope.ListSelected);
            };
            $scope.Cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
}());

