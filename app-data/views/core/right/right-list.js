﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('RightController', ['$scope', '$location', '$filter', '$http', '$q', 'ConstantsApp', 'UtilsService', 'toastr', 'RightApiService',
        function ($scope, $location, $filter, $http, $q, ConstantsApp, UtilsService, $notifications, RightApiService) {


            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/core/role/modal-item.html';

            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            /* ------------------------------------------------------------------------------- */

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.Form.CurrentUserId = currentUserId;
            $scope.Form.Item = {};

            $scope.FormMode = {};
            $scope.FormMode.IsEditMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            // Button DETAIL
            $scope.Button.Save = {};
            $scope.Button.Save.Click = function () {
                if ($scope.DropDownList.Group.Model === null || typeof $scope.DropDownList.Group.Model === "undefined") {
                    $notifications.warning("Bạn chưa chọn chức năng thiết lập quyền", "Chú ý");
                    return;
                }
                else $scope.Form.Item.GroupCode = $scope.DropDownList.Group.Model;

                if (!$scope.FormMode.IsEditMode) {
                    var promise = RightApiService.Add($scope.Form.Item);
                    promise.then(function onSuccess(response) {
                        var responseResult = response.data;
                        if (responseResult.Status === 1) {
                            $scope.Button.Reload.Click();
                            $notifications.success("Thêm mới thành công!");
                        } else $notifications.error(responseResult.Message, "Thêm mới thất bại!");
                    }, function onError(response) {
                        console.log("ERROR", response);
                    });
                } else {
                    promise = RightApiService.Update($scope.Form.Item);
                    promise.then(function onSuccess(response) {
                        var responseResult = response.data;
                        if (responseResult.Status === 1) {
                            $scope.Button.Reload.Click();
                            $notifications.success("Cập nhật thành công!");
                        } else $notifications.error("Cập nhật thất bại!");
                    }, function onError(response) {
                        console.log("ERROR", response);
                    });
                }

            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function () {
                var rightCode = '';
                if ($scope.Form.Item !== null && typeof $scope.Form.Item !== 'undefined') {
                    rightCode = $scope.Form.Item.RightCode;
                }
                var message = "Bạn có chắc muốn xóa quyền này?";
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = RightApiService.Delete(rightCode);

                        promise.then(function (response) {
                            var responseResult = response.data;
                            if (responseResult !== null) {
                                listDeleteField.push({ Name: responseResult.RightCode, Result: responseResult.Result, Message: responseResult.Message });
                            }
                            $scope.Button.Reload.Click();
                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.Form.Item = { Status: true, Order: 0 };
                $scope.DropDownList.Group.Model = null;
                $scope.FormMode.IsEditMode = false;
                initMain();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};
            $scope.DropDownList.Group = { Data: [], Model: null };
            $scope.DropDownList.Group.SelectedChange = function () {
                
                if (!$scope.FormMode.IsEditMode) {
                    var groupCode = $scope.DropDownList.Group.Model;
                    $scope.Form.Item.RightCode = groupCode + "-";
                }
            };
            /* ------------------------------------------------------------------------------- */

            /* TREE VIEWS */
            /* ------------------------------------------------------------------------------- */
            $scope.TreeView = {};

            $scope.TreeView.Right = {};
            $scope.TreeView.Right = { FilterText: '', Data: [] };
            $scope.TreeView.Right.Collapse = function (right) {
                right.IsCollapse = !right.IsCollapse;
            };
            $scope.TreeView.Right.Click = function (right) {
                if (!right.IsGroup) {
                    $scope.FormMode.IsEditMode = true;
                    right.Selected = true;
                    $scope.Form.Item = angular.copy(right);
                    if ($scope.Form.Item.GroupCode !== null && typeof $scope.Form.Item.GroupCode !== "undefined") $scope.DropDownList.Group.Model = $scope.Form.Item.GroupCode;

                } else right.IsCollapse = !right.IsCollapse;

            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("RIGHT-DELETE");
                return true;
            };

            // Init nhóm chức năng
            var initGroup = function () {
                var promise = RightApiService.GetAllGroups()
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.DropDownList.Group.Data = responseResult.Data;
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            };

            // Init cây phân quyền
            var initTree = function () {
                var promise = RightApiService.GetTree();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.TreeView.Right.Data = responseResult.Data;
                        angular.forEach($scope.TreeView.Right.Data, function (group) { group.IsCollapse = true; })
                        console.log("SUCCEEDED", $scope.TreeView.Right.Data);
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            };

            var initMain = function () {
                $q.all([initGroup(), initTree()]).then(function () {
                    initButtonByRightOfUser();
                    $('#catalog-tree2').slimScroll({
                        height: '60vh'
                    });
                });
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }]);
}();

