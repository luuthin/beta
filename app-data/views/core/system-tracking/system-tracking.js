﻿
!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('SystemTrackingController', ['$scope', '$q', '$sce', '$filter', '$uibModal', '$timeout', '$location', '$routeParams', '$log', 'UtilsService', 'ConstantsApp', 'toastr', 'SystemTrackingApiService', 'PermissionApiService',
        function ($scope, $q, $sce, $filter, $uibModal, $timeout, $location, $routeParams, $log, UtilsService, ConstantsApp, $notifications, SystemTrackingApiService, PermissionApiService) {

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.Form.ActionStatus = ConstantsApp.SYSTEM_TRACKING_ACTION;

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;
            $scope.FormMode.BindHtml = function (htmlContent) {
                return $sce.trustAsHtml(htmlContent);
            };

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [
                { Key: 'STT', Value: "STT", Width: '3%', IsSortable: false },
                { Key: 'FullName', Value: "Người dùng", Width: '15%', IsSortable: false },
                { Key: 'LoggedOnDate', Value: "Thời gian", Width: '15%', IsSortable: true },
                { Key: 'Object', Value: "Đối tượng thao tác", Width: '15%', IsSortable: false },
                { Key: 'Action', Value: "Hành động", Width: '10%', IsSortable: true },
                { Key: 'Reference', Value: "Tham chiếu", Width: '20%', IsSortable: false },
                { Key: 'Description', Value: "Mô tả", Width: 'auto', IsSortable: false },
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* ------------------------------------------------------------------------------- */


            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            $scope.AdvancedFilter = {
                Visible: false,
                Model: {
                    UserId: '',
                    PageNumber: 0,
                    PageSize: 0,
                    TextSearch: '',
                    FromDate: null,
                    ToDate: null,
                    Action: [],
                }
            };

            /* ------------------------------------------------------------------------------- */


            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            $scope.DropDownList.ActionTracking = { Data: ConstantsApp.SYSTEM_TRACKING_ACTION, DataSelected: [] };

            $scope.DropDownList.User = { Data: [] };

            $scope.DropDownList.DateOption = {
                Data: [
                    { Code: 'other', Name: 'Khác' },
                    { Code: 'today', Name: 'Hôm nay' },
                    { Code: 'yesterday', Name: 'Hôm qua' },
                ],
                Model: null,
            };
            $scope.DropDownList.DateOption.SelectedChange = function (option, type) {
                if (option !== null && typeof option !== "undefined" && option === "other") $scope.DropDownList.DateOption.Model = $scope.DropDownList.DateOption.Data[0];
                if ($scope.DropDownList.DateOption.Model !== null && typeof $scope.DropDownList.DateOption.Model !== "undefined") {
                    var value = $scope.DropDownList.DateOption.Model.Code;
                    switch (value) {
                        case 'today':
                            $scope.AdvancedFilter.Model.FromDate = new Date(UtilsService.AddDays(new Date(), 0, false, true));
                            $scope.AdvancedFilter.Model.ToDate = new Date(UtilsService.AddDays(new Date(), 0, false, false));
                            break;
                        case 'yesterday':
                            $scope.AdvancedFilter.Model.FromDate = new Date(UtilsService.AddDays(new Date(), -1, false, true));
                            $scope.AdvancedFilter.Model.ToDate = new Date(UtilsService.AddDays(new Date(), -1, false, false));
                            break;
                        case 'other':
                            if (type === 'fromdate') {
                                $scope.AdvancedFilter.Model.FromDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.FromDate, 0, false, true));
                                if ($scope.AdvancedFilter.Model.ToDate !== null && typeof $scope.AdvancedFilter.Model.ToDate !== "undefined")
                                    $scope.AdvancedFilter.Model.ToDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.ToDate, 0, false, false));
                                else
                                    $scope.AdvancedFilter.Model.ToDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.FromDate, 0, false, false));
                            } else if (type === 'todate') {
                                $scope.AdvancedFilter.Model.ToDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.ToDate, 0, false, false));
                                if ($scope.AdvancedFilter.Model.FromDate !== null && typeof $scope.AdvancedFilter.Model.FromDate !== "undefined")
                                    $scope.AdvancedFilter.Model.FromDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.FromDate, 0, false, true));
                                else
                                    $scope.AdvancedFilter.Model.FromDate = new Date(UtilsService.AddDays($scope.AdvancedFilter.Model.ToDate, 0, false, true));
                            }
                            break;
                        default: break;
                    }
                }
            };
            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            $scope.Button.ResetAdvancedFilter = {};
            $scope.Button.ResetAdvancedFilter.Click = function () {
                $scope.AdvancedFilter.Model.UserId = '';
                $scope.AdvancedFilter.Model.FromDate = $scope.AdvancedFilter.Model.ToDate = null;
                $scope.DropDownList.DateOption.Model = null;
                $scope.DropDownList.ActionTracking.DataSelected = [];
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
            };

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.Button.ResetAdvancedFilter.Click();
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            var initUsers = function () {
                var promise = PermissionApiService.GetAllUser();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.DropDownList.User.Data = responseResult.Data;
                    }
                });
                return promise;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                /*Init filter condition*/
                $scope.AdvancedFilter.Model.Action = [];
                if (typeof $scope.DropDownList.ActionTracking.DataSelected !== "undefined" && $scope.DropDownList.ActionTracking.DataSelected.length > 0) {
                    angular.forEach($scope.DropDownList.ActionTracking.DataSelected, function (item) {
                        $scope.AdvancedFilter.Model.Action.push(item.Action);
                    });
                };

                var postData = $scope.AdvancedFilter.Model;
                angular.forEach($scope.Grid.Data, function (items) {
                    items.selected = null;
                });

                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    // 
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    // 
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch-----*/
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    // 
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                // Get data from api
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;
                postData.TrackingZone = "admin";
                var p = SystemTrackingApiService.GetFilter(postData);
                p.then(function onSuccess(response) {
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;

                    //Enable next button
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };

            var initMain = function () {
                $q.all([initUsers()]).then(function () {
                    initData();
                });
            };

            initMain();
            /* ------------------------------------------------------------------------------- */
        }
    ]);
}();


