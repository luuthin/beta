﻿--- File changelog dùng để ghi lại quá trình thêm các gói thư viện vào dự án của các thành viên
--- Mục đích để quản lý việc phát sinh các gói thư viện có hợp lý hay không? Tránh phát sinh dư thừa tài nguyên.
--- Cấu trúc 1 changelog như sau:

<!-- BEGIN CHANGELOG -->
- author: duongpd => username người thêm thư viện
- importedOnDate: 01/01/2018 => ngày import thư viện vào hệ thống (dd/mm/yyyy)
- packageName: angular => tên package trong Manage Bower Package ( cần chính xác )
- homePage: https://angularjs.org => thay cho mô tả gói pagekage làm gì, đưa trang chủ của gói package để các thanh viên tự tìm hiểu
<!-- END CHANGELOG -->

-------------------------------------------------------------------------------------------------------------------------------------------------------

